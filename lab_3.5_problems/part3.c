/*Meghan Richey				Arm Lab			17 September 2014
problem 3.5 part b subpart iii

(1)  Rprime()  that  takes  the  rotated  (“frame  1”)  GSL  vector  and  the  XYZ
Euler  angle  GSL  vector   [ φ ,  θ ,  ψ ]  as input and returns  the  unrotated  (“frame 0”) GSL vector  as  output,
(2)  H()  that  performs a  three­dimensional  homogeneous coordinate  transformation.  Input  arguments are
the  original  (untransformed)  3­D  GSL  vector,  the  XYZ Euler  angle GSL vector, and the three­dimensional
offset vector d=[d1, d2, d3]; H() returns the three­dimensional transformed GSL vector. 


part3.c
*/

#include <stdio.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_vector.h>
#include <math.h>
#include "part3.h"


gsl_vector * Rprime(gsl_vector* rotatedvector, gsl_vector* Eulervector)
{
	double phi;
	double theta;
	double psi;
	
	gsl_vector * vectorprime = gsl_vector_alloc(3);
	gsl_matrix * Ro = gsl_matrix_alloc(3,3);
	
	phi = (M_PI/180) * gsl_vector_get(Eulervector,0);
	theta = (M_PI/180) * gsl_vector_get(Eulervector,1);
	psi = (M_PI/180) * gsl_vector_get(Eulervector,2);
	
	//Calculates row 0 of Ro
	gsl_matrix_set(Ro, 0, 0,  cos(theta)*cos(psi));
	gsl_matrix_set(Ro, 0, 1,  -cos(theta)*sin(psi));
	gsl_matrix_set(Ro, 0, 2,  sin(theta));
	
	//Calculates row 1 of Ro
	gsl_matrix_set(Ro, 1, 0,  sin(phi)*sin(theta)*cos(psi)+cos(phi)*sin(psi));
	gsl_matrix_set(Ro, 1, 1,  cos(phi)*cos(psi)-sin(phi)*sin(theta)*sin(psi));
	gsl_matrix_set(Ro, 1, 2,  -sin(phi)*cos(theta));
	
	//Calculates row 2 of Ro
	gsl_matrix_set(Ro, 2, 0,  sin(phi)*sin(psi)-cos(phi)*sin(theta)*cos(psi));
	gsl_matrix_set(Ro, 2, 1,  sin(phi)*cos(psi)+cos(phi)*sin(theta)*sin(psi));
	gsl_matrix_set(Ro, 2, 2,  cos(phi)*cos(theta));
	
	gsl_blas_dgemv (CblasTrans, 1.0, Ro, rotatedvector, 0.0, vectorprime);

	gsl_matrix_free(Ro);
	
	return vectorprime;
	
}

gsl_vector * H(gsl_vector* vector, gsl_vector* Eulervector, gsl_vector* offsetvector)
{
	double phi;
	double theta;
	double psi;
	
	gsl_vector * transformedvector = gsl_vector_alloc(4);
	gsl_vector * tempvector = gsl_vector_alloc(4);
	gsl_matrix * Ho = gsl_matrix_alloc(4,4);
	
	//Puts input vector into vector of length 4
	gsl_vector_set(tempvector, 0,  gsl_vector_get(vector, 0));
	gsl_vector_set(tempvector, 1,  gsl_vector_get(vector, 1));
	gsl_vector_set(tempvector, 2,  gsl_vector_get(vector, 2));
	gsl_vector_set(tempvector, 3,  1);	
	
	//Calculates angle values
	phi = (M_PI/180) * gsl_vector_get(Eulervector,0);
	theta = (M_PI/180) * gsl_vector_get(Eulervector,1);
	psi = (M_PI/180) * gsl_vector_get(Eulervector,2);
	
	//Calculates row 0 of Ro (rotation matrix of Ho)
	gsl_matrix_set(Ho, 0, 0,  cos(theta)*cos(psi));
	gsl_matrix_set(Ho, 0, 1,  -cos(theta)*sin(psi));
	gsl_matrix_set(Ho, 0, 2,  sin(theta));
	
	//Calculates row 1 of Ro (rotation matrix of Ho)
	gsl_matrix_set(Ho, 1, 0,  sin(phi)*sin(theta)*cos(psi)+cos(phi)*sin(psi));
	gsl_matrix_set(Ho, 1, 1,  cos(phi)*cos(psi)-sin(phi)*sin(theta)*sin(psi));
	gsl_matrix_set(Ho, 1, 2,  -sin(phi)*cos(theta));
	
	//Calculates row 2 of Ro (rotation matrix of Ho)
	gsl_matrix_set(Ho, 2, 0,  sin(phi)*sin(psi)-cos(phi)*sin(theta)*cos(psi));
	gsl_matrix_set(Ho, 2, 1,  sin(phi)*cos(psi)+cos(phi)*sin(theta)*sin(psi));
	gsl_matrix_set(Ho, 2, 2,  cos(phi)*cos(theta));
	
	//Calculates column 3 of Ho (translation vector of Ho)
	gsl_matrix_set(Ho, 0, 3,  gsl_vector_get(offsetvector, 0));
	gsl_matrix_set(Ho, 1, 3,  gsl_vector_get(offsetvector, 1));
	gsl_matrix_set(Ho, 2, 3,  gsl_vector_get(offsetvector, 2));
	
	//Sets remaining entries of Ho
	gsl_matrix_set(Ho, 3, 0,  0);
	gsl_matrix_set(Ho, 3, 1,  0);
	gsl_matrix_set(Ho, 3, 2,  0);	
	gsl_matrix_set(Ho, 3, 3,  1);	
		
	gsl_blas_dgemv (CblasNoTrans, 1.0, Ho, tempvector, 0.0, transformedvector);
	
	gsl_vector_free(tempvector);
	gsl_matrix_free(Ho);
	
	return transformedvector;
	
}



