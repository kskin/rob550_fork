// part1.h

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_vector.h>
#include <math.h>

// Take as input a 3d vector and an angle (deg), 
// compute the rotation about the x-axis, and
// return the rotated vector
gsl_vector* Rx(gsl_vector* x, double angle);

// Take as input a 3d vector and an angle (deg), 
// compute the rotation about the y-axis, and
// return the rotated vector
gsl_vector* Ry(gsl_vector* y, double angle);

// Take as input a 3d vector and an angle (deg), 
// compute the rotation about the z-axis, and
// return the rotated vector
gsl_vector* Rz(gsl_vector* z, double angle);
