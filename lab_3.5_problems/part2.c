/*Katelyn Fry 					Arm Lab			17 September 2014
problem 3.5 part b subpart ii

A function R() that utilizes XYZ Euler angles (roll φ, pitch θ, yaw ψ) to perform a full 3­D rotation.
Input arguments include a t3D (unrotated frame 0) GSL vector and a single GSL vector with the three 
XYZ Euler angles [φ,θ,ψ] specified in degrees. Return the (rotated frame 1) 3D vector.

part2.c
*/

#include <stdio.h>
#include <math.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include "part2.h"

gsl_vector * R(gsl_vector * vector, gsl_vector * Eulervector)
{
	double phi;
	double theta;
	double psi;
	
	gsl_vector * rotatedvector = gsl_vector_alloc(3);
	gsl_matrix * Ro = gsl_matrix_alloc(3,3);
	
	phi = (M_PI/180) * gsl_vector_get(Eulervector,0);
	theta = (M_PI/180) * gsl_vector_get(Eulervector,1);
	psi = (M_PI/180) * gsl_vector_get(Eulervector,2);
	
	//Calculates row 0 of Ro
	gsl_matrix_set(Ro, 0, 0,  cos(theta)*cos(psi));
	gsl_matrix_set(Ro, 0, 1,  -cos(theta)*sin(psi));
	gsl_matrix_set(Ro, 0, 2,  sin(theta));
	
	//Calculates row 1 of Ro
	gsl_matrix_set(Ro, 1, 0,  sin(phi)*sin(theta)*cos(psi)+cos(phi)*sin(psi));
	gsl_matrix_set(Ro, 1, 1,  cos(phi)*cos(psi)-sin(phi)*sin(theta)*sin(psi));
	gsl_matrix_set(Ro, 1, 2,  -sin(phi)*cos(theta));
	
	//Calculates row 2 of Ro
	gsl_matrix_set(Ro, 2, 0,  sin(phi)*sin(psi)-cos(phi)*sin(theta)*cos(psi));
	gsl_matrix_set(Ro, 2, 1,  sin(phi)*cos(psi)+cos(phi)*sin(theta)*sin(psi));
	gsl_matrix_set(Ro, 2, 2,  cos(phi)*cos(theta));
			
	gsl_blas_dgemv (CblasNoTrans, 1.0, Ro, vector, 0.0, rotatedvector);

	gsl_matrix_free(Ro);
	
	return rotatedvector;
}



