/*Meghan Richey, Katelyn Fry, Katie Skinner  	Arm Lab			17 September 2014
problem 3.5 part b

Allows user to input a vector and rotation angles to calculate a rotated vector.

Lab3Main.c
*/


#include <stdio.h>
#include <math.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include "part1.h"
#include "part2.h"
#include "part3.h"

int main(void)
{
 	double temp;					//Temporary variable
 		
	//Allocates memory to vectors	
 	gsl_vector * vector = gsl_vector_alloc(3);			//Vector to be rotated
 	gsl_vector * vectore = gsl_vector_alloc(3);			//Vector to be rotated Euler
 	gsl_vector * Eulervector = gsl_vector_alloc(3);		//Vector to hold Euler angles
 	gsl_vector * offsetvector = gsl_vector_alloc(3);		//Vector to hold offset values
 	
 	gsl_vector * rotatedx = gsl_vector_alloc(3);			//Vector to hold rotated vector about x
 	gsl_vector * rotatedy = gsl_vector_alloc(3);			//Vector to hold rotated vector about y
 	gsl_vector * rotatedz = gsl_vector_alloc(3);			//Vector to hold rotated vector about z
 	gsl_vector * rotatedvector= gsl_vector_alloc(3);		//Vector to hold rotated vector
 	gsl_vector * vectorprime = gsl_vector_alloc(3);		//Vector to hold rerotated vector
 	gsl_vector * transformedvector = gsl_vector_alloc(4);	//Vector to hold transformed vector


 	//Asks for and stores user input to 3D vector
 	printf("Please input a 3D vector to rotate about axes: \n");
 	printf("First element: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(vector, 0, temp);
 	printf("Second element: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(vector, 1, temp);
 	printf("Third element: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(vector, 2, temp);
 	
 	//Get input angle to rotate about X
 	printf("Please input an angle in degrees to rotate about x axis: ");
 	scanf("%lf",&temp);
 	
 	gsl_vector_memcpy(rotatedx, Rx(vector, temp));
 	
 	//Get input angle to rotate about y
 	printf("Please input an angle in degrees to rotate about y axis: ");
 	scanf("%lf",&temp);
 	
 	gsl_vector_memcpy(rotatedy, Ry(vector, temp));
 	
 	//Get input angle to rotate about z
 	printf("Please input an angle in degrees to rotate about z axis: ");
 	scanf("%lf",&temp);
 	
 	gsl_vector_memcpy(rotatedz, Rz(vector, temp));
 	
 	//Prints the rotated vector about x axis
	printf("Rotated Vector about x: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(rotatedx,0), gsl_vector_get(rotatedx,1), gsl_vector_get(rotatedx,2));
	
	//Prints the rotated vector about y axis
	printf("Rotated Vector about y: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(rotatedy,0), gsl_vector_get(rotatedy,1), gsl_vector_get(rotatedy,2));
	
	//Prints the rotated vector about z axis
	printf("Rotated Vector about z: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(rotatedz,0), gsl_vector_get(rotatedz,1), gsl_vector_get(rotatedz,2));
 			
 	//Asks for and stores user input to 3D vector
 	printf("Please input a 3D vector to rotate using Euler angles: \n");
 	printf("First element: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(vectore, 0, temp);
 	printf("Second element: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(vectore, 1, temp);
 	printf("Third element: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(vectore, 2, temp);
 			
 	//Asks for and stores input to 3D Euler vector
 	printf("Please input a 3D Euler vector: \n");
 	printf("Roll (phi) [degrees]: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(Eulervector, 0, temp);
 	printf("Pitch (theta) [degrees]: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(Eulervector, 1, temp);
 	printf("Yaw (psi) [degrees]: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(Eulervector, 2, temp);
 	
 	//Asks for and stores input to 3D offset vector
 	printf("Please input an offset vector: \n");
 	printf("Offset d1: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(offsetvector, 0, temp);
 	printf("Offset d2: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(offsetvector, 1, temp);
 	printf("Offset d3: ");
 	scanf("%lf",&temp);
 	gsl_vector_set(offsetvector, 2, temp);
 	
 	//Calls function R to rotate vector by values given in Eulervector 
	//Stores into rotatedvector			
	gsl_vector_memcpy(rotatedvector,R(vectore, Eulervector));
	
	//Prints the rotated vector
	printf("Rotated Vector: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(rotatedvector,0), gsl_vector_get(rotatedvector,1), gsl_vector_get(rotatedvector,2));
	
	//Calls function Rprime to rotate rotatedvector by values given in Eulervector 
	//Stores into vectorprime			
	gsl_vector_memcpy(vectorprime,Rprime(rotatedvector, Eulervector));
		
	//Prints the re-rotated vector
	printf("Original Vector: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(vectorprime,0), gsl_vector_get(vectorprime,1), gsl_vector_get(vectorprime,2));

	//Calls function Rprime to rotate rotatedvector by values given in Eulervector 
	//Stores into vectorprime	
	gsl_vector_memcpy(transformedvector,H(vectore, Eulervector, offsetvector));
	
	//Prints the transformed vector
	printf("Transformed Vector: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(transformedvector,0), gsl_vector_get(transformedvector,1), gsl_vector_get(transformedvector,2));

 	//Frees Vectors
 	gsl_vector_free(vector);
 	gsl_vector_free(vectore);
 	gsl_vector_free(Eulervector);
 	gsl_vector_free(rotatedvector);
 	gsl_vector_free(vectorprime);
 		
 	return 0;
}
