//part2.h

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_vector.h>
#include <math.h>

//Utilizes XYZ Euler angles (roll φ, pitch θ, yaw ψ) to perform a full 3­D rotation.
//Input arguments include a t3D (unrotated frame 0) GSL vector and a single GSL vector with the three 
//XYZ Euler angles [φ,θ,ψ] specified in degrees. Return the (rotated frame 1) 3D vector.
gsl_vector * R(gsl_vector * vector, gsl_vector * Eulervector);
