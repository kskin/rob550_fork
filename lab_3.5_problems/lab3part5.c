//LAB3PART5.C: Katelyn Fry, Meghan Richey, and Katie Skinner
// See README.txt for compiling instructions


/*Katie Skinner                                 Arm Lab                 17 September 2014
problem 3.5 part b subpart i

Rx(),  Ry(),  and  Rz()   that  takes  as  input  a  three­dimensional  GSL  vector
and  an  angle  (in  degrees);  each   should  return a  three­dimensional  GSL  vector that  represents the input
vector rotated by the specified number of degrees about the designated axis (x, y, or z)

part1.c
*/

#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <math.h>
#include <stdio.h>
#include <gsl/gsl_cblas.h>


const double DEG_TO_RAD = 0.0174532925;

// Take as input a 3d vector and an angle (deg),
// compute the rotation about the x-axis, and
// return the rotated vector
gsl_vector* Rx(gsl_vector* x, double angle)
{
  size_t row = 3;
  size_t col = 3;
  double theta = DEG_TO_RAD * angle;
  gsl_vector *rotatedx = gsl_vector_alloc(row);
  gsl_matrix* rot_x = gsl_matrix_alloc(row, col);

  gsl_matrix_set(rot_x, 0, 0, 1);
  gsl_matrix_set(rot_x, 0, 1, 0);
  gsl_matrix_set(rot_x, 0, 2, 0);
  gsl_matrix_set(rot_x, 1, 0, 0);
  gsl_matrix_set(rot_x, 1, 1, cos(theta));
  gsl_matrix_set(rot_x, 1, 2, -sin(theta));
  gsl_matrix_set(rot_x, 2, 0, 0);
  gsl_matrix_set(rot_x, 2, 1, sin(theta));
  gsl_matrix_set(rot_x, 2, 2, cos(theta));

  gsl_blas_dgemv(CblasNoTrans, 1.0, rot_x, x, 0.0, rotatedx);

  return rotatedx;
}

// Take as input a 3d vector and an angle (deg),
// compute the rotation about the y-axis, and
// return the rotated vector
gsl_vector* Ry(gsl_vector* y, double angle)
{
  size_t row = 3;
  size_t col = 3;
  double theta = DEG_TO_RAD * angle;
  gsl_vector *rotatedy = gsl_vector_alloc(row);
  gsl_matrix* rot_y = gsl_matrix_alloc(row, col);

  gsl_matrix_set(rot_y, 0, 0, cos(theta));
  gsl_matrix_set(rot_y, 0, 1, 0);
  gsl_matrix_set(rot_y, 0, 2, sin(theta));
  gsl_matrix_set(rot_y, 1, 0, 0);
  gsl_matrix_set(rot_y, 1, 1, 1);
  gsl_matrix_set(rot_y, 1, 2, 0);
  gsl_matrix_set(rot_y, 2, 0, -sin(theta));
  gsl_matrix_set(rot_y, 2, 1, 0);
  gsl_matrix_set(rot_y, 2, 2, cos(theta));

  gsl_blas_dgemv(CblasNoTrans, 1.0, rot_y, y, 0.0, rotatedy);

  //gsl_blas_dgemmv

  return rotatedy;
}

// Take as input a 3d vector and an angle (deg),
// compute the rotation about the z-axis, and
// return the rotated vector
gsl_vector* Rz(gsl_vector* z, double angle)
{
  size_t row = 3;
  size_t col = 3;
  double theta = DEG_TO_RAD * angle;
  gsl_vector *rotatedz = gsl_vector_alloc(row);
  gsl_matrix* rot_z = gsl_matrix_alloc(row, col);

  gsl_matrix_set(rot_z, 0, 0, cos(theta));
  gsl_matrix_set(rot_z, 0, 1, -sin(theta));
  gsl_matrix_set(rot_z, 0, 2, 0);
  gsl_matrix_set(rot_z, 1, 0, sin(theta));
  gsl_matrix_set(rot_z, 1, 1, cos(theta));
  gsl_matrix_set(rot_z, 1, 2, 0);
  gsl_matrix_set(rot_z, 2, 0, 0);
  gsl_matrix_set(rot_z, 2, 1, 0);
  gsl_matrix_set(rot_z, 2, 2, 1);

  gsl_blas_dgemv(CblasNoTrans, 1.0, rot_z, z, 0.0, rotatedz);

  return rotatedz;
}

/*Katelyn Fry                                   Arm Lab                 17 September 2014
problem 3.5 part b subpart ii

A function R() that utilizes XYZ Euler angles (roll φ, pitch θ, yaw ψ) to perform a full 3­D rotation.
Input arguments include a t3D (unrotated frame 0) GSL vector and a single GSL vector with the three
XYZ Euler angles [φ,θ,ψ] specified in degrees. Return the (rotated frame 1) 3D vector.

part2.c
*/

gsl_vector * R(gsl_vector * vector, gsl_vector * Eulervector)
{
        double phi;
        double theta;
        double psi;

        gsl_vector * rotatedvector = gsl_vector_alloc(3);
        gsl_matrix * Ro = gsl_matrix_alloc(3,3);

        phi = (M_PI/180) * gsl_vector_get(Eulervector,0);
        theta = (M_PI/180) * gsl_vector_get(Eulervector,1);
        psi = (M_PI/180) * gsl_vector_get(Eulervector,2);

        //Calculates row 0 of Ro
        gsl_matrix_set(Ro, 0, 0,  cos(theta)*cos(psi));
        gsl_matrix_set(Ro, 0, 1,  -cos(theta)*sin(psi));
        gsl_matrix_set(Ro, 0, 2,  sin(theta));

        //Calculates row 1 of Ro
        gsl_matrix_set(Ro, 1, 0,  sin(phi)*sin(theta)*cos(psi)+cos(phi)*sin(psi));
        gsl_matrix_set(Ro, 1, 1,  cos(phi)*cos(psi)-sin(phi)*sin(theta)*sin(psi));
        gsl_matrix_set(Ro, 1, 2,  -sin(phi)*cos(theta));

        //Calculates row 2 of Ro
        gsl_matrix_set(Ro, 2, 0,  sin(phi)*sin(psi)-cos(phi)*sin(theta)*cos(psi));
        gsl_matrix_set(Ro, 2, 1,  sin(phi)*cos(psi)+cos(phi)*sin(theta)*sin(psi));
        gsl_matrix_set(Ro, 2, 2,  cos(phi)*cos(theta));

        gsl_blas_dgemv (CblasNoTrans, 1.0, Ro, vector, 0.0, rotatedvector);

        gsl_matrix_free(Ro);

        return rotatedvector;
}

/*Meghan Richey                         Arm Lab                 17 September 2014
problem 3.5 part b subpart iii

(1)  Rprime()  that  takes  the  rotated  (“frame  1”)  GSL  vector  and  the  XYZ
Euler  angle  GSL  vector   [ φ ,  θ ,  ψ ]  as input and returns  the  unrotated  (“frame 0”) GSL vector  as  output,
(2)  H()  that  performs a  three­dimensional  homogeneous coordinate  transformation.  Input  arguments are
the  original  (untransformed)  3­D  GSL  vector,  the  XYZ Euler  angle GSL vector, and the three­dimensional
offset vector d=[d1, d2, d3]; H() returns the three­dimensional transformed GSL vector.


part3.c
*/


gsl_vector * Rprime(gsl_vector* rotatedvector, gsl_vector* Eulervector)
{
        double phi;
        double theta;
        double psi;

        gsl_vector * vectorprime = gsl_vector_alloc(3);
        gsl_matrix * Ro = gsl_matrix_alloc(3,3);

        phi = (M_PI/180) * gsl_vector_get(Eulervector,0);
        theta = (M_PI/180) * gsl_vector_get(Eulervector,1);
        psi = (M_PI/180) * gsl_vector_get(Eulervector,2);

        //Calculates row 0 of Ro
        gsl_matrix_set(Ro, 0, 0,  cos(theta)*cos(psi));
        gsl_matrix_set(Ro, 0, 1,  -cos(theta)*sin(psi));
        gsl_matrix_set(Ro, 0, 2,  sin(theta));

        //Calculates row 1 of Ro
        gsl_matrix_set(Ro, 1, 0,  sin(phi)*sin(theta)*cos(psi)+cos(phi)*sin(psi));
        gsl_matrix_set(Ro, 1, 1,  cos(phi)*cos(psi)-sin(phi)*sin(theta)*sin(psi));
        gsl_matrix_set(Ro, 1, 2,  -sin(phi)*cos(theta));

        //Calculates row 2 of Ro
        gsl_matrix_set(Ro, 2, 0,  sin(phi)*sin(psi)-cos(phi)*sin(theta)*cos(psi));
        gsl_matrix_set(Ro, 2, 1,  sin(phi)*cos(psi)+cos(phi)*sin(theta)*sin(psi));
        gsl_matrix_set(Ro, 2, 2,  cos(phi)*cos(theta));

        gsl_blas_dgemv (CblasTrans, 1.0, Ro, rotatedvector, 0.0, vectorprime);

        gsl_matrix_free(Ro);

        return vectorprime;

}

gsl_vector * H(gsl_vector* vector, gsl_vector* Eulervector, gsl_vector* offsetvector)
{
        double phi;
        double theta;
        double psi;

        gsl_vector * transformedvector = gsl_vector_alloc(4);
        gsl_vector * tempvector = gsl_vector_alloc(4);
        gsl_matrix * Ho = gsl_matrix_alloc(4,4);

        //Puts input vector into vector of length 4
        gsl_vector_set(tempvector, 0,  gsl_vector_get(vector, 0));
        gsl_vector_set(tempvector, 1,  gsl_vector_get(vector, 1));
        gsl_vector_set(tempvector, 2,  gsl_vector_get(vector, 2));
        gsl_vector_set(tempvector, 3,  1);

        //Calculates angle values
        phi = (M_PI/180) * gsl_vector_get(Eulervector,0);
        theta = (M_PI/180) * gsl_vector_get(Eulervector,1);
        psi = (M_PI/180) * gsl_vector_get(Eulervector,2);

        //Calculates row 0 of Ro (rotation matrix of Ho)
        gsl_matrix_set(Ho, 0, 0,  cos(theta)*cos(psi));
        gsl_matrix_set(Ho, 0, 1,  -cos(theta)*sin(psi));
        gsl_matrix_set(Ho, 0, 2,  sin(theta));

        //Calculates row 1 of Ro (rotation matrix of Ho)
        gsl_matrix_set(Ho, 1, 0,  sin(phi)*sin(theta)*cos(psi)+cos(phi)*sin(psi));
        gsl_matrix_set(Ho, 1, 1,  cos(phi)*cos(psi)-sin(phi)*sin(theta)*sin(psi));
        gsl_matrix_set(Ho, 1, 2,  -sin(phi)*cos(theta));

        //Calculates row 2 of Ro (rotation matrix of Ho)
        gsl_matrix_set(Ho, 2, 0,  sin(phi)*sin(psi)-cos(phi)*sin(theta)*cos(psi));
        gsl_matrix_set(Ho, 2, 1,  sin(phi)*cos(psi)+cos(phi)*sin(theta)*sin(psi));
        gsl_matrix_set(Ho, 2, 2,  cos(phi)*cos(theta));

        //Calculates column 3 of Ho (translation vector of Ho)
        gsl_matrix_set(Ho, 0, 3,  gsl_vector_get(offsetvector, 0));
        gsl_matrix_set(Ho, 1, 3,  gsl_vector_get(offsetvector, 1));
        gsl_matrix_set(Ho, 2, 3,  gsl_vector_get(offsetvector, 2));

        //Sets remaining entries of Ho
        gsl_matrix_set(Ho, 3, 0,  0);
        gsl_matrix_set(Ho, 3, 1,  0);
        gsl_matrix_set(Ho, 3, 2,  0);
        gsl_matrix_set(Ho, 3, 3,  1);

        gsl_blas_dgemv (CblasNoTrans, 1.0, Ho, tempvector, 0.0, transformedvector);

        gsl_vector_free(tempvector);
        gsl_matrix_free(Ho);

        return transformedvector;

}

/*Meghan Richey, Katelyn Fry, Katie Skinner     Arm Lab                 17 September 2014
problem 3.5 part b

Allows user to input a vector and rotation angles to calculate a rotated vector.

Lab3Main.c
*/

int main(void)
{
        double temp;                                    //Temporary variable

        //Allocates memory to vectors
        gsl_vector * vector = gsl_vector_alloc(3);                      //Vector to be rotated
        gsl_vector * vectore = gsl_vector_alloc(3);                     //Vector to be rotated Euler
        gsl_vector * Eulervector = gsl_vector_alloc(3);         //Vector to hold Euler angles
        gsl_vector * offsetvector = gsl_vector_alloc(3);                //Vector to hold offset values

        gsl_vector * rotatedx = gsl_vector_alloc(3);                    //Vector to hold rotated vector about x
        gsl_vector * rotatedy = gsl_vector_alloc(3);                    //Vector to hold rotated vector about y
        gsl_vector * rotatedz = gsl_vector_alloc(3);                    //Vector to hold rotated vector about z
        gsl_vector * rotatedvector= gsl_vector_alloc(3);                //Vector to hold rotated vector
        gsl_vector * vectorprime = gsl_vector_alloc(3);         //Vector to hold rerotated vector
        gsl_vector * transformedvector = gsl_vector_alloc(4);   //Vector to hold transformed vector


        //Asks for and stores user input to 3D vector
        printf("Please input a 3D vector to rotate about axes: \n");
        printf("First element: ");
        scanf("%lf",&temp);
        gsl_vector_set(vector, 0, temp);
        printf("Second element: ");
        scanf("%lf",&temp);
        gsl_vector_set(vector, 1, temp);
        printf("Third element: ");
        scanf("%lf",&temp);
        gsl_vector_set(vector, 2, temp);

        //Get input angle to rotate about X
        printf("Please input an angle in degrees to rotate about x axis: ");
        scanf("%lf",&temp);

        gsl_vector_memcpy(rotatedx, Rx(vector, temp));

        //Get input angle to rotate about y
        printf("Please input an angle in degrees to rotate about y axis: ");
        scanf("%lf",&temp);

        gsl_vector_memcpy(rotatedy, Ry(vector, temp));

        //Get input angle to rotate about z
        printf("Please input an angle in degrees to rotate about z axis: ");
        scanf("%lf",&temp);

        gsl_vector_memcpy(rotatedz, Rz(vector, temp));

        //Prints the rotated vector about x axis
        printf("Rotated Vector about x: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(rotatedx,0), gsl_vector_get(rotatedx,1), gsl_vector_get(rotatedx,2));

        //Prints the rotated vector about y axis
        printf("Rotated Vector about y: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(rotatedy,0), gsl_vector_get(rotatedy,1), gsl_vector_get(rotatedy,2));

        //Prints the rotated vector about z axis
        printf("Rotated Vector about z: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(rotatedz,0), gsl_vector_get(rotatedz,1), gsl_vector_get(rotatedz,2));

        //Asks for and stores user input to 3D vector
        printf("Please input a 3D vector to rotate using Euler angles: \n");
        printf("First element: ");
        scanf("%lf",&temp);
        gsl_vector_set(vectore, 0, temp);
        printf("Second element: ");
        scanf("%lf",&temp);
        gsl_vector_set(vectore, 1, temp);
        printf("Third element: ");
        scanf("%lf",&temp);
        gsl_vector_set(vectore, 2, temp);

        //Asks for and stores input to 3D Euler vector
        printf("Please input a 3D Euler vector: \n");
        printf("Roll (phi) [degrees]: ");
        scanf("%lf",&temp);
        gsl_vector_set(Eulervector, 0, temp);
        printf("Pitch (theta) [degrees]: ");
        scanf("%lf",&temp);
        gsl_vector_set(Eulervector, 1, temp);
        printf("Yaw (psi) [degrees]: ");
        scanf("%lf",&temp);
        gsl_vector_set(Eulervector, 2, temp);

        //Asks for and stores input to 3D offset vector
        printf("Please input an offset vector: \n");
        printf("Offset d1: ");
        scanf("%lf",&temp);
        gsl_vector_set(offsetvector, 0, temp);
        printf("Offset d2: ");
        scanf("%lf",&temp);
        gsl_vector_set(offsetvector, 1, temp);
        printf("Offset d3: ");
        scanf("%lf",&temp);
        gsl_vector_set(offsetvector, 2, temp);

        //Calls function R to rotate vector by values given in Eulervector
        //Stores into rotatedvector
        gsl_vector_memcpy(rotatedvector,R(vectore, Eulervector));

        //Prints the rotated vector
        printf("Rotated Vector: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(rotatedvector,0), gsl_vector_get(rotatedvector,1), gsl_vector_get(rotatedvector,2));

        //Calls function Rprime to rotate rotatedvector by values given in Eulervector
        //Stores into vectorprime
        gsl_vector_memcpy(vectorprime,Rprime(rotatedvector, Eulervector));

        //Prints the re-rotated vector
        printf("Original Vector: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(vectorprime,0), gsl_vector_get(vectorprime,1), gsl_vector_get(vectorprime,2));

        //Calls function Rprime to rotate rotatedvector by values given in Eulervector
        //Stores into vectorprime
        gsl_vector_memcpy(transformedvector,H(vectore, Eulervector, offsetvector));

        //Prints the transformed vector
        printf("Transformed Vector: [%.3lf %.3lf %.3lf]^T\n", gsl_vector_get(transformedvector,0), gsl_vector_get(transformedvector,1), gsl_vector_get(transformedvector,2));

        //Frees Vectors
        gsl_vector_free(vector);
        gsl_vector_free(vectore);
        gsl_vector_free(Eulervector);
        gsl_vector_free(rotatedvector);
        gsl_vector_free(vectorprime);

        return 0;
}

