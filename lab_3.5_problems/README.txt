Section 1 Table 4 Submission for Lab 3.5 parts b)i-iii
Katelyn Fry, Meghan Richey, and Katie Skinner

1. Part b)i
Coded by: Katie Skinner
This problem takes as input a 3D GSL vector and an angle and rotates each axis around the angle. There is an included part1.h file to instantiate the function calls, and this c file is compiled in the main makefile.

2. Part b)ii
Coded by: Katelyn Fry
This problem performs a full 3D rotation by using XYZ Euler angles. Many of these functions are also used in Part b)iii; therefore, there was a lot of collaboration in coding between these two parts. There is an included part2.h file to instantiate the function calls, and this c file is compiled in the main makefile.

3. Part c)iii
Coded by: Meghan Richey (collaboration with Katelyn Fry)
This problem had two parts. The first part takes a rotated vector and XYZ Euler angles and returns an unrotated GSL vector. The second part performs a 3D homogeneous coordinate transformation. This part made use of many functions defined in part2.c; therefore, there was a lot of collaboration in coding. There is an included part3.h file to instantiate the funciton calls, and this c file is compiled in the main makefile.

4. Lab3Main.c
Coded by: Katelyn Fry
The main body of this code prompts the user for input and passes the appropriate variables to each function. The result of each part is printed out clearly. (NOTE: "^T" indicates transpose). This file is compiled in the main makefile.

TO RUN:
1. Type "make"
2. Type "./Lab3Main"
3. Follow prompts
