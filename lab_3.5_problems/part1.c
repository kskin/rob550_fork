/*Katie Skinner					Arm Lab			17 September 2014
problem 3.5 part b subpart i

Rx(),  Ry(),  and  Rz()   that  takes  as  input  a  three­dimensional  GSL  vector 
and  an  angle  (in  degrees);  each   should  return a  three­dimensional  GSL  vector that  represents the input
vector rotated by the specified number of degrees about the designated axis (x, y, or z)

part1.c
*/

#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <math.h>
#include <stdio.h>

const double DEG_TO_RAD = 0.0174532925;

// Take as input a 3d vector and an angle (deg), 
// compute the rotation about the x-axis, and
// return the rotated vector
gsl_vector* Rx(gsl_vector* x, double angle)
{
  size_t row = 3;
  size_t col = 3;
  double theta = DEG_TO_RAD * angle;
  gsl_vector *rotatedx = gsl_vector_alloc(row);
  gsl_matrix* rot_x = gsl_matrix_alloc(row, col);

  gsl_matrix_set(rot_x, 0, 0, 1);
  gsl_matrix_set(rot_x, 0, 1, 0);
  gsl_matrix_set(rot_x, 0, 2, 0);
  gsl_matrix_set(rot_x, 1, 0, 0);
  gsl_matrix_set(rot_x, 1, 1, cos(theta));
  gsl_matrix_set(rot_x, 1, 2, -sin(theta));
  gsl_matrix_set(rot_x, 2, 0, 0);
  gsl_matrix_set(rot_x, 2, 1, sin(theta));
  gsl_matrix_set(rot_x, 2, 2, cos(theta));

  gsl_blas_dgemv(CblasNoTrans, 1.0, rot_x, x, 0.0, rotatedx);

  return rotatedx;
}

// Take as input a 3d vector and an angle (deg), 
// compute the rotation about the y-axis, and
// return the rotated vector
gsl_vector* Ry(gsl_vector* y, double angle)
{
  size_t row = 3;
  size_t col = 3;
  double theta = DEG_TO_RAD * angle;
  gsl_vector *rotatedy = gsl_vector_alloc(row);
  gsl_matrix* rot_y = gsl_matrix_alloc(row, col);

  gsl_matrix_set(rot_y, 0, 0, cos(theta));
  gsl_matrix_set(rot_y, 0, 1, 0);
  gsl_matrix_set(rot_y, 0, 2, sin(theta));
  gsl_matrix_set(rot_y, 1, 0, 0);
  gsl_matrix_set(rot_y, 1, 1, 1);
  gsl_matrix_set(rot_y, 1, 2, 0);
  gsl_matrix_set(rot_y, 2, 0, -sin(theta));
  gsl_matrix_set(rot_y, 2, 1, 0);
  gsl_matrix_set(rot_y, 2, 2, cos(theta));

  gsl_blas_dgemv(CblasNoTrans, 1.0, rot_y, y, 0.0, rotatedy);

  //gsl_blas_dgemmv

  return rotatedy; 
}

// Take as input a 3d vector and an angle (deg), 
// compute the rotation about the z-axis, and
// return the rotated vector
gsl_vector* Rz(gsl_vector* z, double angle)
{
  size_t row = 3;
  size_t col = 3;
  double theta = DEG_TO_RAD * angle;
  gsl_vector *rotatedz = gsl_vector_alloc(row);
  gsl_matrix* rot_z = gsl_matrix_alloc(row, col);

  gsl_matrix_set(rot_z, 0, 0, cos(theta));
  gsl_matrix_set(rot_z, 0, 1, -sin(theta));
  gsl_matrix_set(rot_z, 0, 2, 0);
  gsl_matrix_set(rot_z, 1, 0, sin(theta));
  gsl_matrix_set(rot_z, 1, 1, cos(theta));
  gsl_matrix_set(rot_z, 1, 2, 0);
  gsl_matrix_set(rot_z, 2, 0, 0);
  gsl_matrix_set(rot_z, 2, 1, 0);
  gsl_matrix_set(rot_z, 2, 2, 1);

  gsl_blas_dgemv(CblasNoTrans, 1.0, rot_z, z, 0.0, rotatedz);

  return rotatedz; 
}


