//part3.h

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_vector.h>
#include <math.h>

//Takes  the  rotated  (“frame  1”)  GSL  vector  and  the  XYZ
//Euler  angle  GSL  vector   [ φ ,  θ ,  ψ ]  as input and returns  the  unrotated  (“frame 0”) GSL vector  as  output
gsl_vector * Rprime(gsl_vector* rotatedvector, gsl_vector* Eulervector);

//H()  that  performs a  three­dimensional  homogeneous coordinate  transformation.  Input  arguments are
//the  original  (untransformed)  3­D  GSL  vector,  the  XYZ Euler  angle GSL vector, and the three­dimensional
//offset vector d=[d1, d2, d3]; H() returns the three­dimensional transformed GSL vector.
gsl_vector * H(gsl_vector* vector, gsl_vector* Eulervector, gsl_vector* offsetvector);
