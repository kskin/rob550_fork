// ----------------------------------------------------------------------------------------------
// ----- REXARM.C -------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// This file handles rexarm kinematics and implements clamps and limits to avoid damage
// to the arm.
// ----------------------------------------------------------------------------------------------

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include "rexarm.h"
#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_cblas.h>
#include <math/gsl_util_matrix.h>

// ===============================================================================================
// ===== REXARM CLAMP ============================================================================
// ===============================================================================================
// This function takes in a configuration and sets flags if the configuration is beyond physical
// limits of the arm. Returns both attempted joint angles and flags (1-invalid, 0-valid).
// ===============================================================================================
double *rexarm_clamp(double q[8]){
	if(q[0] < JOINT1_NEG){
		q[0] = JOINT1_NEG;
		q[4] = 1;
	}
	if(q[0] > JOINT1_POS){
		q[0] = JOINT1_POS;
		q[4] = 1;
	}
	if(q[1] < JOINT2_NEGH){
		q[1] = JOINT2_NEGH;
		q[5] = 1;
	}
	if(q[1] > JOINT2_POSH){
		q[1] = JOINT2_POSH;
		q[5] = 1;
	}
	if(q[2] < JOINT3_NEG){
		q[2] = JOINT3_NEG;
		q[6] = 1;
	}
	if(q[2] > JOINT3_POS){
		q[2] = JOINT3_POS;
		q[6] = 1;
	}
	if(q[3] < JOINT4_NEG){
		q[3] = JOINT4_NEG;
		q[7] = 1;
	}
	if(q[3] > JOINT4_POS){
		q[3] = JOINT4_NEG;
		q[7] = 1;
	}
	return q;
}

// ===============================================================================================
// ===== CHECK LIMITS ============================================================================
// ===============================================================================================
// This function returns 1 if the forward kinematics configuration results in a self collision
// or floor collision, returns 0 otherwise.
// ===============================================================================================
int check_limits(forward_kinematics_t *k){
	double r;
	if((k->position[2]) < 0)
		return -1;
	if(k->position[2] < .35){
		r=sqrt(pow(k->position[0], 2)+pow(k->position[1], 2));
		if (r<4.0)
			return -2;
		}
	return 0;
}

// ===============================================================================================
// ===== FORWARD KINEMATICS ======================================================================
// ===============================================================================================
// This function computes forward kinematics parameters for a given joint configuration (deg).
// ===============================================================================================
forward_kinematics_t * forward_kinematics(double * q){

/*  DH Parameter Table
    di    thetai    alphai   ai
1   l1       *       -90      0
2   0       *-90      0      l2
3   0        *        0      l3
4   0        *        0      l4

*/
  forward_kinematics_t *x = calloc (1, sizeof(*x));

  //Robot Link Measurements [m]
  double l1 = .113;
  double l2 = .100;
  double l3 = .100;
  double l4 = .113;

  //Link angles [rad]
  double theta1 = (q[0] - 90) * (M_PI/180.);
  double theta2 = (q[1] + 90) * (M_PI/180.);
  double theta3 = q[2] * (M_PI/180.);
  double theta4 = q[3] * (M_PI/180.);

  //DH parameters [m] and [rad]
  double d1 = l1;
  double d2 = 0;
  double d3 = 0;
  double d4 = 0;
  
  double alpha1 = 90*(M_PI/180.);
  double alpha2 = 0 * (M_PI/180.);
  double alpha3 = 0 * (M_PI/180.);
  double alpha4 = 0 * (M_PI/180.);

  double a1 = 0;
  double a2 = l2;
  double a3 = l3;
  double a4 = l4;

  //Define and allocate Transformation matrices
  gsl_matrix * A01;  //matrix transformation from 0 to 1
  gsl_matrix * A12;  //matrix transformation from 1 to 2
  gsl_matrix * A23;  //matrix transformation from 2 to 3
  gsl_matrix * A34;  //matrix transformation from 3 to 4

  gsl_matrix * T02;  //matrix transformation from 0 to 1
  gsl_matrix * T03;  //matrix transformation from 1 to 2
  gsl_matrix * T04;  //matrix transformation from 2 to 3

  A01 = gsl_matrix_alloc(4,4);
  A12 = gsl_matrix_alloc(4,4);
  A23 = gsl_matrix_alloc(4,4);
  A34 = gsl_matrix_alloc(4,4);

  T02 = gsl_matrix_alloc(4,4);
  T03 = gsl_matrix_alloc(4,4);
  T04 = gsl_matrix_alloc(4,4);

  //Populate Matrices 
  //A01
  gsl_matrix_set(A01, 0, 0, cos(theta1));
  gsl_matrix_set(A01, 0, 1, -sin(theta1)*cos(alpha1));
  gsl_matrix_set(A01, 0, 2, sin(theta1)*sin(alpha1));
  gsl_matrix_set(A01, 0, 3, a1*cos(theta1));
  gsl_matrix_set(A01, 1, 0, sin(theta1));
  gsl_matrix_set(A01, 1, 1, cos(theta1)*cos(alpha1)); 
  gsl_matrix_set(A01, 1, 2, -cos(theta1)*sin(alpha1));
  gsl_matrix_set(A01, 1, 3, a1*sin(theta1));
  gsl_matrix_set(A01, 2, 0, 0);
  gsl_matrix_set(A01, 2, 1, sin(alpha1));
  gsl_matrix_set(A01, 2, 2, cos(alpha1));
  gsl_matrix_set(A01, 2, 3, d1);
  gsl_matrix_set(A01, 3, 0, 0);
  gsl_matrix_set(A01, 3, 1, 0);
  gsl_matrix_set(A01, 3, 2, 0);
  gsl_matrix_set(A01, 3, 3, 1);

  //A12
  gsl_matrix_set(A12, 0, 0, cos(theta2));
  gsl_matrix_set(A12, 0, 1, -sin(theta2)*cos(alpha2));
  gsl_matrix_set(A12, 0, 2, sin(theta2)*sin(alpha2));
  gsl_matrix_set(A12, 0, 3, a2*cos(theta2));
  gsl_matrix_set(A12, 1, 0, sin(theta2));
  gsl_matrix_set(A12, 1, 1, cos(theta2)*cos(alpha2)); 
  gsl_matrix_set(A12, 1, 2, -cos(theta2)*sin(alpha2));
  gsl_matrix_set(A12, 1, 3, a2*sin(theta2));
  gsl_matrix_set(A12, 2, 0, 0);
  gsl_matrix_set(A12, 2, 1, sin(alpha2));
  gsl_matrix_set(A12, 2, 2, cos(alpha2));
  gsl_matrix_set(A12, 2, 3, d2);
  gsl_matrix_set(A12, 3, 0, 0);
  gsl_matrix_set(A12, 3, 1, 0);
  gsl_matrix_set(A12, 3, 2, 0);
  gsl_matrix_set(A12, 3, 3, 1);

  //A23
  gsl_matrix_set(A23, 0, 0, cos(theta3));
  gsl_matrix_set(A23, 0, 1, -sin(theta3)*cos(alpha3));
  gsl_matrix_set(A23, 0, 2, sin(theta3)*sin(alpha3));
  gsl_matrix_set(A23, 0, 3, a3*cos(theta3));
  gsl_matrix_set(A23, 1, 0, sin(theta3));
  gsl_matrix_set(A23, 1, 1, cos(theta3)*cos(alpha3)); 
  gsl_matrix_set(A23, 1, 2, -cos(theta3)*sin(alpha3));
  gsl_matrix_set(A23, 1, 3, a3*sin(theta3));
  gsl_matrix_set(A23, 2, 0, 0);
  gsl_matrix_set(A23, 2, 1, sin(alpha3));
  gsl_matrix_set(A23, 2, 2, cos(alpha3));
  gsl_matrix_set(A23, 2, 3, d3);
  gsl_matrix_set(A23, 3, 0, 0);
  gsl_matrix_set(A23, 3, 1, 0);
  gsl_matrix_set(A23, 3, 2, 0);
  gsl_matrix_set(A23, 3, 3, 1);

  //A34
  gsl_matrix_set(A34, 0, 0, cos(theta4));
  gsl_matrix_set(A34, 0, 1, -sin(theta4)*cos(alpha4));
  gsl_matrix_set(A34, 0, 2, sin(theta4)*sin(alpha4));
  gsl_matrix_set(A34, 0, 3, a4*cos(theta4));
  gsl_matrix_set(A34, 1, 0, sin(theta4));
  gsl_matrix_set(A34, 1, 1, cos(theta4)*cos(alpha4)); 
  gsl_matrix_set(A34, 1, 2, -cos(theta4)*sin(alpha4));
  gsl_matrix_set(A34, 1, 3, a4*sin(theta4));
  gsl_matrix_set(A34, 2, 0, 0);
  gsl_matrix_set(A34, 2, 1, sin(alpha4));
  gsl_matrix_set(A34, 2, 2, cos(alpha4));
  gsl_matrix_set(A34, 2, 3, d4);
  gsl_matrix_set(A34, 3, 0, 0);
  gsl_matrix_set(A34, 3, 1, 0);
  gsl_matrix_set(A34, 3, 2, 0);
  gsl_matrix_set(A34, 3, 3, 1);


  //Calculate w
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, A01, A12, 0.0, T02);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, T02, A23, 0.0, T03);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, T03, A34, 0.0, T04);

  //Values to be returned
    //double orientation;
    //double position[3];
    //double T02[4,4]
    //double T03[4,4]		  
    //double T04[4,4]		  
		  
  x->orientation = theta2 + theta3 + theta4 - M_PI/2;
  x->position [0] = gsl_matrix_get(T04, 0, 3);
  x->position [1] = gsl_matrix_get(T04, 1, 3);
  x->position [2] = gsl_matrix_get(T04, 2, 3);

  x->T02[0] = gsl_matrix_get(T02, 0, 0);
  x->T02[1] = gsl_matrix_get(T02, 0, 1);
  x->T02[2] = gsl_matrix_get(T02, 0, 2);
  x->T02[3] = gsl_matrix_get(T02, 0, 3);
  x->T02[4] = gsl_matrix_get(T02, 1, 0);
  x->T02[5] = gsl_matrix_get(T02, 1, 1); 
  x->T02[6] = gsl_matrix_get(T02, 1, 2);
  x->T02[7] = gsl_matrix_get(T02, 1, 3);
  x->T02[8] = gsl_matrix_get(T02, 2, 0);
  x->T02[9] = gsl_matrix_get(T02, 2, 1);
  x->T02[10] = gsl_matrix_get(T02, 2, 2);
  x->T02[11] = gsl_matrix_get(T02, 2, 3);
  x->T02[12] = gsl_matrix_get(T02, 3, 0);
  x->T02[13] = gsl_matrix_get(T02, 3, 1);
  x->T02[14] = gsl_matrix_get(T02, 3, 2);
  x->T02[15] = gsl_matrix_get(T02, 3, 3);

  x->T03[0] = gsl_matrix_get(T03, 0, 0);
  x->T03[1] = gsl_matrix_get(T03, 0, 1);
  x->T03[2] = gsl_matrix_get(T03, 0, 2);
  x->T03[3] = gsl_matrix_get(T03, 0, 3);
  x->T03[4] = gsl_matrix_get(T03, 1, 0);
  x->T03[5] = gsl_matrix_get(T03, 1, 1); 
  x->T03[6] = gsl_matrix_get(T03, 1, 2);
  x->T03[7] = gsl_matrix_get(T03, 1, 3);
  x->T03[8] = gsl_matrix_get(T03, 2, 0);
  x->T03[9] = gsl_matrix_get(T03, 2, 1);
  x->T03[10] = gsl_matrix_get(T03, 2, 2);
  x->T03[11] = gsl_matrix_get(T03, 2, 3);
  x->T03[12] = gsl_matrix_get(T03, 3, 0);
  x->T03[13] = gsl_matrix_get(T03, 3, 1);
  x->T03[14] = gsl_matrix_get(T03, 3, 2);
  x->T03[15] = gsl_matrix_get(T03, 3, 3);

  x->T04[0] = gsl_matrix_get(T04, 0, 0);
  x->T04[1] = gsl_matrix_get(T04, 0, 1);
  x->T04[2] = gsl_matrix_get(T04, 0, 2);
  x->T04[3] = gsl_matrix_get(T04, 0, 3);
  x->T04[4] = gsl_matrix_get(T04, 1, 0);
  x->T04[5] = gsl_matrix_get(T04, 1, 1); 
  x->T04[6] = gsl_matrix_get(T04, 1, 2);
  x->T04[7] = gsl_matrix_get(T04, 1, 3);
  x->T04[8] = gsl_matrix_get(T04, 2, 0);
  x->T04[9] = gsl_matrix_get(T04, 2, 1);
  x->T04[10] = gsl_matrix_get(T04, 2, 2);
  x->T04[11] = gsl_matrix_get(T04, 2, 3);
  x->T04[12] = gsl_matrix_get(T04, 3, 0);
  x->T04[13] = gsl_matrix_get(T04, 3, 1);
  x->T04[14] = gsl_matrix_get(T04, 3, 2);
  x->T04[15] = gsl_matrix_get(T04, 3, 3);

  x->A01[0] = gsl_matrix_get(A01, 0, 0);
  x->A01[1] = gsl_matrix_get(A01, 0, 1);
  x->A01[2] = gsl_matrix_get(A01, 0, 2);
  x->A01[3] = gsl_matrix_get(A01, 0, 3);
  x->A01[4] = gsl_matrix_get(A01, 1, 0);
  x->A01[5] = gsl_matrix_get(A01, 1, 1);
  x->A01[6] = gsl_matrix_get(A01, 1, 2);
  x->A01[7] = gsl_matrix_get(A01, 1, 3);
  x->A01[8] = gsl_matrix_get(A01, 2, 0);
  x->A01[9] = gsl_matrix_get(A01, 2, 1);
  x->A01[10] = gsl_matrix_get(A01, 2, 2);
  x->A01[11] = gsl_matrix_get(A01, 2, 3);
  x->A01[12] = gsl_matrix_get(A01, 3, 0);
  x->A01[13] = gsl_matrix_get(A01, 3, 1);
  x->A01[14] = gsl_matrix_get(A01, 3, 2);
  x->A01[15] = gsl_matrix_get(A01, 3, 3);

  x->A12[0] = gsl_matrix_get(A12, 0, 0);
  x->A12[1] = gsl_matrix_get(A12, 0, 1);
  x->A12[2] = gsl_matrix_get(A12, 0, 2);
  x->A12[3] = gsl_matrix_get(A12, 0, 3);
  x->A12[4] = gsl_matrix_get(A12, 1, 0);
  x->A12[5] = gsl_matrix_get(A12, 1, 1);
  x->A12[6] = gsl_matrix_get(A12, 1, 2);
  x->A12[7] = gsl_matrix_get(A12, 1, 3);
  x->A12[8] = gsl_matrix_get(A12, 2, 0);
  x->A12[9] = gsl_matrix_get(A12, 2, 1);
  x->A12[10] = gsl_matrix_get(A12, 2, 2);
  x->A12[11] = gsl_matrix_get(A12, 2, 3);
  x->A12[12] = gsl_matrix_get(A12, 3, 0);
  x->A12[13] = gsl_matrix_get(A12, 3, 1);
  x->A12[14] = gsl_matrix_get(A12, 3, 2);
  x->A12[15] = gsl_matrix_get(A12, 3, 3);

  x->A23[0] = gsl_matrix_get(A23, 0, 0);
  x->A23[1] = gsl_matrix_get(A23, 0, 1);
  x->A23[2] = gsl_matrix_get(A23, 0, 2);
  x->A23[3] = gsl_matrix_get(A23, 0, 3);
  x->A23[4] = gsl_matrix_get(A23, 1, 0);
  x->A23[5] = gsl_matrix_get(A23, 1, 1);
  x->A23[6] = gsl_matrix_get(A23, 1, 2);
  x->A23[7] = gsl_matrix_get(A23, 1, 3);
  x->A23[8] = gsl_matrix_get(A23, 2, 0);
  x->A23[9] = gsl_matrix_get(A23, 2, 1);
  x->A23[10] = gsl_matrix_get(A23, 2, 2);
  x->A23[11] = gsl_matrix_get(A23, 2, 3);
  x->A23[12] = gsl_matrix_get(A23, 3, 0);
  x->A23[13] = gsl_matrix_get(A23, 3, 1);
  x->A23[14] = gsl_matrix_get(A23, 3, 2);
  x->A23[15] = gsl_matrix_get(A23, 3, 3);
  
  x->A34[0] = gsl_matrix_get(A34, 0, 0);
  x->A34[1] = gsl_matrix_get(A34, 0, 1);
  x->A34[2] = gsl_matrix_get(A34, 0, 2);
  x->A34[3] = gsl_matrix_get(A34, 0, 3);
  x->A34[4] = gsl_matrix_get(A34, 1, 0);
  x->A34[5] = gsl_matrix_get(A34, 1, 1);
  x->A34[6] = gsl_matrix_get(A34, 1, 2);
  x->A34[7] = gsl_matrix_get(A34, 1, 3);
  x->A34[8] = gsl_matrix_get(A34, 2, 0);
  x->A34[9] = gsl_matrix_get(A34, 2, 1);
  x->A34[10] = gsl_matrix_get(A34, 2, 2);
  x->A34[11] = gsl_matrix_get(A34, 2, 3);
  x->A34[12] = gsl_matrix_get(A34, 3, 0);
  x->A34[13] = gsl_matrix_get(A34, 3, 1);
  x->A34[14] = gsl_matrix_get(A34, 3, 2);
  x->A34[15] = gsl_matrix_get(A34, 3, 3);

  //Free matrices and vectors
  gsl_matrix_free(A01);
  gsl_matrix_free(A12);
  gsl_matrix_free(A23);
  gsl_matrix_free(A34);
  gsl_matrix_free(T02);
  gsl_matrix_free(T03);
  gsl_matrix_free(T04);

  return x;
}
	
// ===============================================================================================
// ===== INVERSE KINEMATICS ======================================================================
// ===============================================================================================
// This function computes inverse kinematics parameters for a desired xyz position (m), and 
// orientation of the end effector.  Returns -1 if invalid configuration.
// ===============================================================================================
inverse_kinematics_t * inverse_kinematics(double * position, double orientation){


  inverse_kinematics_t * x = calloc(1, sizeof(*x));

  //Robot Link Measurements [m]
  double l1 = .113;
  double l2 = .100;
  double l3 = .100;
  double l4 = .113;

  //Link angles [rad]
  double theta1;
  double theta2;
  double theta3;
  double theta4;

  //Intermediate Values
  //Angles
  double Beta;
  double Phi;
  double Psi;

  //Lengths
  double rc;       //Base c is end effector location
  double zw;       //Base w is wrist location
  double rw;

  double temp;
  double theta1i, theta3i;
  int i,j;

  double Solutions[4][4];


  //////////////ELBOW UP/////////////////

  //Calculate Theta1
  if(position[0] == 0 && position[1] >= 0)
    theta1 = 0.0; 
  else if(position[0] == 0 && position[1] < 0)
    theta1 = M_PI;
  else 
    {
      theta1 = (atan2(position[1],position[0])-M_PI/2.0);
    }

  //Bounds Check
  if (theta1 > 3.123)
    theta1 = theta1 - 2.0*M_PI;
  else if(theta1 < -3.123)
    theta1 = theta1 + 2.0*M_PI;

  //Calculate Intermediate Values
  Phi = orientation*(M_PI/180.0);
  rc = sqrt(pow(position[0],2)+pow(position[1],2));
  zw = position[2] + l4 * sin(Phi);
  rw = rc - l4 * cos(Phi);
  printf("zw:   %lf\t rw:   %lf\n",zw,rw);

  //Calculate Theta3
  temp = (pow(zw-l1,2)+pow(rw,2)-pow(l2,2)-pow(l3,2))/(2*l2*l3);
  if (temp > 1 || temp < -1) //Solution only exists if -1<=temp<=1
    {
      printf("IMPOSSIBLE CONFIGURATION:\n");
      return -1;
    }
  else
    {
      theta3 = acos(temp);
    }

  theta3i=theta3; //Used to calculate Elbow Down

  //Calculate Theta2
  Beta = atan2(zw - l1, rw);
  temp = (pow(l3,2)-pow(l2,2)-(pow(zw-l1,2)-pow(rw,2)))/(-2.0*sqrt(pow(zw-l1,2)+pow(rw,2))*l2);
  if (temp > 1 || temp < -1) //Solution only exists if -1<=temp<=1
    {
      printf("IMPOSSIBLE CONFIGURATION:\n");
      return -1;
    }
  else
    {
      Psi = acos(temp);
      if(theta3 >= 0)
	theta2 = -1.0*(M_PI/2.0 - Beta - Psi);
      else
	theta2 = -1.0*(M_PI/2.0 - Beta + Psi);
    }

  //Calculate Theta4
  theta4 = (Phi - theta2 - theta3 + M_PI/2.0);

  //Bounds Check
  if(theta4 > 2.23 || theta4 < -2.23){
    theta1=theta1;
    theta3=-theta3;
    theta2=-theta2;
    theta4 = (Phi - theta2 - theta3 + M_PI/2.0);
  }
	
  //Elbow Up Solutions
  Solutions[0][0] = theta1;
  Solutions[0][1] = theta2;
  Solutions[0][2] = theta3;
  Solutions[0][3] = theta4;

  theta1i = theta1-M_PI;  //Used to Calculate Identical configuration
  if (theta1i > 3.123)
    theta1i = theta1i - 2.0*M_PI;
  else if(theta1i < -3.123)
    theta1i = theta1i + 2.0*M_PI;
	
  Solutions[1][0] = theta1i;
  Solutions[1][1] = -theta2;
  Solutions[1][2] = -theta3;
  Solutions[1][3] = -theta4;

  //////////////////ELBOW DOWN/////////////////

  //Theta 3
  theta3=-theta3i;

  //Theta 2
  if(theta3 >= 0)
    theta2 = -1.0*(M_PI/2.0 - Beta - Psi);
  else
    theta2 = -1.0*(M_PI/2.0 - Beta + Psi);

  //Theta 4
  theta4 = (Phi - theta2 - theta3 + M_PI/2.0);

  //Bounds Check
  if(theta4 > 2.23 || theta4<-2.23){
    theta1=theta1;
    theta3=-theta3;
    theta2=-theta2;
    theta4 = (Phi - theta2 - theta3 + M_PI/2.0);
  }

  //Elbow Down Solutions
  Solutions[2][0] = theta1;
  Solutions[2][1] = theta2;
  Solutions[2][2] = theta3;
  Solutions[2][3] = theta4;

  theta1i = theta1-M_PI;	//Used to Calculate Identical configuration
  if (theta1i > 3.123)	
    theta1i = theta1i - 2.0*M_PI;
  else if(theta1i < -3.123)
    theta1i = theta1i + 2.0*M_PI;
	
  Solutions[3][0] = theta1i;
  Solutions[3][1] = -theta2;
  Solutions[3][2] = -theta3;
  Solutions[3][3] = -theta4;
	

  //CHECK TO SEE IF SOLUTION WITHIN BOUNDS
  for(i=0; i<4; i++)
    {
      if(Solutions[i][0] >3.123 || Solutions[i][0] <-3.123)
	{
	  Solutions[i][0] = 0.0;
	  Solutions[i][1] = 95.0*(M_PI/180.0); //Position will collide
	  Solutions[i][2] = 90.0*(M_PI/180.0);
	  Solutions[i][3] = 90.0*(M_PI/180.0);
	}
      else if(Solutions[i][1] >2.14 || Solutions[i][1] <-2.14)
	{
	  Solutions[i][0] = 0.0;
	  Solutions[i][1] = 95.0*(M_PI/180.0); //Position will collide
	  Solutions[i][2] = 90.0*(M_PI/180.0);
	  Solutions[i][3] = 90.0*(M_PI/180.0);
	}
      else if(Solutions[i][1] >2.2 || Solutions[i][1] <-2.11)
	{
	  Solutions[i][0] = 0.0;
	  Solutions[i][1] = 95.0*(M_PI/180.0); //Position will collide
	  Solutions[i][2] = 90.0*(M_PI/180.0);
	  Solutions[i][3] = 90.0*(M_PI/180.0);
	}
      else if(Solutions[i][1] >2.2 || Solutions[i][1] <-2.23)
	{
	  Solutions[i][0] = 0.0;
	  Solutions[i][1] = 95.0*(M_PI/180.0); //Position will collide
	  Solutions[i][2] = 90.0*(M_PI/180.0);
	  Solutions[i][3] = 90.0*(M_PI/180.0);
	}

    }

  //Set the structure values
  x->q1 [0] = Solutions[0][0];
  x->q1 [1] = Solutions[0][1];
  x->q1 [2] = Solutions[0][2];
  x->q1 [3] = Solutions[0][3];
  
  x->q2 [0] = Solutions[1][0];
  x->q2 [1] = Solutions[1][1];
  x->q2 [2] = Solutions[1][2];
  x->q2 [3] = Solutions[1][3];
  
  x->q3 [0] = Solutions[2][0];
  x->q3 [1] = Solutions[2][1];
  x->q3 [2] = Solutions[2][2];
  x->q3 [3] = Solutions[2][3];
  
  x->q4 [0] = Solutions[3][0];
  x->q4 [1] = Solutions[3][1];
  x->q4 [2] = Solutions[3][2];
  x->q4 [3] = Solutions[3][3];

  return x;
}

// ===============================================================================================
// ===== REXARM VXO ARM ==========================================================================
// ===============================================================================================
// Creates rendering object for rexarm.  Arm is green if it is not in an error state, and joint
// turns red if it is in an error state.
// ===============================================================================================
vx_object_t * rexarm_vxo_arm(float errors[4], double angles[4], float arm_color[4], float alpha){

	double negangles[4];
	for(int i=0; i < 4; i++)
		negangles[i] = angles[i] * -1.0;

	forward_kinematics_t *f = forward_kinematics(negangles);
	float *colors[4];

	if(errors[0] == 1)
		colors[0] = vx_red;
	else
		colors[0] = vx_green;
	if(errors[1] == 1)
		colors[1] = vx_red;
	else
		colors[1] = vx_green;
	if(errors[2] == 1)
		colors[2] = vx_red;
	else
		colors[2] = vx_green;
	if(errors[3] == 1)
		colors[3] = vx_red;
	else
		colors[3] = vx_green;

	double link2x=f->A01[3];
	double link2y=f->A01[11];
	double link2ox=f->A01[3];
	double link2oz=f->A01[11];
	double link2oy=f->A01[7];
	double link2z=link2oz+.05;

	if(angles[1] != 0.0)
		link2x = link2x + .05 * sin(-angles[0]*(M_PI/180.0));
	if(angles[1] != 0.0)
		link2y = link2y+ .05 * cos(-angles[0]*(M_PI/180.0));
	if(angles[1] != 0.0)
		link2z = 1.0*(link2z+.05*sin((90.0+angles[1])*(M_PI/180.0)));

	vx_object_t *vxo_link_base = vxo_chain(
					       vxo_chain (vxo_mat_rotate_z(3.14),
							  vxo_mat_translate3(0.0, 0.0, .07171),
							  vxo_mat_scale3 (.021, .021, .0005),
					       		  vxo_cylinder (vxo_mesh_style (colors[0]))),
					       vxo_chain (vxo_mat_translate3(0.0, 0.0, .03573), 
							  vxo_mat_scale3(.040, .035, .07171),
							  vxo_box (vxo_mesh_style (colors[0]))),
					       vxo_chain (vxo_mat_translate3(0.0, 0.0, .09546),
							  vxo_mat_rotate_z(angles[0]*(M_PI/180.0)),
							  vxo_mat_scale3(.037, .037, .038),
							  vxo_box (vxo_mesh_style (colors[0])))
					       );

	vx_object_t *vxo_link2 = vxo_chain(vxo_chain (vxo_mat_translate3(link2ox, link2oy, link2oz),
						      vxo_mat_scale3 (.0515, .0355, .027),
						      vxo_mat_rotate_z(angles[0]*(M_PI/180.0)),
						      vxo_mat_rotate_x(M_PI/2),
						      vxo_mat_rotate_y(M_PI/2),
						      vxo_cylinder (vxo_mesh_style (colors[1]))),
					   vxo_chain (vxo_mat_translate3(link2ox, link2oy, link2oz),
						      vxo_mat_rotate_z(angles[0]*(M_PI/180.0)),
							vxo_mat_rotate_x(-angles[1]*(M_PI/180.0)),
						      vxo_mat_translate3(0.0, 0.0, .0505),
						      vxo_mat_scale3(.0355, .0355, .101),
						      vxo_box (vxo_mesh_style (colors[1])))
					   );
	vx_object_t *vxo_link3 = vxo_chain(vxo_chain (vxo_mat_translate3(link2ox, link2oy, link2oz),
						      vxo_mat_rotate_z(angles[0]*(M_PI/180.0)),
						      vxo_mat_rotate_x(-angles[1]*(M_PI/180.0)),
						      vxo_mat_translate3(0.0, 0.0, .101),
						       vxo_mat_scale3(.0515, .0355, .027),
						       vxo_mat_rotate_x(M_PI/2),
						       vxo_mat_rotate_y(M_PI/2), 
						       vxo_cylinder (vxo_mesh_style (colors[2]))),
					   vxo_chain (vxo_mat_translate3(link2ox, link2oy, link2oz),
						       vxo_mat_rotate_z(angles[0]*(M_PI/180.0)),
						       vxo_mat_rotate_x(-angles[1]*(M_PI/180.0)),
						       vxo_mat_translate3(0.0, 0.0, .101),
						       vxo_mat_rotate_x(-angles[2]*(M_PI/180.0)),
						       vxo_mat_translate3(0.0, 0.0, .0505),
						       vxo_mat_scale3(.0355, .0355, .101),
						       vxo_box (vxo_mesh_style (colors[2])))
					   );
	vx_object_t *vxo_link4 = vxo_chain(vxo_chain (vxo_mat_translate3(link2ox, link2oy, link2oz),
						       vxo_mat_rotate_z(angles[0]*(M_PI/180.0)),
						       vxo_mat_rotate_x(-angles[1]*(M_PI/180.0)),
						       vxo_mat_translate3(0.0, 0.0, .101),
						       vxo_mat_rotate_x(-angles[2]*(M_PI/180)),
						       vxo_mat_translate3(0.0, 0.0, .101),
						       vxo_mat_scale3(.0505, .0267, .0267),
						       vxo_mat_rotate_x(M_PI/2),
						       vxo_mat_rotate_y(M_PI/2),
						       vxo_cylinder (vxo_mesh_style (colors[3]))),
					    vxo_chain (vxo_mat_translate3(link2ox, link2oy, link2oz),
						       vxo_mat_rotate_z(angles[0]*(M_PI/180.0)),
						       vxo_mat_rotate_x(-angles[1]*(M_PI/180.0)),
						       vxo_mat_translate3(0.0, 0.0, .101),
						       vxo_mat_rotate_x(-angles[2]*(M_PI/180.0)),
						       vxo_mat_translate3(0.0, 0.0, .101),
						       vxo_mat_rotate_x(-angles[3]*(M_PI/180.0)),
						       vxo_mat_translate3(0.0, 0.0, .020),
						       vxo_mat_scale3(.0505, .03475, .04375),
						       vxo_box (vxo_mesh_style (colors[3]))),
					    vxo_chain (vxo_mat_translate3(link2ox, link2oy, link2oz),
						       vxo_mat_rotate_z(angles[0]*(M_PI/180.0)),
						       vxo_mat_rotate_x(-angles[1]*(M_PI/180.0)),
						       vxo_mat_translate3(0.0, 0.0, .101),
						       vxo_mat_rotate_x(-angles[2]*(M_PI/180.0)),
						       vxo_mat_translate3(0.0, 0.0, .101),
						       vxo_mat_rotate_x(-angles[3]*(M_PI/180.0)),
						       vxo_mat_translate3(0.0, 0.0, .0685),
						       vxo_mat_scale3(.006, .006, .057),
						       vxo_cylinder (vxo_mesh_style (colors[3])))
					   //vxo_mat_copy_from_doubles(f->T04)
					   );
	vx_object_t *vxo_arm = vxo_chain(vxo_link_base, vxo_link2, vxo_link3, vxo_link4);
	return vxo_arm;
} 
