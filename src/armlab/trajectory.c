// ----------------------------------------------------------------------------------------------
// ----- TRAJECTORY.C ---------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// This file implements trajectory planning.
// ----------------------------------------------------------------------------------------------

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include "trajectory.h"

// ===============================================================================================
// ===== TRAJECTORY PLANNING =====================================================================
// ===============================================================================================
// This function takes in initial and final values (e.g. from teach-and-repeat) and speed to
// calculate coefficients for smooth cubic spline trajectory planning.
// ===============================================================================================
Trajectory_Planning_t * trajectory_planning(double * q0, double t0, double * qf, double tf, double speed){

  //Initialize Structure
  Trajectory_Planning_t * x = (struct _Trajectory_Planning_t*) malloc(sizeof(struct _Trajectory_Planning_t));

  //Initialize matrices
  gsl_vector *A, *Q;
  gsl_matrix *T, *cov;
		
  //Initialize contants and temp variables
  double chisq;
  double v = 0;                 //Velocity variables
  tf = ((tf - t0)/speed);	//Calculates delta_t
  t0 = 0;              	        //Time variables
  double a0, a1, a2, a3;        //Constants from position and velocity equations

  x->delta_t = tf;

  //Allocate Memory                   			
  T = gsl_matrix_alloc(4, 4);
  Q = gsl_vector_alloc(4);

  A = gsl_vector_alloc (4);
  cov = gsl_matrix_alloc (4, 4);

  	//Populate T
  	gsl_matrix_set(T, 0, 0, 1);
  	gsl_matrix_set(T, 0, 1, t0);
  	gsl_matrix_set(T, 0, 2, pow(t0,2));
  	gsl_matrix_set(T, 0, 3, pow(t0,3));
  	gsl_matrix_set(T, 1, 0, 0);
  	gsl_matrix_set(T, 1, 1, 1);
  	gsl_matrix_set(T, 1, 2, 2*t0);
  	gsl_matrix_set(T, 1, 3, 3*pow(t0,2));
  	gsl_matrix_set(T, 2, 0, 1);
  	gsl_matrix_set(T, 2, 1, tf);
  	gsl_matrix_set(T, 2, 2, pow(tf,2));
  	gsl_matrix_set(T, 2, 3, pow(tf,3));
  	gsl_matrix_set(T, 3, 0, 0);
  	gsl_matrix_set(T, 3, 1, 1);
  	gsl_matrix_set(T, 3, 2, 2*tf);
  	gsl_matrix_set(T, 3, 3, 3*pow(tf,2));

  	gsl_multifit_linear_workspace * work  = gsl_multifit_linear_alloc (4, 4);
  	
	//Calculate A's
	
	//Joint 0
    		//Populate Q = [q0, v0, qf, vf]
 		gsl_vector_set(Q, 0, q0[0]);
  		gsl_vector_set(Q, 1, 0);
  		gsl_vector_set(Q, 2, qf[0]);
  		gsl_vector_set(Q, 3, 0);
  	
     	//Compute A matrix
     	gsl_multifit_linear (T, Q, A, cov, &chisq, work);
     
     	for(int i = 0; i < 4; i++)
     		x->A0[i] = gsl_vector_get(A,i);
     
     //Joint1
     	//Populate Q = [q0, v0, qf, vf]
 		gsl_vector_set(Q, 0, q0[1]);
  		gsl_vector_set(Q, 1, 0);
  		gsl_vector_set(Q, 2, qf[1]);
  		gsl_vector_set(Q, 3, 0);
  	
     	//Compute A matrix
     	gsl_multifit_linear (T, Q, A, cov, &chisq, work);
     
     	for(int i = 0; i < 4; i++)
     		x->A1[i] = gsl_vector_get(A, i);

     //Joint2
     	//Populate Q = [q0, v0, qf, vf]
 		gsl_vector_set(Q, 0, q0[2]);
  		gsl_vector_set(Q, 1, 0);
  		gsl_vector_set(Q, 2, qf[2]);
  		gsl_vector_set(Q, 3, 0);
  	
     	//Compute A matrix
     	gsl_multifit_linear (T, Q, A, cov, &chisq, work);
     
     	for(int i = 0; i < 4; i++)
     		x->A2[i] = gsl_vector_get(A, i);
     		
     //Joint3
     	//Populate Q = [q0, v0, qf, vf]
 		gsl_vector_set(Q, 0, q0[3]);
  		gsl_vector_set(Q, 1, 0);
  		gsl_vector_set(Q, 2, qf[3]);
  		gsl_vector_set(Q, 3, 0);
  	
     	//Compute A matrix
     	gsl_multifit_linear (T, Q, A, cov, &chisq, work);
     
     	for(int i = 0; i < 4; i++)
     		x->A3[i] = gsl_vector_get(A, i);
  
  	gsl_matrix_free(T);
  	gsl_vector_free(Q);
  	gsl_vector_free(A);
  	gsl_matrix_free(cov);

  	gsl_multifit_linear_free (work);
	
	return x;
}

