//-----------------------------------------------------------------------------------------
//----- ARMLAB.C --------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
//-----------------------------------------------------------------------------------------
// This file contains the main code for sending and receiving commands to the rexarm. It
// also implements the GUI and facilitates user interaction with the rexarm.
//-----------------------------------------------------------------------------------------

// ========================================================================================
// ===== DEPENDENCIES =====================================================================
// ========================================================================================
// Armlab.h contains all structures and defined constants- see for further documentation
// ========================================================================================

#include "armlab.h"
#include <sched.h>
// ========================================================================================
// ===== EVENT LISTENERS ==================================================================
// ========================================================================================
// The following functions listen for four different types of events on the GUI
// 	1. Parameter changes in GUI interactive features
//	2. Mouse clicks on the GUI
//	3. Key clicks on the keyboard
//	4. Touch events if implemented on a touchscreen
// ========================================================================================

// ========================================================================================
// ===== TEMPLATE MATCHING THREADS ==========================================================
// The cylinder template matching code takes in the state, which contains the x and y 
// coordinates of the cylinder template. It passes this to the template_matcher.c to find
// all instances of cylinders in the image frame. Finally, it passes back the list of 
// cylinders it found.

// The ring template matching code performs the same as the cylinder matching code.
// ==========================================================================================
varray_t * template_cylinder_thread(state_t *state)
{
        image_u32_t *image = image_u32_copy(state->image);

        int template_x_corner = ((state->cylinderXTemplate + 1)*image->width)/2;
        int template_y_corner = ((-state->cylinderYTemplate*image->width) + image->height)/2;
        image_u32_t * template = image_u32_create(96, 88);
	int template_index_counter = 0;	
	
	//Loop through the image to set the template buffer
	for(int y = 0; y < template->height; y++){
		for(int x = 0; x < template->width; x++){
			int image_index = ((y+template_y_corner) - 1)*image->stride + ((template_x_corner + x));
			int template_index = (y*template->stride) + x;
			template->buf[template_index] = image->buf[image_index];
		}
	}
	
	//Set the template variable in state so that we can access it everywhere else
	image_u32_t *tempcopy = image_u32_copy(template);
	state->cyltempl = image_u32_copy(tempcopy);
	
	//Call the template_matcher_find and put the results in a zarray
        varray_t *cylindersInPixels = template_matcher_find(image, template, 42000, 0, 2.0);
        for(int i=0; i < zarray_size(cylindersInPixels); i++){
                double *p = varray_get(cylindersInPixels, i);
        }
        
        return cylindersInPixels;
}

varray_t * template_ring_thread(state_t *state)
{
        image_u32_t *image = image_u32_copy(state->image);
        int template_x_corner = ((state->ringX + 1)*image->width)/2;
        int template_y_corner = ((-state->ringY*image->width) + image->height)/2;
        image_u32_t * template = image_u32_create(75, 75);
        int template_index_counter = 0;
	
	//Loop through the image to set the template buffer
        for(int y = 0; y < template->height; y++){
                for(int x = 0; x < template->width; x++){
                        int image_index = ((y+template_y_corner) - 1)*image->stride + ((template_x_corner + x));
                        int template_index = (y*template->stride) + x;
                        template->buf[template_index] = image->buf[image_index];
                }
        }

	//Set the ring template variable in state so that we can access it again
        image_u32_t *tempcopy = image_u32_copy(template);
        state->ringtempl = image_u32_copy(tempcopy);
        
	//Call the template matcher_find and put the results in a varray
        varray_t *ringsInPixels = template_matcher_find(image, template, 21000, 0, 2.0);
        for(int i=0; i < varray_size(ringsInPixels); i++){
                double *p = varray_get(ringsInPixels, i);
       	} 
        return ringsInPixels;
}

// ========================================================================================
// ===== PARAMETER LISTENER ===============================================================
// ========================================================================================
// This function is handed to the parameter gui (via a parameter listener)
// and handles events coming from the parameter gui. The parameter listener
// also holds a void* pointer to "impl", which can point to a struct holding
// state, etc if need be.
// ========================================================================================

static void
my_param_changed (parameter_listener_t *pl, parameter_gui_t *pg, const char *name)
{
    /* DECLARATIONS */

    int i;

    state_t *state_temp;	   
    state_temp = pl->impl;

    state_temp->joint1_command = pg_gd (pg, "axis1");
    state_temp->joint2_command = pg_gd (pg, "axis2");
    state_temp->joint3_command = pg_gd (pg, "axis3");
    state_temp->joint4_command = pg_gd (pg, "axis4");
    state_temp->speed_value =  pg_gd_boxes (pg, "speedBox");
    state_temp->torque_value = pg_gd_boxes (pg, "torqueBox");

    double servo_command[4]={state_temp->joint1_command, state_temp->joint2_command, state_temp->joint3_command, state_temp->joint4_command};

    /* Check to see what parameter has changed and change the appropriate variables in state_temp */

    if (0==strcmp ("axis1", name)){
        printf ("Axis 1 = %lf\n", pg_gd (pg, name));
	servo_command[0] = pg_gd (pg, name);
	state_temp->joint1_command = servo_command[0];
    }
    else if (0==strcmp ("axis2", name)){
        printf ("Axis 2 = %lf\n", pg_gd (pg, name));
        servo_command[1] = pg_gd (pg, name);
	state_temp->joint2_command = servo_command[1];
    }
    else if (0==strcmp ("axis3", name)){
	printf("Axis 3 = %lf\n", pg_gd (pg, name));
        servo_command[2] = pg_gd (pg, name);
	state_temp->joint3_command = servo_command[2];
    }
    else if (0==strcmp ("axis4", name)){
	printf("Axis 4 = %lf\n", pg_gd (pg, name));
        servo_command[3] = pg_gd (pg, name);
	state_temp->joint4_command = servo_command[3];
    }
    else if (0==strcmp ("speedBox", name)){
	state_temp->speed_value = pg_gd_boxes (pg, name);
    }
    else if (0==strcmp ("torqueBox", name)){
	state_temp->torque_value = pg_gd_boxes (pg, name);
    }
    else if (0==strcmp ("cylinderBox", name)){
	if(state_temp->calibrated == 1){
		state_temp->drawCylinder = pg_gb (pg, name);
		printf("Cylinder Box = %d\n", pg_gb (pg, name));
		if(pg_gb (pg, name) == 1){
			pg_sb (pg, "ringBox", false);
			state_temp->drawRing = 0;
			pg_sb (pg, "calibrateBox", false);
			state_temp->calibrate = 0;
			pg_sb (pg, "waypointsBox", false);
			state_temp->getWaypoints = 0;
			pg_sb (pg, "pathBox", false);
			state_temp->planPath = 0;
			pg_sb (pg, "executeBox", false);
			state_temp->executeWaypoints = 0;
		}
	}
    }
    else if (0==strcmp ("ringBox", name)){
	if(state_temp->calibrated == 1){
		state_temp->drawRing = pg_gb (pg, name);
		printf("Ring Box = %d\n", pg_gb (pg, name));
		if(pg_gb (pg, name) == 1){
			pg_sb (pg, "cylinderBox", false);
			state_temp->drawCylinder = 0;
			pg_sb (pg, "calibrateBox", false);
			state_temp->calibrate = 0;
			pg_sb (pg, "waypointsBox", false);
			state_temp->getWaypoints = 0;
			pg_sb (pg, "pathBox", false);
			state_temp->planPath = 0;
			pg_sb (pg, "executeBox", false);
			state_temp->executeWaypoints = 0;
		}
	}
    }
    else if (0==strcmp("calibrateBox", name)){
	state_temp->calibrate = pg_gb (pg, name);
	printf("Calibrate Box= %d\n", pg_gb (pg, name));
	if(pg_gb (pg, name) == 1){
		pg_sb (pg, "cylinderBox", false);
		state_temp->drawCylinder = 0;
		pg_sb (pg, "ringBox", false);
		state_temp->drawRing = 0;
		pg_sb (pg, "waypointsBox", false);
		state_temp->getWaypoints = 0;
		pg_sb (pg, "pathBox", false);
		state_temp->planPath = 0;
		pg_sb (pg, "executeBox", false);
		state_temp->executeWaypoints = 0;
	}
    }
    else if(0==strcmp("waypointsBox", name)){
	state_temp->getWaypoints = pg_gb (pg, name);
	printf("Waypoint Box=%d\n", pg_gb (pg, name));
	if(pg_gb (pg, name) == 1){
		pg_sb(pg, "cylinderBox", false);
		state_temp->drawCylinder = 0;
		pg_sb(pg, "ringBox", false);
		state_temp->drawRing = 0;
		pg_sb(pg, "calibrateBox", false);
		state_temp->calibrate = 0;
		pg_sb (pg, "planBox", false);
		state_temp->planPath = 0;
		pg_sb(pg, "executeBox", false);
		state_temp->executeWaypoints = 0;
		state_temp->worldtime = utime_now()/1000000;
		if(varray_size(state_temp->waypointsList) != 0){
			for(int i=0; i < varray_size(state_temp->waypointsList); i++)
				varray_remove(state_temp->waypointsList, i);
		}
	}
    }
    else if(0==strcmp("pathBox", name)){
	state_temp->planPath = pg_gb(pg, name);
	printf("Plan Box=%d\n", pg_gb (pg, name));
	if(pg_gb (pg, name) == 1){
		pg_sb(pg, "cylinderBox", false);
		state_temp->drawCylinder = 0;
		pg_sb(pg, "ringBox", false);
		state_temp->drawRing = 0;
		pg_sb(pg, "calibrateBox", false);
		state_temp->calibrate = 0;
		pg_sb(pg, "waypointsBox", false);
		state_temp->getWaypoints = 0;
		pg_sb(pg, "executeBox", false);
		state_temp->executeWaypoints = 0;
		for(int i=0; i < varray_size(state_temp->waypointsList);i++)
			varray_remove(state_temp->waypointsList, i);
	}
    }
    else if(0==strcmp("executeBox", name)){
	state_temp->executeWaypoints = pg_gb(pg, name);
	printf("Execute Box=%d\n", pg_gb(pg, name));
	if(pg_gb (pg, name) == 1){
		pg_sb(pg, "cylinderBox", false);
                state_temp->drawCylinder = 0;
                pg_sb(pg, "ringBox", false);
                state_temp->drawRing = 0;
                pg_sb(pg, "calibrateBox", false);
                state_temp->calibrate = 0;
                pg_sb(pg, "waypointsBox", false);
                state_temp->getWaypoints = 0;
		pg_sb(pg, "pathBox", false);
		state_temp->planPath = 0;
	}
    }

    else
        printf ("%s changed\n", name);

    for(i=0; i < NUM_SERVOS; i++){
	servo_command[i] = (servo_command[i] * PI)/180;
    }    
}

// =========================================================================================
// ===== MOUSE EVENT =======================================================================
// =========================================================================================
// This function is called when a mouse click is detected on the GUI. It handles calibrating
// the screen for proper display and readings. It also creates new rings and cylinders as
// the user defines their positions.
// =========================================================================================

static int
mouse_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_mouse_event_t *mouse)
{
    /* DECLARATIONS */
    state_t *state = vxeh->impl;

    double coords[2] = {0.0, 0.0};
    double dummyPixels[2] = {0,0};
    double dummyWorld[2] = {0,0};
    double dummyR[2] = {0,0};
    affine_calibration_t *x;
    double *xpoint, *ypoint;
    cylinders_t *newCyl = calloc(1, sizeof(*newCyl));
    rings_t *newRing = calloc(1, sizeof(*newRing));

    // vx_camera_pos_t contains camera location, field of view, etc
    // vx_mouse_event_t contains scroll, x/y, and button click events

    if ((mouse->button_mask & VX_BUTTON1_MASK) &&
        !(state->last_mouse_event.button_mask & VX_BUTTON1_MASK)) {

        vx_ray3_t ray;
        vx_camera_pos_compute_ray (pos, mouse->x, mouse->y, &ray);
        double ground[3];
        vx_ray3_intersect_xy (&ray, 0, ground);

	printf("MOUSE CLICKED AT: %lf\t%lf, GROUND: %lf, %lf\n", mouse->x, mouse->y, ground[0], ground[1]);
	coords[0] = ground[0];
	xpoint = coords;	
	coords[1] = ground[1];
	ypoint = xpoint + 1;

	double z0, z1, z2, z3, z4, z5, z6, z7;
	double zeroarray[2] = {0.0,0.0};
        double xarray[2] = {1.0,0.0};

	// If the camera has not yet been calibrated, don't do anything else

	if(state->mouseclicks < 4){
		zarray_add(state->pairs, xpoint);
		zarray_add(state->pairs, ypoint);
		state->drawCylinder = false;
		state->drawCylinder = false;
		state->drawCoords = false;
		state->drawWhenClicked = false;
	}
	zarray_get(state->pairs, 0, &z0);
	zarray_get(state->pairs, 1, &z1);
	zarray_get(state->pairs, 2, &z2);
	zarray_get(state->pairs, 3, &z3);
	zarray_get(state->pairs, 4, &z4);
	zarray_get(state->pairs, 5, &z5);
	zarray_get(state->pairs, 6, &z6);
	zarray_get(state->pairs, 7, &z7);

	if(state->calibrate == 0 && state->calibrated == 0){
		state->mouseclicks = 0;
	}
	else
		state->mouseclicks += 1;

	//If last mouse click was recorded to calibrate, set the calibration values
	if(state->mouseclicks == 4){
		x=affine_calibration_create(state->pairs);
		//Calibrate the camera based on the screen clicks
		affine_calibration_w2p(x, zeroarray, state->worldOrigin);
		affine_calibration_w2p(x, xarray, state->worldvector);
		state->rotation = atan2(state->worldvector[1] - state-> worldOrigin[1], state->worldvector[0] - state -> worldOrigin[0]);
		state->scale = sqrt(pow(state->worldvector[0]-state->worldOrigin[0],2)+pow(state->worldvector[1]-state->worldOrigin[1],2));
		state->drawCoords = true;
		state->calibrated = 1;
	}
	
	//If more mouse clicks, check to see whether we should draw rings or cylinders
	if(state->mouseclicks > 4){
		//Draw Cylinder
		if(state->drawCylinder == true){
			x=affine_calibration_create(state->pairs);
			dummyPixels[0] = ground[0];
			dummyPixels[1] = ground[1];  
			// newCyl->xpos = ground[0]+.07;
			// newCyl->ypos = ground[1]-.07;
			newCyl->xpos = ground[0];
			newCyl->ypos = ground[1];
			affine_calibration_p2w(x, ground, dummyPixels);
			newCyl->zpos = 0.0;
			/*************************************
			 //TEMPLATE MATCHING
			state->cylinderXTemplate = ground[0];
			state->cylinderYTemplate = ground[1];
		      	image_u32_t *im = image_u32_copy(state->image);
			varray_t *cyls = template_cylinder_thread(state);
			varray_add(cyls, newCyl);

	        	for(int i=0; i < varray_size(cyls); i++){
				if(varray_size(cyls) < 0)
					printf("ERROR\n");
				else{
					double *c = varray_get(cyls, i);
					cylinders_t *newCyl = calloc(1, sizeof(*newCyl));
					varray_add(state->cylinderList, newCyl);
					newCyl->xworld = c[0];
					newCyl->yworld = c[1];
					newCyl->zworld = 0.0;
				}
			}
			/*************************************/
			varray_add(state->cylinderList, newCyl);
			affine_calibration_p2w(x, ground, dummyWorld);
			newCyl->xworld = dummyWorld[0];
			newCyl->yworld = dummyWorld[1];
			newCyl->zworld = 0.0;
		}
		//Draw Ring
		else if(state->drawRing == true){
			x=affine_calibration_create(state->pairs);
			dummyPixels[0] = mouse->x;
			dummyPixels[1] = mouse->y;
			affine_calibration_p2w(x, dummyPixels, dummyWorld);

			// newRing->xpos = ground[0]+.07;
			// newRing->ypos = ground[1]-.07;
			newRing->xpos = ground[0];
			newRing->ypos = ground[1];
			newRing->zpos = 0.0;
			//varray_add(state->ringList, newRing);
			/****************************************
			//TEMPLATE MATCHING
			state->ringX = ground[0];
			state->ringY = ground[1];
			varray_t *rings =template_ring_thread(state);

			for(int i=0; i < varray_size(rings); i++){
				double *r = varray_get(rings, i);
				rings_t *newRing = calloc(1, sizeof(*newRing));
				varray_add(state->ringList, newRing);
				newRing->xworld = r[0];
				newRing->yworld = r[0];
				newRing->zworld = 0.0;
			}
			/***************************************/
			
			varray_add(state->ringList, newRing);
			affine_calibration_p2w(x, ground, dummyWorld);
			newRing->xworld = dummyWorld[0];
			newRing->yworld = dummyWorld[1];
			newRing->zworld = 0.0;
		}
	}

	state->drawWhenClicked = true;
    }

    // store previous mouse event to see if the user *just* clicked or released
    state->last_mouse_event = *mouse;

    return 0;
}

// =======================================================================================
// ===== KEY EVENT =======================================================================
// =======================================================================================
// This function is called when a key event is recorded. It handles storing waypoints and
// sending a waypoint list to the rexarm to execute.
// NOTE: Don't physically send commands to the arm in this function. There are only two
//       places where commands are actually sent to the arm- in the command loop and
//       when the GUI first starts. When you want to send commands to the arm, set the
//       variables state->joint1_command...state->joint4_command to the desired position
//       for each joint. The command loop is constantly checking these variables for 
//       changes and updating the commands to the arm.
// =======================================================================================

static int
key_event (vx_event_handler_t *vxeh, vx_layer_t *vl, vx_key_event_t *key)
{
    /* DECLARATIONS */

    state_t *state = vxeh->impl;

    int sizeWaypoints = 0;
    double tf;
    double q[4];
    double v[4];  
    dijkstra_graph_t *graph;
    int *possible_graph;
    dynamixel_command_list_t cmds;
    cmds.len = 4;
    cmds.commands = malloc(sizeof(dynamixel_command_t)*4);
    forward_kinematics_t *dh = calloc(1, sizeof(*dh));
    zarray_t *timeList = zarray_create(sizeof(double));

  //Only want to record key release 
  if(key->released == true){
    
  	double posi[4];
  	double posf[4];
	int firstRing = 0;
    
	//If we're recording waypoints, store the new waypoint in a structure and add it to the waypoint list
    	if(state-> getWaypoints == true){
    		waypoints_t *newWaypoint = calloc(1, sizeof(*newWaypoint));
    	
    		newWaypoint->joint0pos = state->joint1_value;
    		newWaypoint->joint1pos = state->joint2_value;
    		newWaypoint->joint2pos = state->joint3_value;
    		newWaypoint->joint3pos = state->joint4_value;
    		newWaypoint->t = (utime_now()/1000000)-state->worldtime; 
    	
		double q[4] = {newWaypoint->joint0pos, newWaypoint->joint1pos, newWaypoint->joint2pos, newWaypoint->joint3pos};

		newWaypoint->joint0pos = q[0];
		newWaypoint->joint1pos = q[1];
		newWaypoint->joint2pos = q[2];
		newWaypoint->joint3pos = q[3];
	
    		varray_add(state-> waypointsList, newWaypoint); //waypoints list
    	}
	
	//This will be set when the checkbox is checked
        else if(state->planPath == true){
		state->doingPathPlanning = true;
		double *q = malloc(sizeof(double)*4);
		if(firstRing == 1){
			q[0] = state->joint1_value;
			q[1] = state->joint2_value;
			q[2] = state->joint3_value;
			q[3] = state->joint4_value;
		}
		else{
			q[0] = 0.0;
			q[1] = 0.0;
			q[2] = 90.0; 
			q[3] = 0.0;
			firstRing = 1;
		}
		rings_t *ringToGoTo;
		rings_t *finalRingGoal;
	
		//If the waypoints list still isn't clear, make sure it's clear before we add new waypoints
		if(varray_size(state->waypointsList) != 0){
			for(int i = 0; i < varray_size(state->waypointsList); i++)
				varray_remove(state->waypointsList, i);
		}
		
		//If there is a ring in the list, plan a path to it's center, which is stored as a ring structure in the ringList in state	
		if(varray_size(state->ringList) != 0){


		  varray_t *waypointsTemp = varray_create();
		for(int z=0; z < varray_size(state->ringList); z++){
			//Get the first ring in the list
		  ringToGoTo = varray_get(state->ringList, z);
		  finalRingGoal = varray_get(state->ringList, z);
		  if(zarray_size(timeList)>0){
			int size = zarray_size(timeList);
			for(int i=0; i < size; i++)
				zarray_remove_index(timeList, i, false);
		  }
		  printf("Constructing Roadmap\n");
		  if(z == 0){
		    // Constructing Roadmap
		    graph = roadmap_construction(NODES, NEIGHBOURS, state->cylinderList, state->ringList, state);
		  }
		  printf("Constructed Roadmap\n");
			//Call forward kinematics to get the transform matrices
			dh = forward_kinematics(q);
		
			//Call the path planning function on the ring we want, give it the transform matrices and all the obstacles

			double *qf = malloc(sizeof(double)*4);
	
			double dummy[3] = {finalRingGoal->xworld, finalRingGoal->yworld, finalRingGoal->zworld};
			inverse_kinematics_t *ik = inverse_kinematics(dummy, 45.0);

			if(ik == -1){
				qf[0] = 0.0;
				qf[1] = 0.0;
				qf[2] = 90.0;
				qf[3] = 0.0;
			}
			else{
				qf[0] = ik->q1[0]*(180.0/M_PI);
				qf[1] = ik->q1[1]*(180.0/M_PI);
				qf[2] = ik->q1[2]*(180.0/M_PI);
				qf[3] = ik->q1[3]*(180.0/M_PI);
			}
	        
			possible_graph = solve_query(q, qf, NEIGHBOURS, graph, state);
			printf("RETURNED FROM SOLVE QUERY\n");
			while(possible_graph == -1){
				printf("ERROR, RECREATING ROADMAP\n");
				dijkstra_destroy(graph);
				dijkstra_graph_t *graph = roadmap_construction(NODES, NEIGHBOURS, state->cylinderList, state->ringList, state);
				possible_graph = solve_query(q, qf, NEIGHBOURS, graph, state);
				state->pathSize = 0;
			}

			// FIRST FROM PF TO INITIAL MAP NODE   
			printf("USING PF FOR FIRST WAYPOINT\n");
			 
			double *go = varray_get(state->qList,possible_graph[0]);
			forward_kinematics_t *f = forward_kinematics(go);
			ringToGoTo-> xworld = f->position[0];
			ringToGoTo-> yworld = f->position[1];
			ringToGoTo-> zworld = f->position[2];
			free(f);
			double epsilon = 0.01;
			int pflag = 0;

			double* start = malloc(sizeof(double)*4);
			start[0] = 0.0;
			start[1] = 0.0;
			start[2] = 90.0;
			start[3] = 0.0;
			waypointsTemp = path_plan_create(q, ringToGoTo, state->cylinderList, state->ringList, epsilon, pflag);
			if (waypointsTemp == -1){
			  double dummyW[4] = {0.0,0.0,90.0,0.0};
			  waypoints_t *newWaypoint = calloc(1, sizeof(*newWaypoint));
			  newWaypoint->joint0pos = dummyW[0];
			  newWaypoint->joint1pos = dummyW[1];
			  newWaypoint->joint2pos = dummyW[2];
			  newWaypoint->joint3pos = dummyW[3];
			  newWaypoint->t = 0;
			  double time = 0;
			  zarray_add(timeList, &time);
			  varray_add(state->waypointsList, newWaypoint);
			  varray_add(state->reverseList, newWaypoint);		
			}
			else {
			  for(int d=0; d < varray_size(waypointsTemp); d++){
			    int t = 0;
			    double *dummyW = varray_get(waypointsTemp, d);
			    waypoints_t *newWaypoint = calloc(1, sizeof(*newWaypoint));
			    newWaypoint->joint0pos = dummyW[0];
			    newWaypoint->joint1pos = dummyW[1];
			    newWaypoint->joint2pos = dummyW[2];
			    newWaypoint->joint3pos = dummyW[3];
			    newWaypoint->t = d+t;
			    double time = d+t;
			    zarray_add(timeList, &time);
			    varray_add(state->waypointsList, newWaypoint);
			    varray_add(state->reverseList, newWaypoint);
			  }
			}

			int d;
			for (int t = 0; t < state->pathSize; t++){

			  double *v = varray_get(state->qList, possible_graph[t]);
			  if (t < state->pathSize - 1){
			  double *go = varray_get(state->qList,possible_graph[t+1]);
			  forward_kinematics_t *f = forward_kinematics(go);
			  ringToGoTo-> xworld = f->position[0];
			  ringToGoTo-> yworld = f->position[1];
			  ringToGoTo-> zworld = f->position[2];
			  free(f);
			  }
			  double epsilon = 0.01;
			  int pflag = 0;

			  // Use Trajectory planner in map
			  if ((t >= 0) && (t < (state->pathSize)-1)) {
			    // USE TRAJECTORY
			      printf("USING TRAJECTORY\n");
			    Trajectory_Planning_t *x = (struct _Trajectory_Planning_t*)malloc(sizeof(struct _Trajectory_Planning_t));
			    int n = t + 1;
			    double* first = varray_get(state->qList,possible_graph[t]);
			    double* next = varray_get(state->qList, possible_graph[n]);
			    x = trajectory_planning(first, 0.0, next, 40.0, 1.0);
			    double a[4][4];
			    for(int i=0; i < 4; i++)
			      {
				a[i][0] = x->A0[i];
				a[i][1] = x->A1[i];
				a[i][2] = x->A2[i];
				a[i][3] = x->A3[i];
			      }
			    free(x);
			    varray_t* trajectory_points = varray_create();
			    for(int i=0; i < 40; i++){
			      double* gohere = malloc(sizeof(double)*4);
			      for(int p=0; p < 4; p++)
				{
				  //Position equation
				  gohere[p] = a[0][p] + a[1][p]*i + a[2][p]*pow(i,2) + a[3][p]*pow(i,3);
				}
			      varray_add(trajectory_points,gohere);
			    }
			    for(int d=0; d < varray_size(trajectory_points); d++){
			      double *dummyW = varray_get(trajectory_points, d);
			      waypoints_t *newWaypoint = calloc(1, sizeof(*newWaypoint));
			      newWaypoint->joint0pos = dummyW[0];
			      newWaypoint->joint1pos = dummyW[1];
			      newWaypoint->joint2pos = dummyW[2];
			      newWaypoint->joint3pos = dummyW[3];
			      newWaypoint->t = d;
			      double time = d;
			      varray_add(state->waypointsList, newWaypoint);
			      varray_add(state->reverseList, newWaypoint);
			    }
			  }

			  // Use PF for last point in roadmap
			  else if(t == state->pathSize-1){

		     
			    printf("USING PF ON LAST ONE\n");
			    epsilon = 0.001; 
			    pflag = 1;
			    finalRingGoal->zworld = 0.0;
			    double * current = varray_get(state->qList, possible_graph[t]);
			    waypointsTemp = path_plan_create(current, finalRingGoal, state->cylinderList, state->ringList, epsilon, pflag);
			    if (waypointsTemp == -1) {

			      double dummyW[4] = {0.0,0.0,90.0,0.0};
			      waypoints_t *newWaypoint = calloc(1, sizeof(*newWaypoint));
			      newWaypoint->joint0pos = dummyW[0];
			      newWaypoint->joint1pos = dummyW[1];
			      newWaypoint->joint2pos = dummyW[2];
			      newWaypoint->joint3pos = dummyW[3];
			      newWaypoint->t = d+t;
			      double time = d+t;
			      zarray_add(timeList, &time);
			      varray_add(state->waypointsList, newWaypoint);
			      varray_add(state->reverseList, newWaypoint);

			      t = state->pathSize+2;
			    }else{
			      for(d=0; d < varray_size(waypointsTemp); d++){
				double *dummyW = varray_get(waypointsTemp, d);
				waypoints_t *newWaypoint = calloc(1, sizeof(*newWaypoint));
				newWaypoint->joint0pos = dummyW[0];
				newWaypoint->joint1pos = dummyW[1];
				newWaypoint->joint2pos = dummyW[2];
				newWaypoint->joint3pos = dummyW[3];
				newWaypoint->t = d+t;
				double time = d+t;
				zarray_add(timeList, &time);
				varray_add(state->waypointsList, newWaypoint);
				varray_add(state->reverseList, newWaypoint);
			      }
			    }

			    pflag = 2;
			    epsilon = 0.001;
			    waypoints_t* last  = varray_get(state->waypointsList,varray_size(state->waypointsList) - 1);

		        
			    current[0] = last->joint0pos;
			    current[1] = last->joint1pos;
			    current[2] = last->joint2pos;
			    current[3] = last->joint3pos;
			    printf("COLLECTING BB\n");
			    waypointsTemp = path_plan_create(current, finalRingGoal, state->cylinderList, state->ringList, epsilon, pflag);
			    if (waypointsTemp == -1) {

			      double dummyW[4] = {0.0,0.0,90.0,0.0};
			      waypoints_t *newWaypoint = calloc(1, sizeof(*newWaypoint));
			      newWaypoint->joint0pos = dummyW[0];
			      newWaypoint->joint1pos = dummyW[1];
			      newWaypoint->joint2pos = dummyW[2];
			      newWaypoint->joint3pos = dummyW[3];
			      newWaypoint->t = d+t;
			      double time = d+t;
			      zarray_add(timeList, &time);
			      varray_add(state->waypointsList, newWaypoint);
			      varray_add(state->reverseList, newWaypoint);
				

			      t = state->pathSize;
			    } 
			    else {
			      for(d=0; d < varray_size(waypointsTemp); d++){
				double *dummyW = varray_get(waypointsTemp, d);
				waypoints_t *newWaypoint = calloc(1, sizeof(*newWaypoint));
				newWaypoint->joint0pos = dummyW[0];
				newWaypoint->joint1pos = dummyW[1];
				newWaypoint->joint2pos = dummyW[2];
				newWaypoint->joint3pos = dummyW[3];
				newWaypoint->t = d+t;
				double time = d+t;
				zarray_add(timeList, &time);
				varray_add(state->waypointsList, newWaypoint);
				varray_add(state->reverseList, newWaypoint);
			      }
			    }
		        
			    int size = varray_size(state->waypointsList);
			      zarray_add(state->reverse, &size);
			      printf("ADDING TO REVERSE %d %d\n",size, zarray_size(state->reverse));
			      //  }
			      printf("size of waypoints array: %d\n", varray_size(state->waypointsList));
			      sleep(2);
			    
			  }
			
			  if(t == 1)
			    sizeWaypoints = varray_size(state->waypointsList);
			}	


			printf("MADE IT OUT OF LOOP\n");

			printf("GOING BACK TO DO NEXT RING\n");
			sleep(1);

			int sizehere = varray_size(state->reverseList);

			varray_remove(state->qList, varray_size(state->qList) - 1);
			varray_remove(state->qList, varray_size(state->qList) - 1);
			int sizereverse = varray_size(state->reverseList);
			for(int u=0; u < sizereverse; u++){
			  varray_remove(state->reverseList, u);
			}
		}	
		}
		else{//go home if no rings
		  waypoints_t *newWaypoint = calloc(1, sizeof(*newWaypoint));
		  newWaypoint->joint0pos = 0.0;
		  newWaypoint->joint1pos = 90.0;
		  newWaypoint->joint2pos = 0.0;
		  newWaypoint->joint3pos = 0.0;
		  varray_add(state->waypointsList, newWaypoint);
		}
	}	

	/***********************************************************************************************************/
		
	//If there was a key press and there are waypoints in the list to execute, send them to the rexarm
	//There will be waypoints in the list either if we set the waypoints or if the path planner set the waypoints
	else if((state->executeWaypoints == true) && (varray_size(state-> waypointsList) != 0)){
	  printf("SHOULD EXECUTE WAYPOINTS");
	  //This variable triggers the arm to start moving
	  state->publishingWaypoints = 1;
	  Trajectory_Planning_t * x = (struct _Trajectory_Planning_t*) malloc(sizeof(struct _Trajectory_Planning_t));

	  //Loop through the waypoints list
	  int previous_size = 0;
	  int current_size;
	  printf("SHOULD GO FORWARDS: %d\n",zarray_size(state->reverse));
	  for(int i=0; i < zarray_size(state->reverse); i++){
	 
	    int index;
	    zarray_get(state->reverse, i, &index);
	    //index = index+previous_size;
	    //  previous_size = 0;

	     printf("GOING FORWARDS NOW START: %d, FINISH %d\n",previous_size,index-1);
	      sleep(5);

	    for( int z = previous_size; z < index; z++) //calculates trajectory path waypoint by waypoint
	      {

		if(z == sizeWaypoints){
		  //	  printf("DONE WITH FIRST WAYPOINT HERE -------------------------------------------------------\n");
		  //	sleep(10);
		}
		double t_initial;
		double t_final;
		waypoints_t *waypointi = calloc(1, sizeof(*waypointi));
		waypoints_t *waypointf;

		//If this is the first waypoint, we want it's initial position to be the current position, and the final position to be the first waypoint
		if(z==previous_size){
		  waypointf = varray_get(state->waypointsList, previous_size);

		  waypointi->joint0pos = state->joint1_value;
		  waypointi->joint1pos= state->joint2_value;
		  waypointi->joint2pos=state->joint3_value;
		  waypointi->joint3pos=state->joint4_value;
		  t_initial = 0.0;
		}
		//If it's not the first waypoint, the initial position is the last waypoint (because it presumably got there), and the final waypoint
		//is the next waypoint in the list
		else{
		  waypointi = varray_get(state->waypointsList, z-1);
		  waypointf = varray_get(state->waypointsList, z);
		}
			
			
		//Record initial and final waypoints IN RADIANS!!!!!
		posi[0] = (waypointi-> joint0pos)*(M_PI/180.0);
		posi[1] = (waypointi-> joint1pos)*(M_PI/180.0);
		posi[2] = (waypointi-> joint2pos)*(M_PI/180.0);
		posi[3] = (waypointi-> joint3pos)*(M_PI/180.0);			
			
		//If it's not the first waypoint, initial time is the ending time of the initial waypoint
		//If it is the first waypoint, t_intiial will be 0
		if(z!=previous_size)
		  t_initial = waypointi->t;

		posf[0] = (waypointf-> joint0pos)*(M_PI/180.0);
		posf[1] = (waypointf-> joint1pos)*(M_PI/180.0);
		posf[2] = (waypointf-> joint2pos)*(M_PI/180.0);
		posf[3] = (waypointf-> joint3pos)*(M_PI/180.0);
		//Final time is always the t stored in the waypoint structure

		t_final = waypointf ->t;
			
		//Call trajectory planning function
		x = trajectory_planning(posi, t_initial, posf, t_final, state->speed_value);
     	
		tf = x->delta_t;
     	
		double a[4][4];
		for(int i = 0; i < 4; i++)
		  {
		    a[i][0] = x->A0[i];
		    a[i][1] = x->A1[i];
		    a[i][2] = x->A2[i];
		    a[i][3] = x->A3[i];     		
		  }
     						
		t_initial = 0.0;
			
		//Send trajectory to arm
		for(double t = t_initial; t < tf; t=t+(tf/50.0))
		  {
		    for(int p = 0; p < 4; p++)
		      {
			//Position equation
			q[p] = a[0][p] + a[1][p]*t + a[2][p]*pow(t,2) + a[3][p]*pow(t,3); 
			//Velocity equation
			v[p] = a[1][p] + 2.0*a[2][p]*t + 3.0*a[3][p]*pow(t,2);
			//You actually don't need these next lines, but I'm leaving them in for reference
			cmds.commands[p].utime = t*1000000;
			cmds.commands[p].position_radians = q[p];
			cmds.commands[p].speed = v[p];
			cmds.commands[p].max_torque = state->torque_value;
					
		      }
		    //Set joint commands to calculated position
		    state->joint1_command = q[0] * (180.0/M_PI);
		    state->joint2_command = q[1] * (180.0/M_PI);
		    state->joint3_command = q[2] * (180.0/M_PI);
		    state->joint4_command = q[3] * (180.0/M_PI);
		    usleep(20000/(state->speed_value/.05));
		  }
	      }

	    printf("GOING BACKWARDS NOW START: %d, FINISH %d\n",index-1,previous_size);
	    for(int i=0; i < varray_size(state->waypointsList); i++){
	      waypoints_t *w = varray_get(state->waypointsList, i);
	      w->t = i;
	    }
	    sleep(10);
	
	    for( int z = index-1; z >=previous_size; z--) //calculates trajectory path waypoint by waypoint
	      {

		  
		if(z == sizeWaypoints){
		  //  printf("DONE WITH FIRST WAYPOINT HERE -------------------------------------------------------\n");
		  //	sleep(10);
		}
		double t_initial;
		double t_final;
		waypoints_t *waypointi = calloc(1, sizeof(*waypointi));
		waypoints_t *waypointf;

		//If this is the first waypoint, we want it's initial position to be the current position, and the final position to be the first waypoint
		if (z== index-1){//if(z==0){	//for backwards, z=varray_size(state->waypointsList)-1 is the first one, so maybe want to start here instead?
		  waypointf = varray_get(state->waypointsList, previous_size);

		  waypointi->joint0pos = state->joint1_value;
		  waypointi->joint1pos= state->joint2_value;
		  waypointi->joint2pos=state->joint3_value;
		  waypointi->joint3pos=state->joint4_value;
		  t_initial = 0.0;
		}
			
		if(z == previous_size){
		  waypointf -> joint0pos = 0.0;
		  waypointf -> joint1pos = 90.0;
		  waypointf -> joint2pos = 0.0;
		  waypointf -> joint3pos = 0.0;
		  waypointi = varray_get(state->waypointsList, z);
		  t_initial = waypointi->t;
		}
		//If it's not the first waypoint, the initial position is the last waypoint (because it presumably got there), and the final waypoint
		//is the next waypoint in the list
		else{
		  waypointi = varray_get(state->waypointsList, z);
		  waypointf = varray_get(state->waypointsList, z-1);
		}
			
			
		//Record initial and final waypoints IN RADIANS!!!!!
		posi[0] = (waypointi-> joint0pos)*(M_PI/180.0);
		posi[1] = (waypointi-> joint1pos)*(M_PI/180.0);
		posi[2] = (waypointi-> joint2pos)*(M_PI/180.0);
		posi[3] = (waypointi-> joint3pos)*(M_PI/180.0);			
			
		//If it's not the first waypoint, initial time is the ending time of the initial waypoint
		//If it is the first waypoint, t_intiial will be 0
		if (z != index - 1)//if(z!=0)	//z != varray_size(state->waypointsList) - 1;
		  t_initial = waypointi->t;

		posf[0] = (waypointf-> joint0pos)*(M_PI/180.0);
		posf[1] = (waypointf-> joint1pos)*(M_PI/180.0);
		posf[2] = (waypointf-> joint2pos)*(M_PI/180.0);
		posf[3] = (waypointf-> joint3pos)*(M_PI/180.0);
		//Final time is always the t stored in the waypoint structure

		//printf("WAYPOINTF: %f %f %f %f\n", posf[0], posf[1], posf[2], posf[3]);
		t_final = waypointf ->t;

		//Call trajectory planning function
		x = trajectory_planning(posi, 0, posf, 1, state->speed_value);
		tf = x->delta_t;
     	
		double a[4][4];
		for(int i = 0; i < 4; i++)
		  {
		    a[i][0] = x->A0[i];
		    a[i][1] = x->A1[i];
		    a[i][2] = x->A2[i];
		    a[i][3] = x->A3[i];     		
		  }
     						
		t_initial = 0.0;
			
		//Send trajectory to arm
		for(double t = t_initial; t < tf; t=t+(tf/50.0))
		  {
		    for(int p = 0; p < 4; p++)
		      {
			//Position equation
			q[p] = a[0][p] + a[1][p]*t + a[2][p]*pow(t,2) + a[3][p]*pow(t,3); 
			//Velocity equation
			v[p] = a[1][p] + 2.0*a[2][p]*t + 3.0*a[3][p]*pow(t,2);
			//You actually don't need these next lines, but I'm leaving them in for reference
			cmds.commands[p].utime = t*1000000;
			cmds.commands[p].position_radians = q[p];
			cmds.commands[p].speed = v[p];
			cmds.commands[p].max_torque = state->torque_value;
					
		      }
		    //Set joint commands to calculated position
		    state->joint1_command = q[0] * (180.0/M_PI);
		    state->joint2_command = q[1] * (180.0/M_PI);
		    state->joint3_command = q[2] * (180.0/M_PI);
		    state->joint4_command = q[3] * (180.0/M_PI);
		    usleep(20000/(state->speed_value/.05));
		  }
		//	if(state->doingPathPlanning == false)
		//	usleep(1000000);
	      }

		
	    sleep(5);
	    state->publishingWaypoints = 0;
	    previous_size = index;
	  }
	  //	  previous_size = index;
	}
  }
  state->doingPathPlanning = false;
  return 0;
}

// =============================================================================================
// ===== TOUCH EVENT ===========================================================================
// =============================================================================================
// This function records a touch event if using a touch screen.
// =============================================================================================

static int
touch_event (vx_event_handler_t *vh, vx_layer_t *vl, vx_camera_pos_t *pos, vx_touch_event_t *mouse)
{
    return 0; // Does nothing
}

// ==============================================================================================
// ===== STATUS HANDLERS ========================================================================
// ==============================================================================================
// The following functions deal with the command and response statuses of the rexarm. This is the
// only place in the code in which armlab.c interacts with LCM and the rexarm to properly command
// the servos.
//	1. Command loop constantly sends commands to the arm so that the servos are always
//	commanded.
//	2. Status handler is called when a new message is received from the arm.
//	3. Status loop constantly receives commands from the arm and calls the status handler.
// ===============================================================================================

// ===============================================================================================
// ===== COMMAND LOOP ============================================================================
// ===============================================================================================
// This function sends the arm commands generated in various parts of armlab.c and publishes
// them to rexarm in a continuous loop.
// ===============================================================================================

void * command_loop (void *user)
{
    state_t *state = user;
    const int hz = 30;

    dynamixel_command_list_t cmds;
    cmds.len = NUM_SERVOS;
    cmds.commands = calloc (NUM_SERVOS, sizeof(dynamixel_command_t));

    while (1) {
        // Send LCM commands to arm.
        for (int id = 0; id < NUM_SERVOS; id++) {
        	cmds.commands[id].utime = utime_now ();
		if(state->getWaypoints == 0){
                	cmds.commands[id].speed = state->speed_value;
                	cmds.commands[id].max_torque = state->torque_value;
		}	
		else{
			//If we're recording waypoints, set to zero gravity mode
			cmds.commands[id].speed = 0.0;
			cmds.commands[id].max_torque = 0.0;
		}
        }
	cmds.commands[0].position_radians = state->joint1_command * (M_PI/180.0);
	cmds.commands[1].position_radians = state->joint2_command * (M_PI/180.0);
	cmds.commands[2].position_radians = state->joint3_command * (M_PI/180.0);
	cmds.commands[3].position_radians = state->joint4_command * (M_PI/180.0); 
	dynamixel_command_list_t_publish (state->lcm, "ARM_COMMAND", &cmds);
        usleep (1000000/hz);

    }

    free (cmds.commands);

    return NULL;
}

// ================================================================================================
// ===== STATUS HANDLER ===========================================================================
// ================================================================================================
// This function is called by the status loop when a new status message is received from the arm.
// It updates the joint positions in the state.
// ================================================================================================

static void status_handler (const lcm_recv_buf_t *rbuf,
                const char *channel,
                const dynamixel_status_list_t *msg,
                void *user)
{
	state_t *state = user;
	double clampedvalues[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	double *clamps = clampedvalues;
	state->joint1_value = msg->statuses[0].position_radians * (180.0/M_PI);
	state->joint2_value = msg->statuses[1].position_radians * (180.0/M_PI);
        state->joint3_value = msg->statuses[2].position_radians * (180.0/M_PI);
        state->joint4_value = msg->statuses[3].position_radians * (180.0/M_PI);
	clampedvalues[0] = state->joint1_value * (M_PI/180.0);
	clampedvalues[1] = state->joint2_value * (M_PI/180.0);
	clampedvalues[2] = state->joint3_value * (M_PI/180.0);
	clampedvalues[3] = state->joint4_value * (M_PI/180.0);
	//Call the clamping function to stop the arm from moving past its limits and turn the rendering red	
	clamps = rexarm_clamp(clampedvalues);
	state->joint1_error = clamps[4];
	state->joint2_error = clamps[5];
	state->joint3_error = clamps[6];
	state->joint4_error = clamps[7];
	
	
}

// =================================================================================================
// ===== STATUS LOOP ===============================================================================
// =================================================================================================
// This function waits in a continuous loop and calls the status handler when a new status 
// message is received.
// =================================================================================================

void * status_loop (void *data)
{
    state_t *state = data;
    dynamixel_status_list_t_subscribe (state->lcm,
                                       state->status_channel,
                                       status_handler,
                                       state);
    const int hz = 15;
    while (1) {
        // Set up the LCM file descriptor for waiting. This lets us monitor it
        // until something is "ready" to happen. In this case, we are ready to
        // receive a message.
        int status = lcm_handle_timeout (state->lcm, 1000/hz);
        if (status <= 0)
            continue;

        // LCM has events ready to be processed
    }

    return NULL;
}
	
// ================================================================================================
// ===== ANIMATE THREAD ===========================================================================
// ================================================================================================
// The render loop handles your visualization updates. It is the function run
// by the animate_thread. It periodically renders the contents on the
// vx world contained by state.
// ================================================================================================

void * animate_thread (void *data)
{
    const int fps = 60;
    state_t *state = data;
    cylinders_t *cyltemp;
    cylinders_t *ringtemp;
    double offset = 0.0;
    // Set up the imagesource
    image_source_t *isrc = image_source_open (state->img_url);

    if (isrc == NULL)
        printf ("Error opening device.\n");
    else {
        // Print out possible formats. If no format was specified in the
        // url, then format 0 is picked by default.
        // e.g. of setting the format parameter to format 2:
        for (int i = 0; i < isrc->num_formats (isrc); i++) {
            image_source_format_t ifmt;
            isrc->get_format (isrc, i, &ifmt);
            printf ("%3d: %4d x %4d (%s)\n",
                    i, ifmt.width, ifmt.height, ifmt.format);
        }
        isrc->start (isrc);
    }

    // Continue running until we are signaled otherwise. This happens
    // when the window is closed/Ctrl+C is received.
    while (state->running) {

        // Get the most recent camera frame and render it to screen.
        if (isrc != NULL) {
            image_source_data_t *frmd = calloc (1, sizeof(*frmd));
            int res = isrc->get_frame (isrc, frmd);
            if (res < 0)
                printf ("get_frame fail: %d\n", res);
            else {
                // Handle frame
                image_u32_t *im = image_convert_u32 (frmd);
		/***********************************
		//TEMPLATE MATCHING ADDITIONS
		//Copy the image into the state so that we can access it everywhere
		image_u32_t *image = image_u32_copy(im);
	
		//Render the template of the cylinder in the corner
		if(state->cyltempl != NULL){
			image_u32_t *templ = image_u32_copy(state->cyltempl);
			
                        vx_object_t *vtemp = vxo_image_from_u32(templ,
                                                          VXO_IMAGE_FLIPY,
                                                          VX_TEX_MIN_FILTER | VX_TEX_MAG_FILTER);
                        const double scale = 0.2/templ->width;
			
                        vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "templ"),
                                        vxo_chain (vxo_mat_scale3 (scale, scale, 1.0),
                                                   vxo_mat_translate3 (-450, -220, .01),
                                                   vtemp));
			
                    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "templ"));
                    image_u32_destroy (templ);
                }

		//Render the template of the ring in the corner
		if(state->ringtempl != NULL){
                        image_u32_t *rtempl = image_u32_copy(state->ringtempl);

                        vx_object_t *rvtemp = vxo_image_from_u32(rtempl,
                                                          VXO_IMAGE_FLIPY,
                                                          VX_TEX_MIN_FILTER | VX_TEX_MAG_FILTER);
                        const double scale = 0.2/rtempl->width;

                        vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "rtempl"),
                                        vxo_chain (vxo_mat_scale3 (scale, scale, 1.0),
                                                   vxo_mat_translate3 (-240, -170, .01),
                                                   rvtemp));
                    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "rtempl"));
                    image_u32_destroy (rtempl);
                }
		/*************************************************/
		//state->image = image;
		state->image = im;
		state->image_width = im->width;
		state->image_height = im->height;
                if (im != NULL) {
                    vx_object_t *vim = vxo_image_from_u32(im,
                                                          VXO_IMAGE_FLIPY,
                                                          VX_TEX_MIN_FILTER | VX_TEX_MAG_FILTER);

                    // render the image centered at the origin and at a normalized scale of +/-1 unit in x-dir
                    const double scale = 2./im->width;
                    vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "image"),
                                        vxo_chain (vxo_mat_scale3 (scale, scale, 1.0),
                                                   vxo_mat_translate3 (-im->width/2., -im->height/2., 0.),
                                                   vim));
                    vx_buffer_swap (vx_world_get_buffer (state->vxworld, "image"));
                    image_u32_destroy (im);
                }
		
            }
            fflush (stdout);
            isrc->release_frame (isrc, frmd);
        }

        // RENDER CYLINDERS
	if(varray_size(state->cylinderList) > 0){
		for(int q=0; q < varray_size(state->cylinderList); q++){
			cyltemp = varray_get(state->cylinderList, q);
			//if(q==0)offset=0.2;else offset = 0.0;
			offset = 0.0;
			vx_object_t *vxo_cylinder = vxo_chain( vxo_mat_translate3 (cyltemp->xpos+offset, cyltemp->ypos+offset, 0.123),
						       vxo_mat_scale3(state->scale *.090, state->scale * .088, state->scale * .142),
						       vxo_cylinder (vxo_mesh_style (vx_blue)));
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "cylinder"), vxo_cylinder);
		}
	}
	
	//RENDER RINGS
	if(varray_size(state->ringList) > 0){
		for(int r=0; r < varray_size(state->ringList); r++){
			ringtemp = varray_get(state->ringList, r);
			//if(r==0)offset = -0.3; else offset = 0.0;
			offset = 0.0;
			vx_object_t *vxo_cylinder = vxo_chain(vxo_mat_translate3 (ringtemp->xpos+offset, ringtemp->ypos+offset, 0.02),
						  vxo_mat_scale3(state->scale * .074, state->scale * .074, state->scale * .019),
						  vxo_cylinder (vxo_mesh_style (vx_red)));
			vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "ring"), vxo_cylinder);
		}
	}

        // RENDER AXES AND ARM
	float colors[4] = {0.0, 0.0, 0.0, 0.0};
	float errors[4] = {state->joint1_error, state->joint2_error, state->joint3_error, state->joint4_error};
        double angles[4] = {state->joint1_value, state->joint2_value, state->joint3_value, state->joint4_value};
	if(state->drawCoords == 1){
        	vx_object_t *vxo_axe = vxo_chain (vxo_mat_translate2(state->worldOrigin[0], state->worldOrigin[1]),
                 	                         vxo_mat_rotate_z(state->rotation),
                       		                 vxo_mat_scale(state->scale*.10),
					  	vxo_axes ());
        	vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "axes"), vxo_axe);
		vx_object_t *vxo_arm = vxo_chain(vxo_mat_translate3(state->worldOrigin[0], state->worldOrigin[1], 0.0),
						 vxo_mat_rotate_z(state->rotation),
						 vxo_mat_scale(state->scale),
						 rexarm_vxo_arm(errors, angles, colors, 0.0));
		vx_buffer_add_back (vx_world_get_buffer (state->vxworld, "arm"), vxo_arm);

	}
	//Update buffers to render
	vx_buffer_swap (vx_world_get_buffer (state->vxworld, "axes"));
	vx_buffer_swap (vx_world_get_buffer (state->vxworld, "cylinder"));
	vx_buffer_swap (vx_world_get_buffer (state->vxworld, "ring"));
	vx_buffer_swap (vx_world_get_buffer (state->vxworld, "arm"));

        usleep (1000000/fps);
    }

    if (isrc != NULL)
        isrc->stop (isrc);

    return NULL;
}

// ================================================================================================
// ===== STATE CREATE =============================================================================
// ================================================================================================
// This function is called from main and instantiates the state of the rexarm. It initializes all
// variables and returns the rexarm set up to use.
// ================================================================================================

state_t * state_create (void)
{
    state_t *state = calloc (1, sizeof(*state));
    state->pairs = zarray_create(8);
    dynamixel_command_list_t cmds;

    cmds.len = NUM_SERVOS;
    cmds.commands = malloc(sizeof(dynamixel_command_t)*NUM_SERVOS);

    state->lcm = lcm_create(NULL);
    state->vxworld = vx_world_create ();
    state->vxeh = calloc (1, sizeof(*state->vxeh));
    state->vxeh->key_event = key_event;
    state->vxeh->mouse_event = mouse_event;
    state->vxeh->touch_event = touch_event;
    state->vxeh->dispatch_order = 100;
    state->vxeh->impl = state; 

    state->vxapp.display_started = rob550_default_display_started;
    state->vxapp.display_finished = rob550_default_display_finished;
    state->vxapp.impl = rob550_default_implementation_create (state->vxworld, state->vxeh);
    state->running = 1;

    state->joint1_command = 0.0;
    state->joint2_command = 90.0;
    state->joint3_command = 0.0;
    state->joint4_command = 0.0;
    state->mouseclicks = 0;
    state->publishingWaypoints = 0;
    state->doingPathPlanning = 0; 
    state->cylinderList = varray_create();
    state->ringList = varray_create();
    state->waypointsList = varray_create();
    state->reverse = zarray_create(sizeof(int));
    state->reverseList = varray_create(); 

    for (int id = 0; id < NUM_SERVOS; id++){
        cmds.commands[id].utime = utime_now();
        cmds.commands[id].position_radians = 0.0;
        cmds.commands[id].speed = 0.05;
        cmds.commands[id].max_torque = 0.35;
    }

    //This is the only place other than in the command loop that the arm is commanded. It forces the
    // arm to return to the home position upon startup.

    state->speed_value = .05;
    state->qList = varray_create();
    state->qIDs = zarray_create(sizeof(int));
    dynamixel_command_list_t_publish(state->lcm, "ARM_COMMAND", &cmds);
    state->roadmapConstructed = 0;
    return state;
}

// ==============================================================================================
// ===== STATE DESTROY ==========================================================================
// ==============================================================================================
//  This function destroys the state when the GUI is closed.
// ==============================================================================================

void state_destroy (state_t *state)
{
    if (!state)
        return;

    free (state->vxeh);
    getopt_destroy (state->gopt);
    pg_destroy (state->pg);
    free (state);
}

// ===============================================================================================
// ===== MAIN ====================================================================================
// ===============================================================================================
// The main calls the funciton to create the state and then begins the threads to start listening
// for events and sending/receiving commands.
// ===============================================================================================

int main (int argc, char *argv[])
{
    rob550_init (argc, argv);
    state_t *state = state_create ();

    // Parse arguments from the command line, showing the help
    // screen if required
    //dc1394://b09d0100d8deb4 //table 4
    //dc1394://b09d0100d8de9b //table 1
    //dc1394://b09d0100d8deb6 //table 5
    //dc1394://b09d0100d8deb8 //front table
    //dc1394://b09d0100d928d5 // table 0

    state->gopt = getopt_create ();
    getopt_add_bool   (state->gopt,  'h', "help", 0, "Show help");
    //CHANGE CAMERA VALUE HERE TO THE VALUES DEFINED UP IN THE COMMENTS ABOVE
    //-------------------------------------------VVVVVVVVVVVVVVVVVVVVVVVVV-------------------
    getopt_add_string (state->gopt, '\0', "url","dc1394://b09d0100d8deb6", "Camera URL");
    //-------------------------------------------^^^^^^^^^^^^^^^^^^^^^^^^^-------------------
    getopt_add_string (state->gopt, '\0', "command-channel", "ARM_COMMAND", "LCM command channel");
    getopt_add_string (state->gopt, '\0', "status-channel", "ARM_STATUS", "LCM status channel");

    if (!getopt_parse (state->gopt, argc, argv, 1) || getopt_get_bool (state->gopt, "help")) {
        printf ("Usage: %s [--url=CAMERAURL] [other options]\n\n", argv[0]);
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }

    // Set up the imagesource. This looks for a camera url specified on
    // the command line and, if none is found, enumerates a list of all
    // cameras imagesource can find and picks the first url it finds.
    if (strncmp (getopt_get_string (state->gopt, "url"), "", 1)) {
        state->img_url = strdup (getopt_get_string (state->gopt, "url"));
        printf ("URL: %s\n", state->img_url);
    }
    else {
        // No URL specified. Show all available and then use the first
        zarray_t *urls = image_source_enumerate ();
        printf ("Cameras:\n");
        for (int i = 0; i < zarray_size (urls); i++) {
            char *url;
            zarray_get (urls, i, &url);
            printf ("  %3d: %s\n", i, url);
        }

        if (0==zarray_size (urls)) {
            printf ("Found no cameras.\n");
            return -1;
        }

        zarray_get (urls, 0, &state->img_url);
    }

    // Initialize this application as a remote display source. This allows
    // you to use remote displays to render your visualization. Also starts up
    // the animation thread, in which a render loop is run to update your display.
    vx_remote_display_source_t *cxn = vx_remote_display_source_create (&state->vxapp);

    // Initialize a parameter gui
    state->pg = pg_create ();
    pg_add_double_slider (state->pg, "axis1", "Axis 1", -171.887339, 178.9314719, 0.0);
    pg_add_double_slider (state->pg, "axis2", "Axis 2", -125.993419, 124.331842, 90.0);
    pg_add_double_slider (state->pg, "axis3", "Axis 3", -121.008686, 126.623673, 0.0);
    pg_add_double_slider (state->pg, "axis4", "Axis 4", -127.998771, 127.712293, 0.0);

    pg_add_double_boxes(state->pg,
			"speedBox", "Speed", 0.05,
			"torqueBox", "Torque", 0.35,
			NULL);

    pg_add_check_boxes(state->pg,
			"cylinderBox", "Draw Cylinder", 0, NULL);
    pg_add_check_boxes(state->pg, "ringBox", "Draw Ring", 0, NULL);
    pg_add_check_boxes(state->pg, "calibrateBox", "Calibrate", 0, NULL);
    pg_add_check_boxes(state->pg, "waypointsBox", "Add Waypoints", 0, NULL);
    pg_add_check_boxes(state->pg, "pathBox", "Path Plan", 0, NULL);
    pg_add_check_boxes(state->pg, "executeBox", "Execute Waypoints", 0, NULL);

    parameter_listener_t *my_listener = calloc (1, sizeof(*my_listener));
    my_listener->impl = state;
    my_listener->param_changed = my_param_changed;
    pg_add_listener (state->pg, my_listener);
   
    state->status_channel = getopt_get_string (state->gopt, "status-channel");
    pthread_create (&state->status_thread, NULL, status_loop, state);
    state->command_channel = getopt_get_string (state->gopt, "command-channel");
    pthread_create (&state->command_thread, NULL, command_loop, state);

    // Launch our worker threads
    pthread_create (&state->animate_thread, NULL, animate_thread, state);

    // This is the main loop
    rob550_gui_run (&state->vxapp, state->pg, 1024, 768);

    // Quit when GTK closes
    state->running = 0;
    pthread_join (state->animate_thread, NULL);

    // Cleanup
    lcm_destroy(state->lcm);
    free (my_listener);
    state_destroy (state);
    vx_remote_display_source_destroy (cxn);
    vx_global_destroy ();
}
