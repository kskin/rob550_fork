// ----------------------------------------------------------------------------------------------
// ----- AFFINE_CALIBRATION.H -------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// This file handles all affine calibration calculations. It takes in four coordinates and
// calculates the world origin. It also takes in locations of cylinders or rings and returns an
// appropriate place to render them on the screen.
// ----------------------------------------------------------------------------------------------

#ifndef __AFFINE_CALIBRATION_H__
#define __AFFINE_CALIBRATION_H__

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_blas.h>
#include <stdio.h>

#include "common/zarray.h"
#include "math/matd.h"
#include "structures.h"

#ifdef __cplusplus
extern "C" {
#endif

// ===============================================================================================
// ===== AFFINE CALIBRATION CREATE ===============================================================
// ===============================================================================================
// This function creates an affine calibration matrix based on the input pairs of coordinates.
// ===============================================================================================
affine_calibration_t * affine_calibration_create (const zarray_t *pairs);

// ===============================================================================================
// ===== AFFINE CALIBRATION DESTROY ==============================================================
// ===============================================================================================
// This function destroys an instance of an affine calibration matrix.
// ===============================================================================================
void affine_calibration_destroy (affine_calibration_t *cal);


// ================================================================================================
// ===== PIXELS TO WORLD ==========================================================================
// ================================================================================================
// This function converts input pixels to world coordinates using the affine calibration given.
// ================================================================================================
void affine_calibration_p2w (const affine_calibration_t *cal, const double pixels[2], double world[2]);

// ================================================================================================
// ===== WORLD TO PIXELS ==========================================================================
// ================================================================================================
// This function translates input world coordinates to pixels using the affine calibration given.
// ================================================================================================
void affine_calibration_w2p (const affine_calibration_t *cal, const double world[2], double pixels[2]);

#ifdef __cplusplus
}
#endif

#endif //__AFFINE_CALIBRATION_H__
