// ----------------------------------------------------------------------------------------------
// ----- STRUCTURES.H --------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// This file holds structure definitions for structures used in our armlab implementation.
// ----------------------------------------------------------------------------------------------

#ifndef __STRUCTURES_H__
#define __STRUCTURES_H__

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include <sys/select.h>
#include <sys/time.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math/gsl_util_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>
#include <lcm/lcm.h>
#include "lcmtypes/dynamixel_command_list_t.h"
#include "lcmtypes/dynamixel_command_t.h"
#include "lcmtypes/dynamixel_status_list_t.h"
#include "lcmtypes/dynamixel_status_t.h"
#include "common/varray.h"
#include "common/getopt.h"
#include "common/pg.h"

#include "vx/vx.h"
#include "vx/vx_util.h"
#include "vx/vx_remote_display_source.h"
#include "vx/gtk/vx_gtk_display_source.h"
#include "vx/vxo_drawables.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

//=======================================================================================
//===== STRUCTURE DEFINITIONS ===========================================================
//=======================================================================================

// ==================================
// ===== CYLINDERS ==================
// ==================================
// Cylinders have x, y, and z pos
// ==================================
typedef struct cylinders cylinders_t;
struct cylinders {
    double xpos;
    double ypos;
    double zpos;
    double xworld;
    double yworld;
    double zworld;
};

// ==========================
// ===== RINGS ==============
// ==========================
// Rings have x, y, and z pos
// ==========================
typedef struct rings rings_t;
struct rings {
    double xpos;
    double ypos;
    double zpos;
    double xworld;
    double yworld;
    double zworld;
};

// ==================================
// ===== WAYPOINTS ==================
// ==================================
// Waypoints have positions of each 
// joint and a time at which they 
// were stored.
// ==================================
typedef struct waypoints waypoints_t;
struct waypoints{
     double joint0pos;
     double joint1pos;
     double joint2pos;
     double joint3pos;
     double t;
};

// ==========================
// ===== STATE ==============
// ==========================
// State defines all 
// variables that need to be
// accessible throughout the
// program.
// ==========================
typedef struct state state_t;
struct state {
    //Bools to define global states
    bool running;
    bool drawCylinder;
    bool drawRing;
    bool drawCoords;
    bool getWaypoints;
    bool drawWhenClicked;
  bool calibrate;
  bool calibrated;
  bool publishingWaypoints;
  bool planPath;
  bool executeWaypoints;
  bool doingPathPlanning;

  //Values to define world state
    int mouseclicks;
    double scale;
    double rotation;
    double worldvector[2];    
    
    //Lists of objects and waypoints
    varray_t *cylinderList;
    varray_t *ringList;
    varray_t *waypointsList;
  zarray_t* reverse;
    varray_t *reverseList;
    //Lists to calibrate world
    zarray_t *pairs;
    zarray_t *origin;

    // GUI
    getopt_t        *gopt;
    parameter_gui_t *pg;

    //LCM
    lcm_t *lcm;
    const char *command_channel;
    const char *status_channel;

    // image stuff
    char *img_url;
    int   img_height;
    int   img_width;

    // vx stuff
    vx_application_t    vxapp;
    vx_world_t         *vxworld;      // where vx objects are live
    vx_event_handler_t *vxeh; // for getting mouse, key, and touch events
    vx_mouse_event_t    last_mouse_event;

    // threads
    pthread_t animate_thread;
    pthread_t status_thread;
    pthread_t command_thread;
    pthread_t template_cylinder_thread;
    pthread_t template_ring_thread;

    // for accessing the arrays
    pthread_mutex_t mutex;

    //Set response values of each servo
    double joint1_value;
    double joint2_value;
    double joint3_value;
    double joint4_value;
   
    //Set command values of each servo
    double joint1_command;
    double joint2_command;
    double joint3_command;
    double joint4_command;

    //Values from GUI
    double speed_value;
    double torque_value;
    double worldOrigin[2];
    double worldtime;

    //Stuff to make clamping work
    double joint1_error;
    double joint2_error;
    double joint3_error;
    double joint4_error;

    varray_t *qList;
  zarray_t *qIDs;
    int **path;
    double **dist;
    int pathSize;
    bool roadmapConstructed;
    image_u32_t *image;
    image_u32_t *cyltempl;
    image_u32_t *ringtempl;
    int image_width;
    int image_height;	
    double cylinderXTemplate;
    double cylinderYTemplate;
    double ringX;
    double ringY;
    int ringXTemplate;
    int ringYTemplate;
};

// ==================================
// ===== FORWARD KINEMATICS==========
// ==================================
// Forward Kinematics have position,
// orientations, and transformations
// ==================================
typedef struct _forward_kinematics_t forward_kinematics_t;
struct _forward_kinematics_t{
    double position[3];
    double orientation;
    double T02[16];
    double T03[16];
    double T04[16];
    double A01[16];
    double A12[16];
    double A23[16];
    double A34[16];
};

// ==================================
// ===== INVERSE KINEMATICS==========
// ==================================
// Inverse Kinematics includes joint 
// angles.
// ==================================
typedef struct _inverse_kinematics_t inverse_kinematics_t;
struct _inverse_kinematics_t{
 double q1[4];
 double q2[4];
 double q3[4];
 double q4[4];
/*NOTE: If qi[4] is equal to
 Theta1: 5729.577951, Theta2: 5729.577951, Theta3: 5729.577951, Theta4: 5729.577951
 Then the particular solution was nto a valid configuration.
*/
};


// ===================================
// ========= PATH PLANNER ============
// ===================================
// Path Planner includes arrays of 
// obstacles, goals, & transformations 
// ===================================
typedef struct _path_planner_t path_planner_t;
struct _path_planner_t{
  varray_t* obstacles;
  double ceiling_z;
  rings_t *goal;
  double *T04;
};
typedef struct Jacobian Jacobian_t;
struct Jacobian
{
	double Jo1 [6][4];
	double Jo1c [6][4];
	double Jo2 [6][4];
	double Jo2c [6][4];
	double Jo3 [6][4];
	double Jo3c [6][4];
	double Jo4 [6][4]; 
};



// ==================================
// ========= AFFINE PAIRS ===========
// ==================================
// Affine pairs stores pixels & world
// coordinates.
// ==================================
typedef struct affine_pairs affine_pair_t;
struct affine_pairs {
    double pixel[2];
    double world[2];
};

// ==================================
// ======= AFFINE CALIBRATION========
// ==================================
// Affine calibration stores a length
// and calibration vector as well as
// a point to be transformed.
// ==================================
typedef struct _affine_calibration_t affine_calibration_t;
struct _affine_calibration_t{
    double calibration[6];
    double length;
    double p2w[2];
    double w2p[2];
};
  
// ==================================
// ===== TRAJECTORY PLANNING ========
// ==================================
// Trajectory planning includes DH
// transformations and a delta_t over
// which to perform a path segment.
// ==================================
typedef struct _Trajectory_Planning_t Trajectory_Planning_t;
struct _Trajectory_Planning_t{
    double A0[4];
    double A1[4];
    double A2[4];
    double A3[4];
    double delta_t;
};

#endif
