// ----------------------------------------------------------------------------------------------
// ----- PATH_PLANNING.H -------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// These functions implement the potential field planner
// ----------------------------------------------------------------------------------------------

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include <sys/select.h>
#include <sys/time.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math/gsl_util_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>
#include <lcm/lcm.h>
#include "lcmtypes/dynamixel_command_list_t.h"
#include "lcmtypes/dynamixel_command_t.h"
#include "lcmtypes/dynamixel_status_list_t.h"
#include "lcmtypes/dynamixel_status_t.h"
#include "common/varray.h"
#include "structures.h"
#include "rexarm.h"
#include "affine_calibration.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// ===============================================================================================
// ===== PATH PLAN SELF COLLISION  ===============================================================
// ===============================================================================================
// This function returns 1 if the joint configuration q (in deg) results in a self collision, 
// returns 0 otherwise.
// ===============================================================================================
int path_plan_self_collision(double* q);

// ===============================================================================================
// ===== PATH PLAN OBSTACLE CHECK  ===============================================================
// ===============================================================================================
// This function checks along the straight line path between the end effector positions posi and
// posf every 2 cm and returns 1 if there is an obstacle along the path, returns 0 otherwise.
// ===============================================================================================
int path_plan_obstacle_check(double* posi, double* posf,  varray_t* cylinders, varray_t* rings);

// ===============================================================================================
// ===== CROSS PRODUCT ===========================================================================
// ===============================================================================================
// This function returns the cross product computed for 3x1 vectors a and b.
// ===============================================================================================
double * crossProduct(double * a,double * b);

// ===============================================================================================
// ===== JACOBIAN ================================================================================
// ===============================================================================================
// This function calculate Jacobian matrices for the given forward kinematics configuration,
// and the given floating points (midpoints).
// ===============================================================================================
Jacobian_t * Jacobian(forward_kinematics_t *fk, double * midpoints);

// ===============================================================================================
// ===== PATH PLAN CREATE ========================================================================
// ===============================================================================================
// This function implements a potential field planner from an initial joint configuration, q0 in
// degrees, to a final goal point, in xyz coordinates (meters) given obstancles, rings and
// cylinders in the workspace.  Epsilon is the maximum allowable error. Check final is a flag
// to set the state in the DFA (0-rough plan, 1-precise plan, 2-retrieve BB). Returns varray
// of configuration waypoints.
// ===============================================================================================
varray_t* path_plan_create(double *q0, rings_t *goal, varray_t* cylinders, varray_t* rings, 
			   double epsilon, int check_final);

// ===============================================================================================
// ===== PATH PLAN REPEL =========================================================================
// ===============================================================================================
// This function returns the repel force for a given configuration, qi in radians, and obstacles
// in the workspace, cylinders and rings.  The flag is set if the joint being considered is the
// end effector.  Check final is set as described in path plan create, qi_deg is the configuration
// in degrees, and the goal is the final goal point in xyz coordinates (meters).
// ===============================================================================================
double* path_plan_repel(double * qi, varray_t * cylinders, varray_t * rings, int flag, 
			int check_final, double* qi_deg, rings_t* goal);

// ===============================================================================================
// ===== PATH PLAN ATTRACT =======================================================================
// ===============================================================================================
// This function returns the attractive force of a goal given as ring coordinates (xyz coordinates
// in meters), and a given configuration for the end effector, T04.  Check final is set as 
// described in path plan create.
// ===============================================================================================
double* path_plan_attract(rings_t *goal, double *T04, int check_final);

// ===============================================================================================
// ===== PATH PLAN COLLISION CHECK  ==============================================================
// ===============================================================================================
// This function determines if a collision will occur for a given configuration, q, for
// obstacles of cylinders, ring, floor, ceiling, and self.  Returns 1 if collision occurs, 
// returns 0 otherwise.
// ===============================================================================================
int path_plan_collision_check(double* q, varray_t *cylinders, varray_t *rings);

// ===============================================================================================
// ===== PATH PLAN BIASED COLLISION CHECK  =======================================================
// ===============================================================================================
// This function determines if a collision will occur for a given configuration, q, for
// obstacles of cylinders, ring, floor, ceiling, and self.  A bias is set to raise the floor
// and lower the ceiling to return collision-free configurations in a limited spaced.
// Useful for biasing random sampling for PRM. Returns 1 if collision occurs, returns 0 otherwise.
// ===============================================================================================
int path_plan_biased_collision_check(double* q, varray_t *cylinders, varray_t *rings);

// ===============================================================================================
// ===== PATH PLAN FLOATING POINT ================================================================
// ===============================================================================================
// This function returns the floating point on the arm between two origins that is closest
// to obstacles, including cylinders and rings.
// ===============================================================================================
double* path_plan_floating_point(double* origin_1, double* origin_2, varray_t* cylinders,
				 varray_t* rings);

// ===============================================================================================
// ===== PATH PLAN CLOSEST POINT =================================================================
// ===============================================================================================
// This function returns the closest point on an obstacle for a given floating point (or origin),
// where type refers to the type of obstacle (0-cylinder, 1-ring).
// ===============================================================================================
double* path_plan_closest_point(double* floatingpoints, double* Obstacle, int type);

// ===============================================================================================
// ===== U GRAD ==================================================================================
// ===============================================================================================
// This function returns the total gradient potential, accounting for repulsive and attractive
// forces, for the given configuration, qi, and goal. Cylinders and rings are obstacles, check
// final is described in path plan create, and T04 is the end effector configuration.
// ===============================================================================================
double* U_grad(double* qi, rings_t* goal, double* T04, varray_t * cylinders, varray_t * rings, 
	       int check_final);

// ===============================================================================================
// ===== FINAL COLLISION CHECK ===================================================================
// ===============================================================================================
// This function is used for the final step in BB retrieval, and is the same as collision check
// with an added check for collisions inside of the ring's inner radius.  Returns 1 if collision
// occurs, 0 if otherwise.
// ===============================================================================================
int final_collision_check(double *q, rings_t *goal, varray_t *cylinders, varray_t *rings);
