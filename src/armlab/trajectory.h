// ----------------------------------------------------------------------------------------------
// ----- TRAJECTORY.H ---------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// This file implements trajectory planning.
// ----------------------------------------------------------------------------------------------

#ifndef __TRAJECTORY_H__
#define __TRAJECTORY_H__

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <math.h>
#include "common/timeprofile.h"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math/gsl_util_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>
#include <lcm/lcm.h>
#include "lcmtypes/dynamixel_command_list_t.h"
#include "lcmtypes/dynamixel_command_t.h"
#include "lcmtypes/dynamixel_status_list_t.h"
#include "lcmtypes/dynamixel_status_t.h"
#include "structures.h"

// ===============================================================================================
// ===== TRAJECTORY PLANNING =====================================================================
// ===============================================================================================
// This function takes in initial and final values (e.g. from teach-and-repeat) and speed to
// calculate coefficients for smooth cubic spline trajectory planning.
// ===============================================================================================
Trajectory_Planning_t * trajectory_planning(double * q0, double t0, double * qf, double tf, double speed);

#endif
