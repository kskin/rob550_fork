// ----------------------------------------------------------------------------------------------
// ----- PRM.C ---------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// This file implements a Probabilistic Roadmap Planner (PRM).
// ----------------------------------------------------------------------------------------------

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include "prm.h"

// ===============================================================================================
// ===== PATH EQUAL ==============================================================================
// ===============================================================================================
// This function returns 1 if configurations are the same, 0 otherwise.
// ===============================================================================================
int path_equal(double q1[4], double q2[4]){
        if(q1[0] - q2[0] != 0.0)
                return 0;
        if(q1[1] - q2[1] != 0.0)
                return 0;
        if(q1[2] - q2[2] != 0.0)
                return 0;
        if(q1[3] - q2[3] != 0.0)
                return 0;
        return 1;
}

// ===============================================================================================
// ===== ROADMAP CONSTRUCTION ====================================================================
// ===============================================================================================
// This function constructs a roadmap of the workspace with n nodes. Nodes are set as vertices
// in the graph and connected with edges if the path between nodes is collision free. Nodes refer
// to joint angle configurations. Returns graph.
// ===============================================================================================
dijkstra_graph_t * roadmap_construction(int n, int neighbours, varray_t *cylinders, varray_t *rings, state_t *state){
	printf("MADE IT TO PRMi\n");
	printf("N: %d\n", n);
	neighbours = NEIGHBOURS;

	//Initialize arrays to hold nodes and ids in
	zarray_t *ids = zarray_create(sizeof(int));
	varray_t *v = varray_create();

	double q[4];
	double *z;
	double orientation;
	inverse_kinematics_t *x;

	printf("RANDOMLY SAMPLING\n");
	while(varray_size(v) < n){
		double *q = malloc(sizeof(double)*4);
		q[0] = state->joint1_value;
		q[1] = state->joint2_value;
		q[2] = state->joint3_value;
		q[3] = state->joint4_value;

		//Randomly sample around obstacles
		forward_kinematics_t *f = forward_kinematics(q);
		double *position = f->position;
		orientation = f->orientation;
		double *norms = malloc(sizeof(double)*15);

		double one_neg = JOINT1_NEG;
                double two_neg = JOINT2_NEGH;
                double three_neg = JOINT3_NEG;
                double four_neg = JOINT4_NEG;
                double one_pos = JOINT1_POS;
                double two_pos = JOINT2_POSH;
                double three_pos = JOINT3_POS;
                double four_pos = JOINT4_POS;

		double subtractor1 = (double)rand()/(double)RAND_MAX;
		double subtractor2 = (double)rand()/(double)RAND_MAX;
		double subtractor3 = (double)rand()/(double)RAND_MAX;
		double subtractor4 = (double)rand()/(double)RAND_MAX;
		double subtractor5 = (double)rand()/(double)RAND_MAX;
		q[0] = fmod((double)rand(), one_pos);
		if(subtractor1 < 0.5)
			q[0] = -q[0];
		q[1] = fmod((double)rand(), two_pos);
		if(subtractor2 < 0.5)
			q[1] = -q[1];
		q[2] = fmod((double)rand(), three_pos);
		if(subtractor3 < 0.5)
			q[2] = -q[2];
		q[3] = fmod((double)rand(), four_pos);
		if(subtractor4 < 0.5)
			q[3] = -q[3];
		orientation = fmod((double)rand(), f->orientation);
		if(subtractor5 < 0.5)
			orientation = -orientation;

		// Check if configuration causes collision
		int will_collide;
		double *qdeg = malloc(sizeof(double)*4);
		qdeg[0] = q[0] * (180.0/M_PI);
		qdeg[1] = q[1] * (180.0/M_PI);
		qdeg[2] = q[2] * (180.0/M_PI);
		qdeg[3] = q[3] * (180.0/M_PI);
		
		will_collide = path_plan_collision_check(qdeg, cylinders, rings);
		if(will_collide == 0){
			varray_add(v, qdeg);
		}
                while (will_collide != 0){
			double *q = malloc(sizeof(double)*4);
			double *qdeg = malloc(sizeof(double)*4);
			subtractor1 = (double)rand()/(double)RAND_MAX;
                	subtractor2 = (double)rand()/(double)RAND_MAX;
                	subtractor3 = (double)rand()/(double)RAND_MAX;
                	subtractor4 = (double)rand()/(double)RAND_MAX;
                	subtractor5 = (double)rand()/(double)RAND_MAX;
                	q[0] = fmod((double)rand(), JOINT1_POS);
                	if(subtractor1 < 0.5)
                        	q[0] = -q[0];
                	q[1] = fmod((double)rand(), JOINT2_POSH);
                	if(subtractor2 < 0.5)
                        	q[1] = -q[1];
                	q[2] = fmod((double)rand(), JOINT3_POS);
                	if(subtractor3 < 0.5)
                        	q[2] = -q[2];
                	q[3] = fmod((double)rand(), JOINT4_POS);
                	if(subtractor4 < 0.5)
                        	q[3] = -q[3];
                	orientation = fmod((double)rand(), f->orientation);
                	if(subtractor5 < 0.5)
                        	orientation = -orientation;

			qdeg[0] = q[0] * (180.0/M_PI);
                	qdeg[1] = q[1] * (180.0/M_PI);
               	 	qdeg[2] = q[2] * (180.0/M_PI);
                	qdeg[3] = q[3] * (180.0/M_PI);
                	will_collide = path_plan_collision_check(qdeg, cylinders, rings);
			if(will_collide == 0){
				varray_add(v, qdeg);
			}
		}
	}
	printf("CREATE EMPTY GRAPH\n");
	//Create graph that's empty right now
	dijkstra_graph_t *graph = dijkstra_create(n+2, 0);
	for(int i=0; i < n+2; i++){
		char *name = calloc(4, sizeof(*name));
		name = 'a';
		dijkstra_set_user(graph, i, name);
		name = dijkstra_get_user(graph, i);
	}

	for(int i=0; i < n+2; i++){
		zarray_add(ids, &i);
		zarray_add(state->qIDs, &i);
	}
	printf("FIND CLOSEST NEIGHBORS\n");
	for(int s=0; s < n; s++){
		double *position = varray_get(v, s);
		int id_position;
		zarray_get(ids, s, &id_position);

                //k closest neigbhours of q
		zarray_t *nq_ids = zarray_create(sizeof(int));
                double twoNorm = 0;
		double *comparing;
		double comparing_id;
                for(int i=0; i < n; i++){
             		double *possible = varray_get(v, i);
			int id_possible;
			zarray_get(ids, i, &id_possible);
			if(id_possible != id_position){
                        	twoNorm = two_norm(possible, position);
                        	if(zarray_size(nq_ids) < neighbours){
					zarray_add(nq_ids, &id_possible);
				}
                        	else{
                                	for(int j=0; j < zarray_size(nq_ids); j++){
                                        	zarray_get(nq_ids, j, &comparing_id);
						for(int u=0; u < varray_size(v); u++){
							double *dummy = varray_get(v, u);
						}
						comparing = varray_get(v, comparing_id);
                                        	if(twoNorm < two_norm(comparing, position)){
							zarray_remove_index(nq_ids, j, false);
							zarray_add(nq_ids, &id_possible);
						}
					}
				}
			}
		}

		// Setting graph edges
		printf("SETTING GRAPH EDGES\n");
		for(int i=0; i < zarray_size(nq_ids); i++){
			int z_id;
			zarray_get(nq_ids, i, &z_id);
			z = varray_get(v, z_id);
			forward_kinematics_t *initial = forward_kinematics(position);
			forward_kinematics_t *final = forward_kinematics(z);
			double init[3] = {initial->position[0], initial->position[1], initial->position[2]};
			double fin[3] = {final->position[0], final->position[1], final->position[2]};
			
			int collide = 0;
			Trajectory_Planning_t *x = (struct _Trajectory_Planning_T*)malloc(sizeof(struct _Trajectory_Planning_t));
			x = trajectory_planning(position, 0.0, z, 40.0, 1.0);
			double a[4][4];
			for(int i=0; i < 4; i++)
			{
				a[i][0] = x->A0[i];
				a[i][1] = x->A1[i];
				a[i][2] = x->A2[i];
				a[i][3] = x->A3[i];
			}
			for(int t=0; t < 40; t++){
				for(int p=0; p < 4; p++)
				{
					//Position equation
                                        q[p] = a[0][p] + a[1][p]*t + a[2][p]*pow(t,2) + a[3][p]*pow(t,3);
					int localCollide = path_plan_collision_check(q, cylinders, rings);
					collide = collide || localCollide;
				}
			}
			printf("ADDING EDGES TO GRAPH\n");			
			if((path_equal(z, position) == 0) && (dijkstra_get_edge_weight(graph, z_id, id_position)< 0.0) && (collide == 0)){
			  if (abs(position[0] - z[0]) < 180.0)
					dijkstra_add_edge_undir(graph, z_id, id_position, two_norm(z, position));
			}
		}
	}
	for(int g=0; g < varray_size(v); g++){
		double *k = varray_get(v, g);
		varray_add(state->qList, k);
	}

	return graph;
}
			
// ===============================================================================================
// ===== TWO NORM ================================================================================
// ===============================================================================================
// This function returns the q-space norm between qp and q.
// ===============================================================================================		
double two_norm(double qp[4], double q[4]){
	double sum=0;
	double subtract = 0;
	double result;

	for (int i=0; i < 4; i++){
		subtract = qp[i]-q[i];
		sum += pow(subtract, 2);
	}
	result = pow(sum, 0.5);
	return result;
}

// ===============================================================================================
// ===== DIST NORM ===============================================================================
// ===============================================================================================
// This function returns the sum of squares distance (xyz coordinates) for end effector positions
// of configurations qp and q.
// ===============================================================================================
double dist_norm(double qp[4], double q[4]){
  double sum=0;
  double subtract = 0;
  double result;

  forward_kinematics_t* xi = forward_kinematics(qp);
  forward_kinematics_t* xf = forward_kinematics(q);

  double dist  = pow((xi->position[0] - xf->position[0]),2) + pow((xi->position[1] - xf->position[1]),2) + pow((xi->position[2] - xf->position[2]),2);

  result = pow(dist, 0.5);
  free(xi);
  free(xf);
  return result;
}

// ===============================================================================================
// ===== SOLVE QUERY =============================================================================
// ===============================================================================================
// This function finds the closest node in the roadmap to the initial arm configuration, qi,
// and the goal configuration, qf, given a graph g and arm state, state. Distance is measured
// by dist_norm. Returns shortest path between node closest to qi and node closest to qf, or
// returns -1 if no valid solution was found.
// ===============================================================================================
int *solve_query(double *qi, double *qf, int k, dijkstra_graph_t *g, state_t *state){

  //printf("MADE IT TO SOLVE QUERY\n");

  // Iterate through node in the graphs to find closest node using dist_norm to 
  // qi and qf
  double min_i = 1000000000;
  double min_f = 1000000000;
  double smallest_i = 0;
  double smallest_f = 0;
  int index_i;
  int index_f;
  double* closest_qi = malloc(sizeof(double)*4);
  double* closest_qf = malloc(sizeof(double)*4);
  for (int i = 0; i < varray_size(state->qList); i++)
    {
      double* current_q = varray_get(state->qList,i);
      smallest_i = dist_norm(qi,current_q);
      smallest_f = dist_norm(qf,current_q);

      if (smallest_i < min_i) {
	min_i = smallest_i;
	closest_qi[0] = current_q[0];
	closest_qi[1] = current_q[1];
	closest_qi[2] = current_q[2];
	closest_qi[3] = current_q[3];
	index_i = i;
      }

      if (smallest_f < min_f) {
	min_f = smallest_f;
	closest_qf[0] = current_q[0];
	closest_qf[1] = current_q[1];
	closest_qf[2] = current_q[2];
	closest_qf[3] = current_q[3];
	index_f = i;
      }
    }

  //printf("MIN F FOUND %f:\n",min_f);
  //printf("MIN I FOUND %f:\n",min_i);
  //sleep(3);

  // Find shortest path between node closest to qi and node closest
  // to qf in the roadmap
  //printf("PASSING TO DIJKSTRA GRAPH\n");
  int *path;
  double *dist;
  int closest_qiID;
  int closest_qfID;
  zarray_get(state->qIDs,index_i,&closest_qiID);
  zarray_get(state->qIDs,index_f,&closest_qfID);
  // printf("CLOSEST qiID: %d\n", closest_qiID);
  //  printf("CLOSEST qfID: %d\n", closest_qfID);
  // printf("CLOSEST index i: %d\n", index_i);
  // printf("CLOSEST index f: %d\n", index_f);
  // sleep(3);
  dijkstra_calc_dest(g, closest_qiID, closest_qfID);
  //  printf("CALCULATED ALL\n");
  int p = dijkstra_get_path(g, closest_qiID, closest_qfID, &path, &dist);
  if (p == -1)
    return -1;
  state->pathSize = p;
  // printf("SIZE OF PATH: %d\n", p);
  // sleep(3);
  if(path != NULL)
    return path;
  return -1;
}
