// ----------------------------------------------------------------------------------------------
// ----- PATH_PLANNING.C -------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// These functions implement the potential field planner
// ----------------------------------------------------------------------------------------------

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include "path_planning.h"
#include "rexarm.h"

// ===============================================================================================
// ===== PATH PLAN CREATE ========================================================================
// ===============================================================================================
// This function implements a potential field planner from an initial joint configuration, q0 in
// degrees, to a final goal point, in xyz coordinates (meters) given obstancles, rings and
// cylinders in the workspace.  Epsilon is the maximum allowable error. Check final is a flag
// to set the state in the DFA (0-rough plan, 1-precise plan, 2-retrieve BB). Returns varray
// of configuration waypoints.
// ===============================================================================================
varray_t* path_plan_create(double *q0, rings_t *goal, varray_t* cylinders, varray_t* rings, double epsilon, int check_final){

int i;
  varray_t* q_path = varray_create(); // waypoint path in radians
  varray_t* q_path_deg = varray_create(); // waypoint path in degrees
  double* qi; // current configuration
  double* qi_rad = malloc(sizeof(double)*4); // current config in radians
  double* qi_deg = malloc(sizeof(double)*4); // current config in degrees

  double epsilon_m = 0.00000001; // tuning threshold
  double min_check[3] = {0,0,0}; // check for random walk

  double position[3]; // xyz position of goal
  position[0] = goal->xpos;
  position[1] = goal->ypos;
  position[2] = goal->zpos;

  // Initializing variables
  i = 0;
  qi = malloc(sizeof(double)*4);
  qi[0] = q0[0]*(M_PI/180.);
  qi[1] = q0[1]*(M_PI/180.);
  qi[2] = q0[2]*(M_PI/180.);
  qi[3] = q0[3]*(M_PI/180.);

  qi_deg[0] = qi[0]*(180./M_PI);
  qi_deg[1] = qi[1]*(180./M_PI);
  qi_deg[2] = qi[2]*(180./M_PI);
  qi_deg[3] = qi[3]*(180./M_PI);

  varray_add(q_path, qi);
  varray_add(q_path_deg, qi_deg);

  double* grad = malloc(sizeof(double)*4);
  double* qi_next = malloc(sizeof(double)*4);
  double norm1 = 1000000000000;
  forward_kinematics_t* current;

  qi_deg[0] = qi[0]*(180./M_PI);
  qi_deg[1] = qi[1]*(180./M_PI);
  qi_deg[2] = qi[2]*(180./M_PI);
  qi_deg[3] = qi[3]*(180./M_PI);

  current = forward_kinematics(qi_deg);

  grad = U_grad(qi, goal, current->T04, cylinders, rings, check_final);
  norm1 = sqrt(pow(grad[0],2) + pow(grad[1],2) + pow(grad[2],2)+ pow(grad[3],2));

  // Gradient descent
  double error = 1000000.0;
  int k;
  int num_walks = 0;
  while(error > epsilon)
    {
      if (k++ > 5000) break;
      printf("ERROR: %f\n",error);
      double alpha = 0.05;
      if(check_final > 0)      
	if (error < 0.06) 
	  alpha = 0.01; //0.005
      qi_next[0] = qi[0] + alpha*(grad[0]/norm1);
      qi_next[1] = qi[1] + alpha*(grad[1]/norm1);
      qi_next[2] = qi[2] + alpha*(grad[2]/norm1);
      qi_next[3] = qi[3] + alpha*(grad[3]/norm1);

      min_check[0] = min_check[1];
      min_check[1] = min_check[2];
      min_check[2] = error;

      double* qi_deg = malloc(sizeof(double)*4);
      qi_deg[0] = qi_next[0]*(180./M_PI);
      qi_deg[1] = qi_next[1]*(180./M_PI);
      qi_deg[2] = qi_next[2]*(180./M_PI);
      qi_deg[3] = qi_next[3]*(180./M_PI);

      // Set to 2 to allow random walks on final BB retrieval
      if (check_final == 4) {
	int willcollide = final_collision_check(qi_deg,goal,cylinders,rings);
	willcollide = 0;
          
	// Random walk
	if ((k > 4)|| willcollide==1) {
	  if (((((min_check[0]+0.0006)> min_check[1]) && (min_check[1] < (min_check[2]+0.0006))) || (min_check[2] > (min_check[0]+0.01))) || willcollide == 1)
	    {
	      int willcollide = 1;
	      num_walks++;
	     
	      double v = (double)rand()/(double)RAND_MAX;
	      double sign = (double)rand()/(double)RAND_MAX;

	      v = v/60.0;
	      if (sign > 0.5) v = -v;

	      double a = qi[0];
	      double b = qi[1];
	      double c = qi[2];
	      double d = qi[3];
	      double a_next,b_next,c_next,d_next;
	      int num_try = 0;
	      // Find collision free configuration to walk to
	      while(willcollide != 0) {
		num_try++;
		if(num_try > 500) {
		  break;
		}
		v = (double)rand()/(double)RAND_MAX;
		sign = (double)rand()/(double)RAND_MAX;
		v=v/60.0;
		printf("RANDOM: %f\n", v);
		if(sign > 0.5) v =-v;
		a_next = a + v;
		sign = (double)rand()/(double)RAND_MAX;
		if (sign > 0.5) v = -v;
		b_next = b + v;
		sign = (double)rand()/(double)RAND_MAX;
		if (sign > 0.5) v = -v;
		c_next = c + v;
		sign = (double)rand()/(double)RAND_MAX;
		if (sign > 0.5) v = -v;
		d_next = d + v;

		double* qi_next_deg = malloc(sizeof(double)*4);
		qi_next_deg[0] = a_next*(180./M_PI);
		qi_next_deg[1] = b_next*(180./M_PI);
		qi_next_deg[2] = c_next*(180./M_PI);
		qi_next_deg[3] = d_next*(180./M_PI);
		willcollide = final_collision_check(qi_next_deg,goal,cylinders, rings);
	      }
	      qi_next[3] = d_next;
	      qi[0] = a;
	      qi[1] = b;
	      qi[2] = c;
	      qi[3] = d;
	    }
	}
      }

      // Random walk for all other steps besides BB retrieval
      if (check_final != 2) {
	int willcollide = path_plan_collision_check(qi_deg,cylinders,rings);  
	if ((k > 4)|| willcollide==1) {
	  if (((((min_check[0]+0.0006)> min_check[1]) && (min_check[1] < (min_check[2]+0.0006))) || (min_check[2] > (min_check[0]+0.01))) || willcollide == 1)
	    {
	      int t = 0;

	      // Take 2 steps at a time
	      while(t < 2){
		int willcollide = 1;
		num_walks++;

		double v = (double)rand()/(double)RAND_MAX;
		double sign = (double)rand()/(double)RAND_MAX;
		v = v/10.0;
		if (sign > 0.5) v = -v;
		double a = qi[0];
		double b = qi[1];
		double c = qi[2];
		double d = qi[3];
		double a_next,b_next,c_next,d_next;
		int num_try = 0;
		while(willcollide != 0) {

		  // Break if tried over 500 configs and all were colliding
		  num_try++;
		  if(num_try > 500) {
		    break;
		  }
		  v = (double)rand()/(double)RAND_MAX;
		  sign = (double)rand()/(double)RAND_MAX;
		  v=v/10.0;
		  if(sign > 0.5) v =-v;
		  a_next = a + v;
		  sign = (double)rand()/(double)RAND_MAX;
		  if (sign > 0.5) v = -v;
		  b_next = b + v;
		  sign = (double)rand()/(double)RAND_MAX;
		  if (sign > 0.5) v = -v;
		  c_next = c + v;
		  sign = (double)rand()/(double)RAND_MAX;
		  if (sign > 0.5) v = -v;
		  d_next = d + v;

		  double* qi_next_deg = malloc(sizeof(double)*4);
		  qi_next_deg[0] = a_next*(180./M_PI);
		  qi_next_deg[1] = b_next*(180./M_PI);
		  qi_next_deg[2] = c_next*(180./M_PI);
		  qi_next_deg[3] = d_next*(180./M_PI);
		  willcollide = path_plan_collision_check(qi_next_deg,cylinders, rings);
		}
	  
		qi_next[0] = a_next;
		qi_next[1] = b_next;
		qi_next[2] = c_next;
		qi_next[3] = d_next;
		qi[0] = a;
		qi[1] = b;
		qi[2] = c;
		qi[3] = d;
		double* qi_next_deg = malloc(sizeof(double)*4);
		qi_next_deg[0] = a_next*(180./M_PI);
		qi_next_deg[1] = b_next*(180./M_PI);
		qi_next_deg[2] = c_next*(180./M_PI);
		qi_next_deg[3] = d_next*(180./M_PI);
		if (t < 4) {
		  varray_add(q_path, qi_next);
		  varray_add(q_path_deg, qi_next_deg);
		  i = i+1;
		}
		t++;
	      }
	    }
	}
      }

      qi_deg[0] = qi_next[0]*(180./M_PI);
      qi_deg[1] = qi_next[1]*(180./M_PI);
      qi_deg[2] = qi_next[2]*(180./M_PI);
      qi_deg[3] = qi_next[3]*(180./M_PI);
      
      varray_add(q_path, qi_next);
      varray_add(q_path_deg, qi_deg);

      i = i + 1;

      qi[0] = qi_next[0];
      qi[1] = qi_next[1];
      qi[2] = qi_next[2];
      qi[3] = qi_next[3];

      qi_deg[0] = qi[0]*(180./M_PI);
      qi_deg[1] = qi[1]*(180./M_PI);
      qi_deg[2] = qi[2]*(180./M_PI);
      qi_deg[3] = qi[3]*(180./M_PI);

      current = forward_kinematics(qi_deg);

      // Calculate potential
      grad = U_grad(qi, goal, current->T04, cylinders, rings, check_final);
      norm1 = sqrt(pow(grad[0],2) + pow(grad[1],2) + pow(grad[2],2)+ pow(grad[3],2));
 
      // Calculate error
      if (check_final == 1)
	error = sqrt(pow((goal->xworld - current->T04[3]),2) + pow((goal->yworld - current->T04[7]),2) + pow(((goal->zworld+0.04) - current->T04[11]),2));
      else if (check_final == 0)
	error = sqrt(pow((goal->xworld - current->T04[3]),2) + pow((goal->yworld - current->T04[7]),2) + pow(((goal->zworld) - current->T04[11]),2));
      else if (check_final == 2)
	error = sqrt(pow((goal->xworld - current->T04[3]),2) + pow((goal->yworld - current->T04[7]),2) + pow(((goal->zworld+0.005) - current->T04[11]),2));
    }

   double* get = varray_get(q_path_deg,varray_size(q_path_deg)- 1); 
 
   // Return error state if PF failed to find path with error less than 10 cm
  if (error > 0.1) {
    printf("FAILED PF\n");
    sleep(1);
    return -1;
  }

  printf("PATH PLANNER DONE\n");
  return q_path_deg;
}

// ===============================================================================================
// ===== U GRAD ==================================================================================
// ===============================================================================================
// This function returns the total gradient potential, accounting for repulsive and attractive
// forces, for the given configuration, qi, and goal. Cylinders and rings are obstacles, check
// final is described in path plan create, and T04 is the end effector configuration.
// ===============================================================================================
double* U_grad(double* qi, rings_t *goal, double *T04, varray_t * cylinders, varray_t * rings, int check_final)
{
  double* total_grad = malloc(sizeof(double)*4);
  double* qi_deg = malloc(sizeof(double)*4);

  double* grad_att = path_plan_attract(goal, T04, check_final);
  double midpoints[3] = {0,0,0};
  double* grad_rep = malloc(sizeof(double)*4);
  double* total_grad_rep = malloc(sizeof(double)*4);

  qi_deg[0] = qi[0]*(180./M_PI);
  qi_deg[1] = qi[1]*(180./M_PI);
  qi_deg[2] = qi[2]*(180./M_PI);
  qi_deg[3] = qi[3]*(180./M_PI);

  forward_kinematics_t *fk = forward_kinematics(qi_deg);

  // Origins
  double o0[3] = {0,0,0};
  double o1[3] = {fk->A01[3],fk->A01[7],fk->A01[11]};
  double o2[3] = {fk->T02[3],fk->T02[7],fk->T02[11]};
  double o3[3] = {fk->T03[3],fk->T03[7],fk->T03[11]};
  double o4[3] = {fk->T04[3],fk->T04[7],fk->T04[11]};

  double* float_pts = malloc(sizeof(double)*9);
  double* floating = path_plan_floating_point(o1, o2, cylinders, rings);
  float_pts[0] = floating[0];
  float_pts[1] = floating[1];
  float_pts[2] = floating[2];
  floating = path_plan_floating_point(o2, o3, cylinders, rings);
  float_pts[3] = floating[0];
  float_pts[4] = floating[1];
  float_pts[5] = floating[2];
  floating = path_plan_floating_point(o3, o4, cylinders, rings);
  float_pts[6] = floating[0];
  float_pts[7] = floating[1];
  float_pts[8] = floating[2];

  Jacobian_t* J = Jacobian(fk, float_pts);

  total_grad_rep[0] = 0.0;
  total_grad_rep[1] = 0.0;
  total_grad_rep[2] = 0.0;
  total_grad_rep[3] = 0.0;

  // Origin 1
  grad_rep = path_plan_repel(o1, cylinders, rings,0,check_final,qi_deg,goal);

  total_grad_rep[0] = total_grad_rep[0] + J->Jo1[0][0]*grad_rep[0] + J->Jo1[1][0]*grad_rep[1] + J->Jo1[2][0]*grad_rep[2];
  total_grad_rep[1] = total_grad_rep[1] + J->Jo1[0][1]*grad_rep[0] + J->Jo1[1][1]*grad_rep[1] + J->Jo1[2][1]*grad_rep[2];
  total_grad_rep[2] = total_grad_rep[2] + J->Jo1[0][2]*grad_rep[0] + J->Jo1[1][2]*grad_rep[1] + J->Jo1[2][2]*grad_rep[2];
  total_grad_rep[3] = total_grad_rep[3] + J->Jo1[0][3]*grad_rep[0] + J->Jo1[1][3]*grad_rep[1] + J->Jo1[2][3]*grad_rep[2];
  
  // Origin 2
  grad_rep = path_plan_repel(o2, cylinders, rings,0,check_final,qi_deg,goal);

  total_grad_rep[0] = total_grad_rep[0] + J->Jo2[0][0]*grad_rep[0] + J->Jo2[1][0]*grad_rep[1] + J->Jo2[2][0]*grad_rep[2];
  total_grad_rep[1] = total_grad_rep[1] + J->Jo2[0][1]*grad_rep[0] + J->Jo2[1][1]*grad_rep[1] + J->Jo2[2][1]*grad_rep[2];
  total_grad_rep[2] = total_grad_rep[2] + J->Jo2[0][2]*grad_rep[0] + J->Jo2[1][2]*grad_rep[1] + J->Jo2[2][2]*grad_rep[2];
  total_grad_rep[3] = total_grad_rep[3] + J->Jo2[0][3]*grad_rep[0] + J->Jo2[1][3]*grad_rep[1] + J->Jo2[2][3]*grad_rep[2];
 
  // Origin 3
  grad_rep = path_plan_repel(o3, cylinders, rings,0,check_final,qi_deg,goal);

  total_grad_rep[0] = total_grad_rep[0] + J->Jo3[0][0]*grad_rep[0] + J->Jo3[1][0]*grad_rep[1] + J->Jo3[2][0]*grad_rep[2];
  total_grad_rep[1] = total_grad_rep[1] + J->Jo3[0][1]*grad_rep[0] + J->Jo3[1][1]*grad_rep[1] + J->Jo3[2][1]*grad_rep[2];
  total_grad_rep[2] = total_grad_rep[2] + J->Jo3[0][2]*grad_rep[0] + J->Jo3[1][2]*grad_rep[1] + J->Jo3[2][2]*grad_rep[2];
  total_grad_rep[3] = total_grad_rep[3] + J->Jo3[0][3]*grad_rep[0] + J->Jo3[1][3]*grad_rep[1] + J->Jo3[2][3]*grad_rep[2];
 
  // End effector
  grad_rep = path_plan_repel(o4, cylinders, rings,1,check_final,qi_deg,goal);

  total_grad_rep[0] = total_grad_rep[0] + J->Jo4[0][0]*grad_rep[0] + J->Jo4[1][0]*grad_rep[1] + J->Jo4[2][0]*grad_rep[2];
  total_grad_rep[1] = total_grad_rep[1] + J->Jo4[0][1]*grad_rep[0] + J->Jo4[1][1]*grad_rep[1] + J->Jo4[2][1]*grad_rep[2];
  total_grad_rep[2] = total_grad_rep[2] + J->Jo4[0][2]*grad_rep[0] + J->Jo4[1][2]*grad_rep[1] + J->Jo4[2][2]*grad_rep[2];
  total_grad_rep[3] = total_grad_rep[3] + J->Jo4[0][3]*grad_rep[0] + J->Jo4[1][3]*grad_rep[1] + J->Jo4[2][3]*grad_rep[2];

  // Float 1
  double* float1 = malloc(sizeof(double)*3);
  float1[0] = float_pts[0];
  float1[1] = float_pts[1];
  float1[2] = float_pts[2];
  grad_rep = path_plan_repel(float1, cylinders, rings,0,check_final,qi_deg,goal);

  total_grad_rep[0] = total_grad_rep[0] + J->Jo1c[0][0]*grad_rep[0] + J->Jo1c[1][0]*grad_rep[1] + J->Jo1c[2][0]*grad_rep[2];
  total_grad_rep[1] = total_grad_rep[1] + J->Jo1c[0][1]*grad_rep[0] + J->Jo1c[1][1]*grad_rep[1] + J->Jo1c[2][1]*grad_rep[2];
  total_grad_rep[2] = total_grad_rep[2] + J->Jo1c[0][2]*grad_rep[0] + J->Jo1c[1][2]*grad_rep[1] + J->Jo1c[2][2]*grad_rep[2];
  total_grad_rep[3] = total_grad_rep[3] + J->Jo1c[0][3]*grad_rep[0] + J->Jo1c[1][3]*grad_rep[1] + J->Jo1c[2][3]*grad_rep[2];

  // Float 2
  double* float2 = malloc(sizeof(double)*3);
  float2[0] = float_pts[3];
  float2[1] = float_pts[4];
  float2[2] = float_pts[5];
  grad_rep = path_plan_repel(float2, cylinders, rings,0,check_final,qi_deg,goal);

  total_grad_rep[0] = total_grad_rep[0] + J->Jo2c[0][0]*grad_rep[0] + J->Jo2c[1][0]*grad_rep[1] + J->Jo2c[2][0]*grad_rep[2];
  total_grad_rep[1] = total_grad_rep[1] + J->Jo2c[0][1]*grad_rep[0] + J->Jo2c[1][1]*grad_rep[1] + J->Jo2c[2][1]*grad_rep[2];
  total_grad_rep[2] = total_grad_rep[2] + J->Jo2c[0][2]*grad_rep[0] + J->Jo2c[1][2]*grad_rep[1] + J->Jo2c[2][2]*grad_rep[2];
  total_grad_rep[3] = total_grad_rep[3] + J->Jo2c[0][3]*grad_rep[0] + J->Jo2c[1][3]*grad_rep[1] + J->Jo2c[2][3]*grad_rep[2];

  // Origin 1
  double* float3 = malloc(sizeof(double)*3);
  float3[0] = float_pts[6];
  float3[1] = float_pts[7];
  float3[2] = float_pts[8];
  grad_rep = path_plan_repel(float3, cylinders, rings,0,check_final,qi_deg,goal);

  total_grad_rep[0] = total_grad_rep[0] + J->Jo3c[0][0]*grad_rep[0] + J->Jo3c[1][0]*grad_rep[1] + J->Jo3c[2][0]*grad_rep[2];
  total_grad_rep[1] = total_grad_rep[1] + J->Jo3c[0][1]*grad_rep[0] + J->Jo3c[1][1]*grad_rep[1] + J->Jo3c[2][1]*grad_rep[2];
  total_grad_rep[2] = total_grad_rep[2] + J->Jo3c[0][2]*grad_rep[0] + J->Jo3c[1][2]*grad_rep[1] + J->Jo3c[2][2]*grad_rep[2];
  total_grad_rep[3] = total_grad_rep[3] + J->Jo3c[0][3]*grad_rep[0] + J->Jo3c[1][3]*grad_rep[1] + J->Jo3c[2][3]*grad_rep[2];

  total_grad[0] = total_grad_rep[0] + J->Jo4[0][0]*grad_att[0] + J->Jo4[1][0]*grad_att[1] + J->Jo4[2][0]*grad_att[2];
  total_grad[1] = total_grad_rep[1] + J->Jo4[0][1]*grad_att[0] + J->Jo4[1][1]*grad_att[1] + J->Jo4[2][1]*grad_att[2];
  total_grad[2] = total_grad_rep[2] + J->Jo4[0][2]*grad_att[0] + J->Jo4[1][2]*grad_att[1] + J->Jo4[2][2]*grad_att[2];
  total_grad[3] = total_grad_rep[3] + J->Jo4[0][3]*grad_att[0] + J->Jo4[1][3]*grad_att[1] + J->Jo4[2][3]*grad_att[2];

  return total_grad;
}

// ===============================================================================================
// ===== CROSS PRODUCT ===========================================================================
// ===============================================================================================
// This function returns the cross product computed for 3x1 vectors a and b.
// ===============================================================================================
double * crossProduct(double * a,double * b)
{
  double *c = malloc(sizeof(double)*3);

  c[0] = a[1]*b[2] - a[2]*b[1];
  c[1] = a[2]*b[0] - a[0]*b[2];
  c[2] = a[0]*b[1] - a[1]*b[0];
 
  return c;
}

// ===============================================================================================
// ===== JACOBIAN ================================================================================
// ===============================================================================================
// This function calculate Jacobian matrices for the given forward kinematics configuration,
// and the given floating points (midpoints).
// ===============================================================================================
Jacobian_t * Jacobian(forward_kinematics_t *fk, double * midpoints)
{
  
  Jacobian_t * x = (struct Jacobian*) malloc(sizeof(struct Jacobian));
  
  double o0[3] = {0,0,0};
  double z0[3] = {0,0,1};

  double o1[3] = {fk->A01[3],fk->A01[7],fk->A01[11]};
  double z1[3] = {fk->A01[2],fk->A01[6],fk->A01[10]};

  double o2[3] = {fk->T02[3],fk->T02[7],fk->T02[11]};
  double z2[3] = {fk->T02[2],fk->T02[6],fk->T02[10]};

  double o3[3] = {fk->T03[3],fk->T03[7],fk->T03[11]};
  double z3[3] = {fk->T03[2],fk->T03[6],fk->T03[10]};

  double o4[3] = {fk->T04[3],fk->T04[7],fk->T04[11]};
  double z4[3] = {fk->T04[2],fk->T04[6],fk->T04[10]};


  double o1c[3] = {midpoints[0], midpoints[1], midpoints[2]}; 
  double o2c[3] = {midpoints[3], midpoints[4], midpoints[5]};
  double o3c[3] = {midpoints[6], midpoints[7], midpoints[8]};

  int i;
  double* temp = malloc(sizeof(double)*3); //Check
  //Jo1
  for(i=1; i<4; i++)
    {
      x->Jo1[0][i] = 0;
      x->Jo1[1][i] = 0;
      x->Jo1[2][i] = 0;
      x->Jo1[3][i] = 0;
      x->Jo1[4][i] = 0;
      x->Jo1[5][i] = 0;

    }
  double * input = malloc(sizeof(double)*3);
  for (int i = 0; i < 3; i++)
    input[i] = (o1[i] - o0[i]);
  temp = (crossProduct(z0, input));
  x->Jo1[0][0] = temp[0];
  x->Jo1[1][0] = temp[1];
  x->Jo1[2][0] = temp[2];
  x->Jo1[3][0] = z0[0];
  x->Jo1[4][0] = z0[1];
  x->Jo1[5][0] = z0[2];


  //Jo2 and Jo1c
  for(i=2; i<4; i++)
    {
      x->Jo1c[0][i] = 0;
      x->Jo1c[1][i] = 0;
      x->Jo1c[2][i] = 0;
      x->Jo1c[3][i] = 0;
      x->Jo1c[4][i] = 0;
      x->Jo1c[5][i] = 0;

      x->Jo2[0][i] = 0;
      x->Jo2[1][i] = 0;
      x->Jo2[2][i] = 0;
      x->Jo2[3][i] = 0;
      x->Jo2[4][i] = 0;
      x->Jo2[5][i] = 0;
    }
  for (int i = 0; i < 3; i++)
    input[i] = o1c[i] - o0[i];
  temp = (crossProduct(z0, input));
  x->Jo1c[0][0] = temp[0];
  x->Jo1c[1][0] = temp[1];
  x->Jo1c[2][0] = temp[2];
  x->Jo1c[3][0] = z0[0];
  x->Jo1c[4][0] = z0[1];
  x->Jo1c[5][0] = z0[2];

  for (int i = 0; i < 3; i++)
    input[i] = o2[i] - o0[i];
  temp = (crossProduct(z0, input));
  x->Jo2[0][0] = temp[0];
  x->Jo2[1][0] = temp[1];
  x->Jo2[2][0] = temp[2];
  x->Jo2[3][0] = z0[0];
  x->Jo2[4][0] = z0[1];
  x->Jo2[5][0] = z0[2];

  for (int i = 0; i < 3; i++)
    input[i] = o1c[i] - o1[i];
  temp = (crossProduct(z1, input));
  x->Jo1c[0][1] = temp[0];
  x->Jo1c[1][1] = temp[1];
  x->Jo1c[2][1] = temp[2];
  x->Jo1c[3][1] = z1[0];
  x->Jo1c[4][1] = z1[1];
  x->Jo1c[5][1] = z1[2];

  for (int i = 0; i < 3; i++)
    input[i] = o2[i] - o1[i];
  temp = (crossProduct(z1, input));
  x->Jo2[0][1] = temp[0];
  x->Jo2[1][1] = temp[1];
  x->Jo2[2][1] = temp[2];
  x->Jo2[3][1] = z1[0];
  x->Jo2[4][1] = z1[1];
  x->Jo2[5][1] = z1[2];

  //Jo3 and Jo2c
  for(i=3; i<4; i++)
    {
      x->Jo2c[0][i] = 0;
      x->Jo2c[1][i] = 0;
      x->Jo2c[2][i] = 0;
      x->Jo2c[3][i] = 0;
      x->Jo2c[4][i] = 0;
      x->Jo2c[5][i] = 0;

      x->Jo3[0][i] = 0;
      x->Jo3[1][i] = 0;
      x->Jo3[2][i] = 0;
      x->Jo3[3][i] = 0;
      x->Jo3[4][i] = 0;
      x->Jo3[5][i] = 0;
    }

  for (int i = 0; i < 3; i++)
    input[i] = o2c[i] - o0[i];
  temp = (crossProduct(z0, input));
  x->Jo2c[0][0] = temp[0];
  x->Jo2c[1][0] = temp[1];
  x->Jo2c[2][0] = temp[2];
  x->Jo2c[3][0] = z0[0];
  x->Jo2c[4][0] = z0[1];
  x->Jo2c[5][0] = z0[2];

  for (int i = 0; i < 3; i++)
    input[i] = o3[i] - o0[i];
  temp = (crossProduct(z0, input));
  x->Jo3[0][0] = temp[0];
  x->Jo3[1][0] = temp[1];
  x->Jo3[2][0] = temp[2];
  x->Jo3[3][0] = z0[0];
  x->Jo3[4][0] = z0[1];
  x->Jo3[5][0] = z0[2];

  for (int i = 0; i < 3; i++)
    input[i] = o2c[i] - o1[i];
  temp = (crossProduct(z1, input));
  x->Jo2c[0][1] = temp[0];
  x->Jo2c[1][1] = temp[1];
  x->Jo2c[2][1] = temp[2];
  x->Jo2c[3][1] = z1[0];
  x->Jo2c[4][1] = z1[1];
  x->Jo2c[5][1] = z1[2];

  for (int i = 0; i < 3; i++)
    input[i] = o3[i] - o1[i];
  temp = (crossProduct(z1, input));
  x->Jo3[0][1] = temp[0];
  x->Jo3[1][1] = temp[1];
  x->Jo3[2][1] = temp[2];
  x->Jo3[3][1] = z1[0];
  x->Jo3[4][1] = z1[1];
  x->Jo3[5][1] = z1[2];

  for (int i = 0; i < 3; i++)
    input[i] = o2c[i] - o2[i];
  temp = (crossProduct(z2, input));
  x->Jo2c[0][2] = temp[0];
  x->Jo2c[1][2] = temp[1];
  x->Jo2c[2][2] = temp[2];
  x->Jo2c[3][2] = z2[0];
  x->Jo2c[4][2] = z2[1];
  x->Jo2c[5][2] = z2[2];

  for (int i = 0; i < 3; i++)
    input[i] = o3[i] - o2[i];
  temp = (crossProduct(z2, input));
  x->Jo3[0][2] = temp[0];
  x->Jo3[1][2] = temp[1];
  x->Jo3[2][2] = temp[2];
  x->Jo3[3][2] = z2[0];
  x->Jo3[4][2] = z2[1];
  x->Jo3[5][2] = z2[2];

  //Jo4 and Jo3c
  for (int i = 0; i < 3; i++)
    input[i] = o3c[i] - o0[i];
  temp = (crossProduct(z0, input));
  x->Jo3c[0][0] = temp[0];
  x->Jo3c[1][0] = temp[1];
  x->Jo3c[2][0] = temp[2];
  x->Jo3c[3][0] = z0[0];
  x->Jo3c[4][0] = z0[1];
  x->Jo3c[5][0] = z0[2];

  for (int i = 0; i < 3; i++)
    input[i] = o4[i] - o0[i];
  temp = (crossProduct(z0, input));
  x->Jo4[0][0] = temp[0];
  x->Jo4[1][0] = temp[1];
  x->Jo4[2][0] = temp[2];
  x->Jo4[3][0] = z0[0];
  x->Jo4[4][0] = z0[1];
  x->Jo4[5][0] = z0[2];

  for (int i = 0; i < 3; i++)
    input[i] = o3c[i] - o1[i];
  temp = (crossProduct(z1, input));
  x->Jo3c[0][1] = temp[0];
  x->Jo3c[1][1] = temp[1];
  x->Jo3c[2][1] = temp[2];
  x->Jo3c[3][1] = z1[0];
  x->Jo3c[4][1] = z1[1];
  x->Jo3c[5][1] = z1[2];

  for (int i = 0; i < 3; i++)
    input[i] = o4[i] - o1[i];
  temp = (crossProduct(z1, input));
  x->Jo4[0][1] = temp[0];
  x->Jo4[1][1] = temp[1];
  x->Jo4[2][1] = temp[2];
  x->Jo4[3][1] = z1[0];
  x->Jo4[4][1] = z1[1];
  x->Jo4[5][1] = z1[2];

  for (int i = 0; i < 3; i++)
    input[i] = o3c[i] - o2[i];
  temp = (crossProduct(z2, input));
  x->Jo3c[0][2] = temp[0];
  x->Jo3c[1][2] = temp[1];
  x->Jo3c[2][2] = temp[2];
  x->Jo3c[3][2] = z2[0];
  x->Jo3c[4][2] = z2[1];
  x->Jo3c[5][2] = z2[2];

  for (int i = 0; i < 3; i++)
    input[i] = o4[i] - o2[i];
  temp = (crossProduct(z2, input));
  x->Jo4[0][2] = temp[0];
  x->Jo4[1][2] = temp[1];
  x->Jo4[2][2] = temp[2];
  x->Jo4[3][2] = z2[0];
  x->Jo4[4][2] = z2[1];
  x->Jo4[5][2] = z2[2];

  for (int i = 0; i < 3; i++)
    input[i] = o3c[i] - o3[i];
  temp = (crossProduct(z3, input));
  x->Jo3c[0][3] = temp[0];
  x->Jo3c[1][3] = temp[1];
  x->Jo3c[2][3] = temp[2];
  x->Jo3c[3][3] = z3[0];
  x->Jo3c[4][3] = z3[1];
  x->Jo3c[5][3] = z3[2];

  for (int i = 0; i < 3; i++)
    input[i] = o4[i] - o3[i];
  temp = (crossProduct(z3, input));
  x->Jo4[0][3] = temp[0];
  x->Jo4[1][3] = temp[1];
  x->Jo4[2][3] = temp[2];
  x->Jo4[3][3] = z3[0];
  x->Jo4[4][3] = z3[1];
  x->Jo4[5][3] = z3[2];

  return x;
}

// ===============================================================================================
// ===== PATH PLAN ATTRACT =======================================================================
// ===============================================================================================
// This function returns the attractive force of a goal given as ring coordinates (xyz coordinates
// in meters), and a given configuration for the end effector, T04.  Check final is set as 
// described in path plan create.
// ===============================================================================================
double* path_plan_attract(rings_t *goal, double *T04, int check_final)
{
  double o_q[3];
  double o_qf[3];
  double Magnitude;
  double Uatt;
  double *Fatt = malloc(sizeof(double)*3);
  int i;

  //TUNING VARIABLES
  double d = 0.15; //meters.  distance from end effector to desired position
  double zeta = 7;

  o_q[0] = T04[3];
  o_q[1] = T04[7];
  o_q[2] = T04[11];
	
  o_qf[0]=goal->xworld;
  o_qf[1]=goal->yworld;
  if (check_final == 1)
    o_qf[2]=(goal->zworld)+0.04;
  else if (check_final == 0)
    o_qf[2]=(goal->zworld);
  else if (check_final == 2){
    o_qf[2]=(goal->zworld)+0.005;
  }
	
  Magnitude = sqrt(pow(o_q[0]-o_qf[0],2)+pow(o_q[1]-o_qf[1],2)+pow(o_q[2]-o_qf[2],2));

  if(Magnitude <= d)
    {
      Uatt = 0.5*zeta*pow(Magnitude,2);
     
      Fatt[0] = -1.0*15*(o_q[0]-o_qf[0]);
      Fatt[1] = -1.0*15*(o_q[1]-o_qf[1]);
      Fatt[2] = -1.0*zeta*(o_q[2]-o_qf[2]);
	
    }
  else if (Magnitude > d)
    {
      Uatt = (d*zeta*Magnitude) - (0.5*zeta*pow(d,2));
    
      Fatt[0] = -1.0*d*15*((o_q[0]-o_qf[0])/Magnitude);
      Fatt[1] = -1.0*d*15*((o_q[1]-o_qf[1])/Magnitude);
      Fatt[2] = -1.0*d*zeta*((o_q[2]-o_qf[2])/Magnitude);
    }

  return Fatt;
}

// ===============================================================================================
// ===== PATH PLAN REPEL =========================================================================
// ===============================================================================================
// This function returns the repel force for a given configuration, qi in radians, and obstacles
// in the workspace, cylinders and rings.  The flag is set if the joint being considered is the
// end effector.  Check final is set as described in path plan create, qi_deg is the configuration
// in degrees, and the goal is the final goal point in xyz coordinates (meters).
// ===============================================================================================
double* path_plan_repel(double * floating, varray_t * cylinders, varray_t * rings, int flag,int check_final,double* q,rings_t* goal)
{
  
	//Repulsions and Forces Cylinders
	double Urep_Cylinders = 0.0;
	double Frep_Cylinders[3] = {0,0,0};
	double Frep_Floor[3] = {0,0,0};
	
	//Repulsions and Forces Rings
	double Urep_Rings = 0.0;
	double Frep_Rings[3] = {0.,0.,0.};
	double Frep_Ceil[3] = {0.,0.,0.};
	double Frep_Inner[3] = {0.,0.,0.};
	
	//Total Repulsions and Forces
	double Urep_Total;
	double* Frep_Total = malloc(sizeof(double)*3);
	
	//Radii of Influence
	double rho0C = (.090/2.) +.04; 
	double rho0R_inner = .032 +.0025;
	double rho0R_outer = .074 + .02;
	double HeightC = .152 + .04;
	double HeightR = 0.019+0.008;
	
	//Counters and Temporary Variables
	int i;
	double Urep = 0.0;
	double Frep[3] = {0.,0.,0.};

	//Variables Used to Calculate Gradient
	double rho_oi = 0;//Shortest distance between oi and any workspace obstacle
	double Gradient_rho_oi[3]; //Unit vector directed from b toward oi (closest point)
	double* b = malloc(sizeof(double)*3); //Point on obstacle closest to oi
	cylinders_t* cylinder;
	//Tuning Variables
	double etaC= 5;
	double etaiR = 1;
	double etaoR = 0.1;
	double etaF = 0.15;
	double Urep_Floor = 0.0;
	double Urep_Ceil = 0.0;
	double Urep_Inner = 0.0;
	double Urep_Self = 0.0;
	double Frep_Self[3] = {0.,0.,0.};

	
	//Self Repel
	double rho0S = .03;
	double etaS = 1;
	double self_xyz[3] = {0, 0, 0.15};
	b = path_plan_closest_point(floating, self_xyz, 1);

	if (flag == 1){
	  if(floating[2] <= self_xyz[2])
	    {
	      if (sqrt(pow(floating[0]-self_xyz[0],2) + pow(floating[1]-self_xyz[1],2)) <= rho0S)
		{
		  rho_oi = sqrt(pow(b[0]-floating[0],2) + pow(floating[1]-b[1],2)+ pow(floating[2]-b[2],2));
		  Urep_Self = .5 * etaS * pow((1/rho_oi)-(1/rho0S),2);
		  Gradient_rho_oi[0] = (floating[0]-b[0])/rho_oi;
		  Gradient_rho_oi[1] = (floating[1]-b[1])/rho_oi;
		  Gradient_rho_oi[2] = (floating[2]-b[2])/rho_oi;
		  Frep_Self[0] = etaS * (1/rho_oi - 1/rho0S)*(1/pow(rho_oi,2))*Gradient_rho_oi[0];
		  Frep_Self[1] = etaS * (1/rho_oi - 1/rho0S)*(1/pow(rho_oi,2))*Gradient_rho_oi[1];
		  Frep_Self[2] = etaS * (1/rho_oi - 1/rho0S)*(1/pow(rho_oi,2))*Gradient_rho_oi[2];
		}
	      else if (floating[2] > (self_xyz[2]))
		{
		  Urep_Self = 0;
		  Frep_Self[0] = 0;
		  Frep_Self[1] = 0;
		  Frep_Self[2] = 0;
		}
	    }
	}

	// Floor
        if (check_final != 2){
	  if (floating[2] <= 0.03)//0.01
	    {
	      rho_oi = 0.02;
	      double rhoF = 0.003;
	      Urep_Floor = .5 * etaF * pow((1/rho_oi)-(1/rhoF),2);
			
	      Gradient_rho_oi[0] = 0.0;
	      Gradient_rho_oi[1] = 0.0;
	      Gradient_rho_oi[2] = 1;
			
	      Frep_Floor[0] = 0.0;
	      Frep_Floor[1] = 0.0;
	      Frep_Floor[2] = etaF*Gradient_rho_oi[2];
	    }
	}

	// CEILING
	if (floating[2] >= (0.375 - 0.03))
	  {
	    rho_oi = 0.02;
	    double rhoF = 0.003;
	    Urep_Ceil = .5 * etaF * pow((1/rho_oi)-(1/rhoF),2);
			
	    Gradient_rho_oi[0] = 0.0;
	    Gradient_rho_oi[1] = 0.0;
	    Gradient_rho_oi[2] = 1;
			
	    Frep_Ceil[0] = 0.0;
	    Frep_Ceil[1] = 0.0;
	    double etaC = 0.25;
	    Frep_Ceil[2] = etaC*Gradient_rho_oi[2];
	  }


	for(i = 0; i < varray_size(cylinders); i++)
	  {	

	    cylinder = varray_get(cylinders, i);

	    double cylinder_xyz[3] = {cylinder->xworld, cylinder->yworld, HeightC};

	    b = path_plan_closest_point(floating, cylinder_xyz, 1);

	    if(floating[2] <= cylinder_xyz[2])
	      {	
		if (sqrt(pow(floating[0]-cylinder_xyz[0],2) + pow(floating[1]-cylinder_xyz[1],2)) <= rho0C)
		  {	
		  rho_oi = sqrt(pow(b[0]-floating[0],2) + pow(floating[1]-b[1],2)+ pow(floating[2]-b[2],2));
		  Urep = .5 * etaC * pow((1/rho_oi)-(1/rho0C),2);
			
		  Gradient_rho_oi[0] = (floating[0]-b[0])/rho_oi;
		  Gradient_rho_oi[1] = (floating[1]-b[1])/rho_oi;
		  Gradient_rho_oi[2] = (floating[2]-b[2])/rho_oi;
			
		  Frep[0] = etaC * (1/rho_oi - 1/rho0C)*(1/pow(rho_oi,2))*Gradient_rho_oi[0]; 
		  Frep[1] = etaC * (1/rho_oi - 1/rho0C)*(1/pow(rho_oi,2))*Gradient_rho_oi[1]; 
		  Frep[2] = etaC * (1/rho_oi - 1/rho0C)*(1/pow(rho_oi,2))*Gradient_rho_oi[2]; 
		}
	      else if (floating[2] > (cylinder_xyz[2]))
		{
		  Urep = 0;
				
		  Frep[0] = 0;
		  Frep[1] = 0;
		  Frep[2] = 0;
		}		
	    }
					
	    Urep_Cylinders += Urep;

	    Frep_Cylinders[0] += Frep[0];
	    Frep_Cylinders[1] += Frep[1];
	    Frep_Cylinders[2] += Frep[2];
	  }

	if (check_final != 2) {
	  rings_t* ring;
	  for(i = 0; i < varray_size(rings); i++)
	    {
	      ring = varray_get(rings, i);
	      double ring_xyz[3] = {ring->xworld, ring->yworld, HeightR};
	      b = path_plan_closest_point(floating, ring_xyz, 0);
	      if(floating[2] <= HeightR)
		{
		  if (sqrt(pow(floating[0]-ring_xyz[0],2) + pow(floating[1]-ring_xyz[1],2)) <= rho0R_outer)
		    {
		      Urep = .5 * etaoR * pow((1/rho_oi)-(1/rho0R_outer),2);
		      rho_oi = sqrt(pow(b[0]-floating[0],2) + pow(floating[1]-b[1],2)+ pow(floating[2]-b[2],2));
			
		      Gradient_rho_oi[0] = (floating[0]-b[0])/rho_oi;
		      Gradient_rho_oi[1] = (floating[1]-b[1])/rho_oi;
		      Gradient_rho_oi[2] = (floating[2]-b[2])/rho_oi;
			
		      Frep[0] = etaoR * (1/rho_oi - 1/rho0R_outer)*(1/pow(rho_oi,2))*Gradient_rho_oi[0];
		      Frep[1] = etaoR * (1/rho_oi - 1/rho0R_outer)*(1/pow(rho_oi,2))*Gradient_rho_oi[1];
		      Frep[2] = etaoR * (1/rho_oi - 1/rho0R_outer)*(1/pow(rho_oi,2))*Gradient_rho_oi[2];
		    }
		  else if (floating[2] > ring_xyz[2])
		    {
		      Urep = 0;
				
		      Frep[0] = 0;
		      Frep[1] = 0;
		      Frep[2] = 0;
		    }
		}
	
					
	      Urep_Rings += Urep;
		
	      Frep_Rings[0] += Frep[0];
	      Frep_Rings[1] += Frep[1];
	      Frep_Rings[2] += Frep[2];
	    }
	}

	// FINAL RING REPEL (set to 2 if desired - not used)
	if (check_final == 4) {
	  double etaRinner = 0.001;
	  double rhoRinner = 0.001;
	  double goal_xyz[3];
	  double ring_h = 0.019 + 0.005;
	  goal_xyz[0] = goal->xworld;
	  goal_xyz[1] = goal->yworld;
	  goal_xyz[2] = 0.005;

	  double* o4 = malloc(sizeof(double)*4);
	  double* o3 = malloc(sizeof(double)*4);
	  double* l = malloc(sizeof(double)*2);
	  double* unit = malloc(sizeof(double)*2);
	  forward_kinematics_t* fk = forward_kinematics(q);
	  o4[0] = fk->T04[3];
	  o4[1] = fk->T04[7];
	  o4[2] = fk->T04[11];
	  o3[0] = fk->T03[3];
	  o3[1] = fk->T03[7];
	  o3[2] = fk->T03[11];
	  double orientation = (M_PI/180.)*(q[1] + q[2] + q[3]) - (M_PI/2.);

	  double c = abs(o4[2]-ring_h)/tan(orientation);
	  l[0] = o4[0]-o3[0];
	  l[1] = o4[1]-o3[1];
	  double norm = sqrt(pow(l[0],2)+pow(l[1],2));
	  unit[0] = l[0]/norm;
	  unit[1] = l[1]/norm;
	  floating[0]= c*unit[0];
	  floating[1]= c*unit[1];
	  floating[2]= .019;

	  double Rinner = .032/2.;
	  double* closest = malloc(sizeof(double)*3);
	  l[0] = floating[0]-goal_xyz[0];
	  l[1] = floating[1]-goal_xyz[1];
	  norm = sqrt(pow(l[0],2)+pow(l[1],2));
	  unit[0] = l[0]/norm;
	  unit[1] = l[1]/norm;
	  closest[0]= c*unit[0];
	  closest[1]= c*unit[1];
	  closest[2]= .019;

	  if (sqrt(pow(floating[0]-goal_xyz[0],2) + pow(floating[1]-goal_xyz[1],2)) >= rhoRinner)
	    {	
	      rho_oi = sqrt(pow(b[0]-floating[0],2) + pow(floating[1]-b[1],2)+ pow(floating[2]-b[2],2));
	      Urep = .5 * etaRinner * pow((1/rho_oi)-(1/rhoRinner),2);
	      Gradient_rho_oi[0] = -1.0*(floating[0]-b[0])/rho_oi;
	      Gradient_rho_oi[1] = -1.0*(floating[1]-b[1])/rho_oi;
	      Gradient_rho_oi[2] = -1.0*(floating[2]-b[2])/rho_oi;
			
	      Frep[0] = etaRinner * (1/rho_oi - 1/rhoRinner)*(1/pow(rho_oi,2))*Gradient_rho_oi[0]; 
	      Frep[1] = etaRinner * (1/rho_oi - 1/rhoRinner)*(1/pow(rho_oi,2))*Gradient_rho_oi[1]; 
	      Frep[2] = etaRinner * (1/rho_oi - 1/rhoRinner)*(1/pow(rho_oi,2))*Gradient_rho_oi[2]; 
	    }
	    
	  Urep_Inner += Urep;
		
	  Frep_Inner[0] += Frep[0];
	  Frep_Inner[1] += Frep[1];
	  Frep_Inner[2] += Frep[2];

	}

        Urep_Total = Urep_Cylinders + Urep_Rings + Urep_Floor + Urep_Ceil+Urep_Self+Urep_Inner;

	Frep_Total[0] = Frep_Cylinders[0] + Frep_Rings[0] + Frep_Floor[0] + Frep_Ceil[0]+Frep_Self[0]+Frep_Inner[0];
	Frep_Total[1] = Frep_Cylinders[1] + Frep_Rings[1] + Frep_Floor[1] + Frep_Ceil[1]+Frep_Self[1]+Frep_Inner[1];
	Frep_Total[2] = Frep_Cylinders[2] + Frep_Rings[2] + Frep_Floor[2] + Frep_Ceil[2]+Frep_Self[2]+Frep_Inner[2];	
	
	return Frep_Total;
}

// ===============================================================================================
// ===== PATH PLAN OBSTACLE CHECK  ===============================================================
// ===============================================================================================
// This function checks along the straight line path between the end effector positions posi and
// posf every 2 cm and returns 1 if there is an obstacle along the path, returns 0 otherwise.
// ===============================================================================================
int path_plan_obstacle_check(double* posi, double* posf, varray_t* cylinders, varray_t* rings)
{

 int willcollide = 0;

  double cylinder_height = .152 + 0.04;
  double cylinder_radius = .09/2+ 0.04;
  double rings_height = .019 + 0.02;
  double rings_radius = .074/2 + 0.02;
  double r, h, temp;
  int sizec, sizer;

  double oi[3] = {posi[0],posi[1],posi[2]};
  double of[3] = {posf[0],posf[1],posf[2]};

  varray_t* points = varray_create();
  double dist = 0.0;
  double total_dist = sqrt(pow(oi[0] - of[0],2) + pow(oi[1] - of[1],2) + pow(oi[2] - of[2],2));
  double incr = 0.02;

  while (dist < total_dist) {

    double u = incr/dist;
 
    double temp[3] = {0.0,0.0,0.0};
    temp[0] = (1-u)*oi[0] + u*of[0];
    temp[1] = (1-u)*oi[1] + u*of[1];
    temp[2] = (1-u)*oi[2] + u*of[2];

    dist = sqrt(pow(oi[0] - temp[0],2) + pow(oi[1] - temp[1],2) + pow(oi[2] - temp[2],2));

    if (dist < total_dist) varray_add(points, temp);

    incr = incr + 0.02;
  }

  double* o = malloc(sizeof(double)*3);
  for (int i = 0; i < varray_size(points); i++) {
    o = varray_get(points,i);

    //Floor
    if(o[2]<=0.03)
      {    
	willcollide=1;
	return willcollide;
      }
 
    // Ceiling
    if(o[2] >=( 0.375 - 0.03))
      {
	willcollide = 1;
	return willcollide;
      }

      //Outer bounds
      //sphere
      r = .326;
      temp = (pow(o[0],2)+pow(o[1],2)+pow(o[2],2));
      if(temp>=pow(r,2))
	{
	  willcollide=1;
	  return willcollide;
	}

      //Cylinder bounds
      sizec = varray_size(cylinders);
      for (int i=0; i<sizec; i++)
	{
	  cylinders_t* cylinder = varray_get(cylinders, i);
	  temp = (pow(o[0] - cylinder->xworld,2)+pow(o[1] - cylinder->yworld,2));
	  if((o[2]<=cylinder_height) && (temp<=pow(cylinder_radius,2)))
	    {
	      willcollide=1;
	      return willcollide;
	    }
     
	}
 
      //Rings bounds
      sizer = varray_size(rings);
      for (int i=0; i<sizer; i++)
	{
	  rings_t* ring = varray_get(rings, i);
	  temp = (pow(o[0] - ring->xworld,2)+pow(o[1] - ring->yworld,2));
	  if((o[2]<=rings_height) && (temp<=pow(rings_radius,2)))
	    {
	      willcollide=1;
	      return willcollide;
	    }
     
	}
  }
  return willcollide;

}

// ===============================================================================================
// ===== PATH PLAN SELF COLLISION  ===============================================================
// ===============================================================================================
// This function returns 1 if the joint configuration q (in deg) results in a self collision, 
// returns 0 otherwise.
// ===============================================================================================
int path_plan_self_collision(double* q) {

  int willcollide = 0;
  double temp = 0.0;
  varray_t* points = varray_create();

  forward_kinematics_t* fk = forward_kinematics(q);
  double o4[3] = {fk->T04[3],fk->T04[7],fk->T04[11]};
  double o[3] = {o4[0], o4[1], o4[2]};

  //Robot bounds
  double r = 0.06;
  double h = 0.15;
  temp = (pow(o[0],2)+pow(o[1],2));
  if((o[2]<=h) && (temp<=pow(r,2)))
    {
      willcollide=1;
      free(fk);
      varray_destroy(points);
      return willcollide;
    }

  free(fk);
  varray_destroy(points);
  return willcollide;
}

// ===============================================================================================
// ===== PATH PLAN BIASED COLLISION CHECK  =======================================================
// ===============================================================================================
// This function determines if a collision will occur for a given configuration, q, for
// obstacles of cylinders, ring, floor, ceiling, and self.  A bias is set to raise the floor
// and lower the ceiling to return collision-free configurations in a limited spaced.
// Useful for biasing random sampling for PRM. Returns 1 if collision occurs, returns 0 otherwise.
// ===============================================================================================
int path_plan_biased_collision_check(double* q, varray_t *cylinders, varray_t *rings){

  int willcollide = 0;

  double cylinder_height = .152 + 0.04;
  double cylinder_radius = .09/2+ 0.04;
  double rings_height = .019 + 0.02;
  double rings_radius = .074/2 + 0.02;
  double r, h, temp;
  int sizec, sizer;
  double* o = malloc(sizeof(double)*3);
  varray_t* points = varray_create();

  forward_kinematics_t* fk = forward_kinematics(q);

  // Origins
  double o0[3] = {0,0,0};
  double o1[3] = {fk->A01[3],fk->A01[7],fk->A01[11]};
  varray_add(points,o1);
  double o2[3] = {fk->T02[3],fk->T02[7],fk->T02[11]};
  varray_add(points, o2);
  double o3[3] = {fk->T03[3],fk->T03[7],fk->T03[11]};
  varray_add(points,o3);
  double o4[3] = {fk->T04[3],fk->T04[7],fk->T04[11]};
  varray_add(points,o4);

  // Check floating point collisions
  double* floating_1 = path_plan_floating_point(o1, o2, cylinders, rings);
  double* floating_2 = path_plan_floating_point(o2, o3, cylinders, rings);
  double* floating_3 = path_plan_floating_point(o3, o4, cylinders, rings);
  varray_add(points,floating_1);
  varray_add(points,floating_2);
  varray_add(points,floating_3);
  double q_deg[4] = {q[0]*(M_PI/180.),q[1]*(M_PI/180.),q[2]*(M_PI/180.),q[3]*(M_PI/180.)};

  // CLAMPS
  if(q_deg[0] < (JOINT1_NEG + (15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[0] > (JOINT1_POS-(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[1] < (JOINT2_NEGH+(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[1] > (JOINT2_POSH-(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[2] < (JOINT3_NEG+(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[2] > (JOINT3_POS-(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[3] < (JOINT4_NEG+(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[3] > (JOINT4_POS-(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }

  r = 0.06;
  h = 0.30;
  temp = (pow(o4[0],2)+pow(o4[1],2));
  if((o4[2]<=h) && (temp<=pow(r,2)))
    {
      willcollide=1;
      free(fk);
      varray_destroy(points);
      return willcollide;
    }

  // Check origin collision
  for (int i = 0; i < varray_size(points); i++){
    o = varray_get(points, i);

    //Floor
    if(o[2]<=0.07)
      {    
	willcollide=1;
	free(fk);
	varray_destroy(points);
	return willcollide;
      }
 
    // Ceiling
    if(o[2] >=(0.20))
      {
	willcollide = 1;
	free(fk);
	varray_destroy(points);
	return willcollide;
      }

    //Outer bounds
    //sphere
    r = .326;
    temp = (pow(o[0],2)+pow(o[1],2)+pow(o[2],2));
    if(temp>=pow(r,2))
      {
	willcollide=1;
	free(fk);
	varray_destroy(points);
	return willcollide;
      }

      //Cylinder bounds
      sizec = varray_size(cylinders);
      for (int i=0; i<sizec; i++)
	{
	  cylinders_t* cylinder = varray_get(cylinders, i);
	  temp = (pow(o[0] - cylinder->xworld,2)+pow(o[1] - cylinder->yworld,2));
	  if((o[2]<=cylinder_height) && (temp<=pow(cylinder_radius,2)))
	    {
	      willcollide=1;
	      free(fk);
	      varray_destroy(points);
	      return willcollide;
	    }
     
	}
 
      //Rings bounds
      sizer = varray_size(rings);
      for (int i=0; i<sizer; i++)
	{
	  rings_t* ring = varray_get(rings, i);
	  temp = (pow(o[0] - ring->xworld,2)+pow(o[1] - ring->yworld,2));
	  if((o[2]<=rings_height) && (temp<=pow(rings_radius,2)))
	    {
	      willcollide=1;
	      free(fk);
	      varray_destroy(points);
	      return willcollide;
	    }
     
	}
  }
  free(fk);
  varray_destroy(points);
  return willcollide;
}

// ===============================================================================================
// ===== PATH PLAN COLLISION CHECK  ==============================================================
// ===============================================================================================
// This function determines if a collision will occur for a given configuration, q, for
// obstacles of cylinders, ring, floor, ceiling, and self.  Returns 1 if collision occurs, 
// returns 0 otherwise.
// ===============================================================================================
int path_plan_collision_check(double* q, varray_t* cylinders, varray_t* rings){

  int willcollide = 0;

  double cylinder_height = .152 + 0.04;
  double cylinder_radius = .09/2+ 0.04;
  double rings_height = .019 + 0.02;
  double rings_radius = .074/2 + 0.02;
  double r, h, temp;
  int sizec, sizer;
  double* o = malloc(sizeof(double)*3);
  varray_t* points = varray_create();

  forward_kinematics_t* fk = forward_kinematics(q);

  // Origins
  double o0[3] = {0,0,0};
  double o1[3] = {fk->A01[3],fk->A01[7],fk->A01[11]};
  varray_add(points,o1);
  double o2[3] = {fk->T02[3],fk->T02[7],fk->T02[11]};
  varray_add(points, o2);
  double o3[3] = {fk->T03[3],fk->T03[7],fk->T03[11]};
  varray_add(points,o3);
  double o4[3] = {fk->T04[3],fk->T04[7],fk->T04[11]};
  varray_add(points,o4);

  // Check floating point collisions
  double* floating_1 = path_plan_floating_point(o1, o2, cylinders, rings);
  double* floating_2 = path_plan_floating_point(o2, o3, cylinders, rings);
  double* floating_3 = path_plan_floating_point(o3, o4, cylinders, rings);
  varray_add(points,floating_1);
  varray_add(points,floating_2);
  varray_add(points,floating_3);
  double q_deg[4] = {q[0]*(M_PI/180.),q[1]*(M_PI/180.),q[2]*(M_PI/180.),q[3]*(M_PI/180.)};

  // CLAMPS
  if(q_deg[0] < (JOINT1_NEG + (15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[0] > (JOINT1_POS-(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[1] < (JOINT2_NEGH+(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[1] > (JOINT2_POSH-(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[2] < (JOINT3_NEG+(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[2] > (JOINT3_POS-(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[3] < (JOINT4_NEG+(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }
  if(q_deg[3] > (JOINT4_POS-(15*M_PI/180.))){
    willcollide = 1;
 free(fk);
      varray_destroy(points);
    return willcollide;
  }

  r = 0.06;
  h = 0.30;
  temp = (pow(o4[0],2)+pow(o4[1],2));
  if((o4[2]<=h) && (temp<=pow(r,2)))
    {
      willcollide=1;
      free(fk);
      varray_destroy(points);
      return willcollide;
    }

  // Check origin collision
  for (int i = 0; i < varray_size(points); i++){
    o = varray_get(points, i);

    //Floor
    if(o[2]<=0.03)
      {    
	willcollide=1;
	free(fk);
	varray_destroy(points);
	return willcollide;
      }
 
    // Ceiling
    if(o[2] >=( 0.375 - 0.03))
      {
	willcollide = 1;
	free(fk);
	varray_destroy(points);
	return willcollide;
      }

      //Outer bounds
      //sphere
      r = .326;
      temp = (pow(o[0],2)+pow(o[1],2)+pow(o[2],2));
      if(temp>=pow(r,2))
	{
	  willcollide=1;
	  free(fk);
      varray_destroy(points);
	  return willcollide;
	}

      //Cylinder bounds
      sizec = varray_size(cylinders);
      for (int i=0; i<sizec; i++)
	{
	  cylinders_t* cylinder = varray_get(cylinders, i);
	  temp = (pow(o[0] - cylinder->xworld,2)+pow(o[1] - cylinder->yworld,2));
	  if((o[2]<=cylinder_height) && (temp<=pow(cylinder_radius,2)))
	    {
	      willcollide=1;
	      free(fk);
	      varray_destroy(points);
	      return willcollide;
	    }
     
	}
 
      //Rings bounds
      sizer = varray_size(rings);
      for (int i=0; i<sizer; i++)
	{
	  rings_t* ring = varray_get(rings, i);
	  temp = (pow(o[0] - ring->xworld,2)+pow(o[1] - ring->yworld,2));
	  if((o[2]<=rings_height) && (temp<=pow(rings_radius,2)))
	    {
	      willcollide=1;
	      free(fk);
	      varray_destroy(points);
	      return willcollide;
	    }
     
	}
  }
  free(fk);
  varray_destroy(points);
  return willcollide;
}

// ===============================================================================================
// ===== FINAL COLLISION CHECK ===================================================================
// ===============================================================================================
// This function is used for the final step in BB retrieval, and is the same as collision check
// with an added check for collisions inside of the ring's inner radius.  Returns 1 if collision
// occurs, 0 if otherwise.
// ===============================================================================================
int final_collision_check(double* q, rings_t* goal, varray_t* cylinders, varray_t* rings)
{
  int willcollide = 0;
  forward_kinematics_t* fk = forward_kinematics(q);
  varray_t* points = varray_create();
  // Origins
  double o0[3] = {0,0,0};
  double o1[3] = {fk->A01[3],fk->A01[7],fk->A01[11]};
  varray_add(points,o1);
  double o2[3] = {fk->T02[3],fk->T02[7],fk->T02[11]};
  varray_add(points, o2);
  double o3[3] = {fk->T03[3],fk->T03[7],fk->T03[11]};
  varray_add(points,o3);
  double o4[3] = {fk->T04[3],fk->T04[7],fk->T04[11]};
  varray_add(points,o4);

  // Check floating point collisions
  double* floating_1 = path_plan_floating_point(o1, o2, cylinders, rings);
  double* floating_2 = path_plan_floating_point(o2, o3, cylinders, rings);
  double* floating_3 = path_plan_floating_point(o3, o4, cylinders, rings);
  varray_add(points,floating_1);
  varray_add(points,floating_2);
  varray_add(points,floating_3);
  double* o = malloc(sizeof(double)*3);
  double rings_height = 0.019 + 0.01;
  double rings_inner_radius = (0.032/2.)-0.002;
  double temp = 0.0;

  // Check origin collision
  for (int i = 0; i < varray_size(points); i++){
    o = varray_get(points, i);

    temp = (pow(o[0] - goal->xworld,2)+pow(o[1] - goal->yworld,2));
    if((o[2]<=rings_height) && (temp>=pow(rings_inner_radius,2)))
      {
	willcollide=1;
	free(fk);
	varray_destroy(points);
	return willcollide;
      }

    if (o[2] < 0.00) {
      willcollide = 1;
      free(fk);
      varray_destroy(points);
      return willcollide;
    }


  }
  free(fk);
  varray_destroy(points);
  return willcollide;
}

// ===============================================================================================
// ===== PATH PLAN FLOATING POINT ================================================================
// ===============================================================================================
// This function returns the floating point on the arm between two origins that is closest
// to obstacles, including cylinders and rings.
// ===============================================================================================
double* path_plan_floating_point(double* origin_1, double* origin_2, varray_t* cylinders, varray_t* rings){

  double l2[3];
  double C;
  double floatingpoints[3];
  double Obs2[2];
  double min_floatingpoint[3];
  double min_dist = 100000;
  double dist = 0.0;
  double cylinder_xyz[3];
  double ring_xyz[3];


  double cylinder_radius = .09/2+.02;
  double ring_radius = .074/2+.02;


  //Link Vectors
  l2[0]=origin_1[0]-origin_2[0];
  l2[1]=origin_1[1]-origin_2[1];
  l2[2]=origin_1[2]-origin_2[2];

  double cyl_h = 0.152+.02;
  double ring_h = 0.019+.02;

  //ALL CYLINDER CHECKS
  for (int i = 0; i < (varray_size(cylinders)); i++) 
    {
      cylinders_t* cylinder = varray_get(cylinders, i);
      cylinder_xyz[0] = cylinder->xworld;
      cylinder_xyz[1] = cylinder->yworld;

      if ((origin_1[2] <= cyl_h) || (origin_2[2] <= cyl_h))
	{
	  //Vector from Obstacle to Upper origin
	  Obs2[0]=origin_2[0] - cylinder_xyz[0];
	  Obs2[1]=origin_2[1] - cylinder_xyz[1];		
		
	  //Projections of Obs vector onto link vector
	  C=(l2[0]*Obs2[0]+l2[1]*Obs2[1])/(sqrt(pow(l2[0],2)+pow(l2[1],2)));
	
	  floatingpoints[0] = l2[0]*C+origin_2[0];
	  floatingpoints[1] = l2[1]*C+origin_2[1];
	  floatingpoints[2] = l2[2]*C+origin_2[2];
	  
	  dist = sqrt(pow(floatingpoints[0] - cylinder_xyz[0], 2) + pow(floatingpoints[1] - cylinder_xyz[1], 2) + pow(floatingpoints[2] - cylinder_xyz[2], 2));

	  if (dist < min_dist){
	    min_floatingpoint[0] = floatingpoints[0];
	    min_floatingpoint[1] = floatingpoints[1];
	    min_floatingpoint[2] = floatingpoints[2];
	  }
	}
      	
      else if((origin_1[2] > cyl_h) && (origin_2[2] > cyl_h))
	{
	  double temp1 = (pow(origin_1[0] - cylinder->xworld,2)+pow(origin_1[1] - cylinder->yworld,2));
	  double temp2 = (pow(origin_2[0] - cylinder->xworld,2)+pow(origin_2[1] - cylinder->yworld,2));

	  if((temp1 > pow(cylinder_radius,2)) && (temp2 > pow(cylinder_radius,2)))
	    {

	      double o1_prime[3];
	      double o2_prime[3];
	      double m, a,b,c;
	      o1_prime[0] = origin_1[0] - cylinder_xyz[0];
	      o1_prime[1] = origin_1[1] - cylinder_xyz[1];
	      o1_prime[2] = origin_1[2] - cylinder_xyz[2];
	      o2_prime[0] = origin_2[0] - cylinder_xyz[0];
	      o2_prime[1] = origin_2[1] - cylinder_xyz[1];
	      o2_prime[2] = origin_2[2] - cylinder_xyz[2];
	      m = o2_prime[1] - o1_prime[1] / (o2_prime[0] - o1_prime[0]);
	      a = -m;
	      b = 1;
	      c = o1_prime[1] - (m*o1_prime[0]);
	      double intersect1[3];
	      double intersect2[3];
	      intersect1[0] = (a*c + b*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
	      intersect1[1] = (b*c - a*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
	      intersect2[0] = (a*c - b*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
	      intersect2[1] = (b*c + a*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));

	      if (origin_1[2] < origin_2[2]) 
		{
		  double C1 = sqrt(pow(origin_1[0] - intersect1[0],2) + pow(origin_1[1] - intersect1[1],1));
		  double C2 = sqrt(pow(origin_1[0] - intersect2[0],2) + pow(origin_1[1] - intersect2[1],1));

		  //Projections of Obs vector onto link vector
		  double l2[3];
		  l2[0] = -origin_1[0] + origin_2[0];
		  l2[1] = -origin_1[1] + origin_2[1];
		  l2[2] = -origin_1[2] + origin_2[2];

		  if (C1 < C2) 
		    {
		   
		      //Projections of Obs vector onto link vector

		      floatingpoints[0] = l2[0]*C1+origin_1[0];
		      floatingpoints[1] = l2[1]*C1+origin_1[1];
		      floatingpoints[2] = l2[2]*C1+origin_1[2];
		    }
		  else 
		    {
		      floatingpoints[0] = l2[0]*C2+origin_1[0];
		      floatingpoints[1] = l2[1]*C2+origin_1[1];
		      floatingpoints[2] = l2[2]*C2+origin_1[2];
		    }
		}	    
	      else if (origin_2[2] < origin_1[2]) 
		{
		  double C1 = sqrt(pow(origin_2[0] - intersect1[0],2) + pow(origin_2[1] - intersect1[1],1));
		  double C2 = sqrt(pow(origin_2[0] - intersect2[0],2) + pow(origin_2[1] - intersect2[1],1));

		  double l2[3];
		  l2[0] = origin_1[0] - origin_2[0];
		  l2[1] = origin_1[1] - origin_2[1];
		  l2[2] = origin_1[2] - origin_2[2];

		  if (C1 < C2) 
		    {
		      floatingpoints[0] = l2[0]*C1+origin_2[0];
		      floatingpoints[1] = l2[1]*C1+origin_2[1];
		      floatingpoints[2] = l2[2]*C1+origin_2[2];
		    }
		  else 
		    {
		      floatingpoints[0] = l2[0]*C2+origin_2[0];
		      floatingpoints[1] = l2[1]*C2+origin_2[1];
		      floatingpoints[2] = l2[2]*C2+origin_2[2];
		    }
		}
	      else if (origin_1[2] == origin_2[2])
		{
		  floatingpoints[0] = (origin_1[0] - origin_2[0]) / 2.;
		  floatingpoints[1] = (origin_1[1] - origin_2[1]) / 2.;
		  floatingpoints[2] = (origin_1[2] - origin_2[2]) / 2.;
		}
	    }

	  else if((temp1 <= pow(cylinder_radius,2)) && (temp2 <= pow(cylinder_radius,2)))
	    {
	      if(origin_1[2] < origin_2[2])
		{
		  floatingpoints[0] = origin_1[0];
		  floatingpoints[1] = origin_1[1];
		  floatingpoints[2] = origin_1[2];
		}
	      else
		{
		  floatingpoints[0] = origin_2[0];
		  floatingpoints[1] = origin_2[1];
		  floatingpoints[2] = origin_2[2];
		}
	    }

	  else if((temp1 <= pow(cylinder_radius,2)) && (temp2 > pow(cylinder_radius,2)))
	    {
	      if(origin_1[2] < origin_2[2])
		{
		  floatingpoints[0] = origin_1[0];
		  floatingpoints[1] = origin_1[1];
		  floatingpoints[2] = origin_1[2];
		}
	      else
		{

		  double o1_prime[3];
		  double o2_prime[3];
		  double m, a,b,c;
		  o1_prime[0] = origin_1[0] - cylinder_xyz[0];
		  o1_prime[1] = origin_1[1] - cylinder_xyz[1];
		  o1_prime[2] = origin_1[2] - cylinder_xyz[2];
		  o2_prime[0] = origin_2[0] - cylinder_xyz[0];
		  o2_prime[1] = origin_2[1] - cylinder_xyz[1];
		  o2_prime[2] = origin_2[2] - cylinder_xyz[2];
		  m = o2_prime[1] - o1_prime[1] / (o2_prime[0] - o1_prime[0]);
		  a = -m;
		  b = 1;
		  c = o1_prime[1] - (m*o1_prime[0]);
		  double intersect1[3];
		  double intersect2[3];
		  intersect1[0] = (a*c + b*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		  intersect1[1] = (b*c - a*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		  intersect2[0] = (a*c - b*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		  intersect2[1] = (b*c + a*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));


		  double tempa = sqrt(pow(origin_2[0] - intersect1[0],2) + pow(origin_2[1] - intersect1[1],1));  //Choose opposite origin to get intersection on link
		  double tempb = sqrt(pow(origin_2[0] - intersect2[0],2) + pow(origin_2[1] - intersect2[1],1));

		  if (tempa < tempb) 
		    {
		      double C1 = sqrt(pow(origin_1[0] - intersect1[0],2) + pow(origin_1[1] - intersect1[1],1));
		      double C2 = sqrt(pow(origin_2[0] - intersect1[0],2) + pow(origin_2[1] - intersect1[1],1));

		      //Projections of Obs vector onto link vector
		      double l2[3];

		      if (C1 < C2) 
			{
			  //Projections of Obs vector onto link vector
			  l2[0] = -origin_1[0] + origin_2[0];
			  l2[1] = -origin_1[1] + origin_2[1];
			  l2[2] = -origin_1[2] + origin_2[2];

			  floatingpoints[0] = l2[0]*C1+origin_1[0];
			  floatingpoints[1] = l2[1]*C1+origin_1[1];
			  floatingpoints[2] = l2[2]*C1+origin_1[2];
			}
		      else 
			{
			  l2[0] = origin_1[0] - origin_2[0];
			  l2[1] = origin_1[1] - origin_2[1];
			  l2[2] = origin_1[2] - origin_2[2];

			  floatingpoints[0] = l2[0]*C2+origin_1[0];
			  floatingpoints[1] = l2[1]*C2+origin_1[1];
			  floatingpoints[2] = l2[2]*C2+origin_1[2];
			}
		    }		  

		  else 
		    {		  
		      double C1 = sqrt(pow(origin_1[0] - intersect2[0],2) + pow(origin_1[1] - intersect2[1],1));
		      double C2 = sqrt(pow(origin_2[0] - intersect2[0],2) + pow(origin_2[1] - intersect2[1],1));

		      //Projections of Obs vector onto link vector
		      double l2[3];

		      if (C1 < C2) 
			{
			  //Projections of Obs vector onto link vector
			  l2[0] = -origin_1[0] + origin_2[0];
			  l2[1] = -origin_1[1] + origin_2[1];
			  l2[2] = -origin_1[2] + origin_2[2];

			  floatingpoints[0] = l2[0]*C1+origin_1[0];
			  floatingpoints[1] = l2[1]*C1+origin_1[1];
			  floatingpoints[2] = l2[2]*C1+origin_1[2];
			}
		      else 
			{
			  l2[0] = origin_1[0] - origin_2[0];
			  l2[1] = origin_1[1] - origin_2[1];
			  l2[2] = origin_1[2] - origin_2[2];

			  floatingpoints[0] = l2[0]*C2+origin_1[0];
			  floatingpoints[1] = l2[1]*C2+origin_1[1];
			  floatingpoints[2] = l2[2]*C2+origin_1[2];
			}
		    }
		}
	    }
	  else if((temp1 > pow(cylinder_radius,2)) && (temp2 <= pow(cylinder_radius,2)))
	    {
	      if(origin_1[2] > origin_2[2])
		{
		  floatingpoints[0] = origin_2[0];
		  floatingpoints[1] = origin_2[1];
		  floatingpoints[2] = origin_2[2];
		}
	      else
		{

		  double o1_prime[3];
		  double o2_prime[3];
		  double m, a,b,c;
		  o1_prime[0] = origin_1[0] - cylinder_xyz[0];
		  o1_prime[1] = origin_1[1] - cylinder_xyz[1];
		  o1_prime[2] = origin_1[2] - cylinder_xyz[2];
		  o2_prime[0] = origin_2[0] - cylinder_xyz[0];
		  o2_prime[1] = origin_2[1] - cylinder_xyz[1];
		  o2_prime[2] = origin_2[2] - cylinder_xyz[2];
		  m = o2_prime[1] - o1_prime[1] / (o2_prime[0] - o1_prime[0]);
		  a = -m;
		  b = 1;
		  c = o1_prime[1] - (m*o1_prime[0]);
		      double intersect1[3];
		      double intersect2[3];
		      intersect1[0] = (a*c + b*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		      intersect1[1] = (b*c - a*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		      intersect2[0] = (a*c - b*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		      intersect2[1] = (b*c + a*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));


		      double tempa = sqrt(pow(origin_1[0] - intersect1[0],2) + pow(origin_1[1] - intersect1[1],1));  //Choose opposite origin to get intersection on link
		      double tempb = sqrt(pow(origin_1[0] - intersect2[0],2) + pow(origin_1[1] - intersect2[1],1));

		      if (tempa < tempb) 
			{
			  double C1 = sqrt(pow(origin_1[0] - intersect1[0],2) + pow(origin_1[1] - intersect1[1],1));
			  double C2 = sqrt(pow(origin_2[0] - intersect1[0],2) + pow(origin_2[1] - intersect1[1],1));

			  //Projections of Obs vector onto link vector
			  double l2[3];

			  if (C1 < C2) 
			    {
			      //Projections of Obs vector onto link vector
			      l2[0] = -origin_1[0] + origin_2[0];
			      l2[1] = -origin_1[1] + origin_2[1];
			      l2[2] = -origin_1[2] + origin_2[2];

			      floatingpoints[0] = l2[0]*C1+origin_1[0];
			      floatingpoints[1] = l2[1]*C1+origin_1[1];
			      floatingpoints[2] = l2[2]*C1+origin_1[2];
			    }
			  else 
			    {
			      l2[0] = origin_1[0] - origin_2[0];
			      l2[1] = origin_1[1] - origin_2[1];
			      l2[2] = origin_1[2] - origin_2[2];

			      floatingpoints[0] = l2[0]*C2+origin_1[0];
			      floatingpoints[1] = l2[1]*C2+origin_1[1];
			      floatingpoints[2] = l2[2]*C2+origin_1[2];
			    }
			}		  
		      else 
			{		  
			  double C1 = sqrt(pow(origin_1[0] - intersect2[0],2) + pow(origin_1[1] - intersect2[1],1));
			  double C2 = sqrt(pow(origin_2[0] - intersect2[0],2) + pow(origin_2[1] - intersect2[1],1));

			  //Projections of Obs vector onto link vector
			  double l2[3];

			  if (C1 < C2) 
			    {
			      //Projections of Obs vector onto link vector
			      l2[0] = -origin_1[0] + origin_2[0];
			      l2[1] = -origin_1[1] + origin_2[1];
			      l2[2] = -origin_1[2] + origin_2[2];

			      floatingpoints[0] = l2[0]*C1+origin_1[0];
			      floatingpoints[1] = l2[1]*C1+origin_1[1];
			      floatingpoints[2] = l2[2]*C1+origin_1[2];
			    }
			  else 
			    {
			      l2[0] = origin_1[0] - origin_2[0];
			      l2[1] = origin_1[1] - origin_2[1];
			      l2[2] = origin_1[2] - origin_2[2];

			      floatingpoints[0] = l2[0]*C2+origin_1[0];
			      floatingpoints[1] = l2[1]*C2+origin_1[1];
			      floatingpoints[2] = l2[2]*C2+origin_1[2];
			    }
			}
		}
		
	    }  
	}
      dist = sqrt(pow(floatingpoints[0] - cylinder_xyz[0], 2) + pow(floatingpoints[1] - cylinder_xyz[1], 2) + pow(floatingpoints[2] - cylinder_xyz[2], 2));

      if (dist < min_dist)
	{
	  min_floatingpoint[0] = floatingpoints[0];
	  min_floatingpoint[1] = floatingpoints[1];
	  min_floatingpoint[2] = floatingpoints[2];
	}
    }

  //ALL RING CHECKS!
  for (int i = 0; i < (varray_size(rings)); i++) 
    {
      rings_t* cylinder = varray_get(rings, i);
      cylinder_xyz[0] = cylinder->xworld;
      cylinder_xyz[1] = cylinder->yworld;

      if ((origin_1[2] <= ring_h) || (origin_2[2] <= ring_h))
	{
	  //Vector from Obstacle to Upper origin
	  Obs2[0]=origin_2[0] - cylinder_xyz[0];
	  Obs2[1]=origin_2[1] - cylinder_xyz[1];		
		
	  //Projections of Obs vector onto link vector
	  C=(l2[0]*Obs2[0]+l2[1]*Obs2[1])/(sqrt(pow(l2[0],2)+pow(l2[1],2)));
	
	  floatingpoints[0] = l2[0]*C+origin_2[0];
	  floatingpoints[1] = l2[1]*C+origin_2[1];
	  floatingpoints[2] = l2[2]*C+origin_2[2];

	  dist = sqrt(pow(floatingpoints[0] - cylinder_xyz[0], 2) + pow(floatingpoints[1] - cylinder_xyz[1], 2) + pow(floatingpoints[2] - cylinder_xyz[2], 2));

	  if (dist < min_dist){
	    min_floatingpoint[0] = floatingpoints[0];
	    min_floatingpoint[1] = floatingpoints[1];
	    min_floatingpoint[2] = floatingpoints[2];
	  }
	}
      else if((origin_1[2] > ring_h) && (origin_2[2] > ring_h))
	{
	  double temp1 = (pow(origin_1[0] - cylinder->xworld,2)+pow(origin_1[1] - cylinder->yworld,2));
	  double temp2 = (pow(origin_2[0] - cylinder->xworld,2)+pow(origin_2[1] - cylinder->yworld,2));

	  if((temp1 > pow(ring_radius,2)) && (temp2 > pow(ring_radius,2)))
	    {

	      double o1_prime[3];
	      double o2_prime[3];
	      double m, a,b,c;
	      o1_prime[0] = origin_1[0] - cylinder_xyz[0];
	      o1_prime[1] = origin_1[1] - cylinder_xyz[1];
	      o1_prime[2] = origin_1[2] - cylinder_xyz[2];
	      o2_prime[0] = origin_2[0] - cylinder_xyz[0];
	      o2_prime[1] = origin_2[1] - cylinder_xyz[1];
	      o2_prime[2] = origin_2[2] - cylinder_xyz[2];
	      m = o2_prime[1] - o1_prime[1] / (o2_prime[0] - o1_prime[0]);
	      a = -m;
	      b = 1;
	      c = o1_prime[1] - (m*o1_prime[0]);
	      double intersect1[3];
	      double intersect2[3];
	      intersect1[0] = (a*c + b*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
	      intersect1[1] = (b*c - a*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
	      intersect2[0] = (a*c - b*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
	      intersect2[1] = (b*c + a*sqrt(pow(cylinder_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));

	      if (origin_1[2] < origin_2[2]) 
		{
		  double C1 = sqrt(pow(origin_1[0] - intersect1[0],2) + pow(origin_1[1] - intersect1[1],1));
		  double C2 = sqrt(pow(origin_1[0] - intersect2[0],2) + pow(origin_1[1] - intersect2[1],1));

		  //Projections of Obs vector onto link vector
		  double l2[3];
		  l2[0] = -origin_1[0] + origin_2[0];
		  l2[1] = -origin_1[1] + origin_2[1];
		  l2[2] = -origin_1[2] + origin_2[2];

		  if (C1 < C2) 
		    {
		      //Projections of Obs vector onto link vector

		      floatingpoints[0] = l2[0]*C1+origin_1[0];
		      floatingpoints[1] = l2[1]*C1+origin_1[1];
		      floatingpoints[2] = l2[2]*C1+origin_1[2];
		    }
		  else 
		    {
		      floatingpoints[0] = l2[0]*C2+origin_1[0];
		      floatingpoints[1] = l2[1]*C2+origin_1[1];
		      floatingpoints[2] = l2[2]*C2+origin_1[2];
		    }
		}	    
	      else if (origin_2[2] < origin_1[2]) 
		{
		  double C1 = sqrt(pow(origin_2[0] - intersect1[0],2) + pow(origin_2[1] - intersect1[1],1));
		  double C2 = sqrt(pow(origin_2[0] - intersect2[0],2) + pow(origin_2[1] - intersect2[1],1));

		  double l2[3];
		  l2[0] = origin_1[0] - origin_2[0];
		  l2[1] = origin_1[1] - origin_2[1];
		  l2[2] = origin_1[2] - origin_2[2];

		  if (C1 < C2) 
		    {
		      floatingpoints[0] = l2[0]*C1+origin_2[0];
		      floatingpoints[1] = l2[1]*C1+origin_2[1];
		      floatingpoints[2] = l2[2]*C1+origin_2[2];
		    }
		  else 
		    {
		      floatingpoints[0] = l2[0]*C2+origin_2[0];
		      floatingpoints[1] = l2[1]*C2+origin_2[1];
		      floatingpoints[2] = l2[2]*C2+origin_2[2];
		    }
		}
	      else if (origin_1[2] == origin_2[2])
		{
		  floatingpoints[0] = (origin_1[0] - origin_2[0]) / 2.;
		  floatingpoints[1] = (origin_1[1] - origin_2[1]) / 2.;
		  floatingpoints[2] = (origin_1[2] - origin_2[2]) / 2.;
		}
	    }
	  else if((temp1 <= pow(ring_radius,2)) && (temp2 <= pow(ring_radius,2)))
	    {
	      if(origin_1[2] < origin_2[2])
		{
		  floatingpoints[0] = origin_1[0];
		  floatingpoints[1] = origin_1[1];
		  floatingpoints[2] = origin_1[2];
		}
	      else
		{
		  floatingpoints[0] = origin_2[0];
		  floatingpoints[1] = origin_2[1];
		  floatingpoints[2] = origin_2[2];
		}
	    }
	  else if((temp1 <= pow(ring_radius,2)) && (temp2 > pow(ring_radius,2)))
	    {
	      if(origin_1[2] < origin_2[2])
		{
		  floatingpoints[0] = origin_1[0];
		  floatingpoints[1] = origin_1[1];
		  floatingpoints[2] = origin_1[2];
		}
	      else
		{

		  double o1_prime[3];
		  double o2_prime[3];
		  double m, a,b,c;
		  o1_prime[0] = origin_1[0] - cylinder_xyz[0];
		  o1_prime[1] = origin_1[1] - cylinder_xyz[1];
		  o1_prime[2] = origin_1[2] - cylinder_xyz[2];
		  o2_prime[0] = origin_2[0] - cylinder_xyz[0];
		  o2_prime[1] = origin_2[1] - cylinder_xyz[1];
		  o2_prime[2] = origin_2[2] - cylinder_xyz[2];
		  m = o2_prime[1] - o1_prime[1] / (o2_prime[0] - o1_prime[0]);
		  a = -m;
		  b = 1;
		  c = o1_prime[1] - (m*o1_prime[0]);
		  double intersect1[3];
		  double intersect2[3];
		  intersect1[0] = (a*c + b*sqrt(pow(ring_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		  intersect1[1] = (b*c - a*sqrt(pow(ring_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		  intersect2[0] = (a*c - b*sqrt(pow(ring_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		  intersect2[1] = (b*c + a*sqrt(pow(ring_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));


		  double tempa = sqrt(pow(origin_2[0] - intersect1[0],2) + pow(origin_2[1] - intersect1[1],1));  //Choose opposite origin to get intersection on link
		  double tempb = sqrt(pow(origin_2[0] - intersect2[0],2) + pow(origin_2[1] - intersect2[1],1));

		  if (tempa < tempb) 
		    {
		      double C1 = sqrt(pow(origin_1[0] - intersect1[0],2) + pow(origin_1[1] - intersect1[1],1));
		      double C2 = sqrt(pow(origin_2[0] - intersect1[0],2) + pow(origin_2[1] - intersect1[1],1));

		      //Projections of Obs vector onto link vector
		      double l2[3];

		      if (C1 < C2) 
			{
			  //Projections of Obs vector onto link vector
			  l2[0] = -origin_1[0] + origin_2[0];
			  l2[1] = -origin_1[1] + origin_2[1];
			  l2[2] = -origin_1[2] + origin_2[2];

			  floatingpoints[0] = l2[0]*C1+origin_1[0];
			  floatingpoints[1] = l2[1]*C1+origin_1[1];
			  floatingpoints[2] = l2[2]*C1+origin_1[2];
			}
		      else 
			{
			  l2[0] = origin_1[0] - origin_2[0];
			  l2[1] = origin_1[1] - origin_2[1];
			  l2[2] = origin_1[2] - origin_2[2];

			  floatingpoints[0] = l2[0]*C2+origin_1[0];
			  floatingpoints[1] = l2[1]*C2+origin_1[1];
			  floatingpoints[2] = l2[2]*C2+origin_1[2];
			}
		    }		  

		  else 
		    {		  
		      double C1 = sqrt(pow(origin_1[0] - intersect2[0],2) + pow(origin_1[1] - intersect2[1],1));
		      double C2 = sqrt(pow(origin_2[0] - intersect2[0],2) + pow(origin_2[1] - intersect2[1],1));

		      //Projections of Obs vector onto link vector
		      double l2[3];

		      if (C1 < C2) 
			{
			  //Projections of Obs vector onto link vector
			  l2[0] = -origin_1[0] + origin_2[0];
			  l2[1] = -origin_1[1] + origin_2[1];
			  l2[2] = -origin_1[2] + origin_2[2];

			  floatingpoints[0] = l2[0]*C1+origin_1[0];
			  floatingpoints[1] = l2[1]*C1+origin_1[1];
			  floatingpoints[2] = l2[2]*C1+origin_1[2];
			}
		      else 
			{
			  l2[0] = origin_1[0] - origin_2[0];
			  l2[1] = origin_1[1] - origin_2[1];
			  l2[2] = origin_1[2] - origin_2[2];

			  floatingpoints[0] = l2[0]*C2+origin_1[0];
			  floatingpoints[1] = l2[1]*C2+origin_1[1];
			  floatingpoints[2] = l2[2]*C2+origin_1[2];
			}
		    }
		}

	    }
	  else if((temp1 > pow(ring_radius,2)) && (temp2 <= pow(ring_radius,2)))
	    {
	      if(origin_1[2] > origin_2[2])
		{
		  floatingpoints[0] = origin_2[0];
		  floatingpoints[1] = origin_2[1];
		  floatingpoints[2] = origin_2[2];
		}
	      else
		{

		  double o1_prime[3];
		  double o2_prime[3];
		      double m, a,b,c;
		      o1_prime[0] = origin_1[0] - cylinder_xyz[0];
		      o1_prime[1] = origin_1[1] - cylinder_xyz[1];
		      o1_prime[2] = origin_1[2] - cylinder_xyz[2];
		      o2_prime[0] = origin_2[0] - cylinder_xyz[0];
		      o2_prime[1] = origin_2[1] - cylinder_xyz[1];
		      o2_prime[2] = origin_2[2] - cylinder_xyz[2];
		      m = o2_prime[1] - o1_prime[1] / (o2_prime[0] - o1_prime[0]);
		      a = -m;
		      b = 1;
		      c = o1_prime[1] - (m*o1_prime[0]);
		      double intersect1[3];
		      double intersect2[3];
		      intersect1[0] = (a*c + b*sqrt(pow(ring_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		      intersect1[1] = (b*c - a*sqrt(pow(ring_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		      intersect2[0] = (a*c - b*sqrt(pow(ring_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));
		      intersect2[1] = (b*c + a*sqrt(pow(ring_radius,2)*(pow(a,2) + pow(b,2)) - pow(c,2))) / (pow(a,2) + pow(b,2));


		      double tempa = sqrt(pow(origin_1[0] - intersect1[0],2) + pow(origin_1[1] - intersect1[1],1));  //Choose opposite origin to get intersection on link
		      double tempb = sqrt(pow(origin_1[0] - intersect2[0],2) + pow(origin_1[1] - intersect2[1],1));

		      if (tempa < tempb) 
			{
			  double C1 = sqrt(pow(origin_1[0] - intersect1[0],2) + pow(origin_1[1] - intersect1[1],1));
			  double C2 = sqrt(pow(origin_2[0] - intersect1[0],2) + pow(origin_2[1] - intersect1[1],1));

			  //Projections of Obs vector onto link vector
			  double l2[3];

			  if (C1 < C2) 
			    {
			      //Projections of Obs vector onto link vector
			      l2[0] = -origin_1[0] + origin_2[0];
			      l2[1] = -origin_1[1] + origin_2[1];
			      l2[2] = -origin_1[2] + origin_2[2];

			      floatingpoints[0] = l2[0]*C1+origin_1[0];
			      floatingpoints[1] = l2[1]*C1+origin_1[1];
			      floatingpoints[2] = l2[2]*C1+origin_1[2];
			    }
			  else 
			    {
			      l2[0] = origin_1[0] - origin_2[0];
			      l2[1] = origin_1[1] - origin_2[1];
			      l2[2] = origin_1[2] - origin_2[2];

			      floatingpoints[0] = l2[0]*C2+origin_1[0];
			      floatingpoints[1] = l2[1]*C2+origin_1[1];
			      floatingpoints[2] = l2[2]*C2+origin_1[2];
			    }
			}		  
		      else 
			{		  
			  double C1 = sqrt(pow(origin_1[0] - intersect2[0],2) + pow(origin_1[1] - intersect2[1],1));
			  double C2 = sqrt(pow(origin_2[0] - intersect2[0],2) + pow(origin_2[1] - intersect2[1],1));

			  //Projections of Obs vector onto link vector
			  double l2[3];

			  if (C1 < C2) 
			    {
			      //Projections of Obs vector onto link vector
			      l2[0] = -origin_1[0] + origin_2[0];
			      l2[1] = -origin_1[1] + origin_2[1];
			      l2[2] = -origin_1[2] + origin_2[2];

			      floatingpoints[0] = l2[0]*C1+origin_1[0];
			      floatingpoints[1] = l2[1]*C1+origin_1[1];
			      floatingpoints[2] = l2[2]*C1+origin_1[2];
			    }
			  else 
			    {
			      l2[0] = origin_1[0] - origin_2[0];
			      l2[1] = origin_1[1] - origin_2[1];
			      l2[2] = origin_1[2] - origin_2[2];

			      floatingpoints[0] = l2[0]*C2+origin_1[0];
			      floatingpoints[1] = l2[1]*C2+origin_1[1];
			      floatingpoints[2] = l2[2]*C2+origin_1[2];
			    }
			}
		}
	    }
	}
      dist = sqrt(pow(floatingpoints[0] - cylinder_xyz[0], 2) + pow(floatingpoints[1] - cylinder_xyz[1], 2) + pow(floatingpoints[2] - cylinder_xyz[2], 2));

      if (dist < min_dist)
	{
	  min_floatingpoint[0] = floatingpoints[0];
	  min_floatingpoint[1] = floatingpoints[1];
	  min_floatingpoint[2] = floatingpoints[2];
	}
    }	
				
  return min_floatingpoint;
}

// ===============================================================================================
// ===== PATH PLAN CLOSEST POINT =================================================================
// ===============================================================================================
// This function returns the closest point on an obstacle for a given floating point (or origin),
// where type refers to the type of obstacle (0-cylinder, 1-ring).
// ===============================================================================================
double* path_plan_closest_point(double* floatingpoints, double* Obstacle, int type){
  double cylinder_radius = .045;	//Radius of cylinder
  double rings_radius = .074/2;    //Radius of Rings
  double radius;
	  
  double origin_1[3];
  double origin_2[3];
  double origin_3[3];
  double origin_4[3];
	  
  double *closestpoint = malloc(sizeof(double)*18);
	  
  double intersect2[3];
  double intersect3[3];
  double intersect4[3];
  double intersect2c[3];
  double intersect3c[3];
  double intersect4c[3];

  //Set Origins at current configuration
	  
  if(type == 1)
    radius = cylinder_radius;
  else
    radius = rings_radius;

  double intersect[3];
  double* closest = malloc(sizeof(double)*3);

  intersect[0]=floatingpoints[0]-Obstacle[0];
  intersect[1]=floatingpoints[1]-Obstacle[1];
  intersect[2]=floatingpoints[2]-Obstacle[2];

  closest[0]=radius*intersect[0]+Obstacle[0];
  closest[1]=radius*intersect[1]+Obstacle[1];
  closest[2]=radius*intersect[2]+Obstacle[2];

  return closest;
}
