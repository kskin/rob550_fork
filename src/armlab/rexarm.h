// ----------------------------------------------------------------------------------------------
// ----- REXARM.H -------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// This file handles rexarm kinematics and implements clamps and limits to avoid damage
// to the arm.
// ----------------------------------------------------------------------------------------------

#ifndef __REXARM_H__
#define __REXARM_H__

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include <stdint.h>
#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_cblas.h>
#include <math/gsl_util_matrix.h>

// core api
#include "vx/vx.h"
#include "vx/vx_util.h"
#include "vx/vx_remote_display_source.h"
#include "vx/gtk/vx_gtk_display_source.h"

#include "structures.h"

// drawables
#include "vx/vxo_drawables.h"

// ==============================================================================================
// ===== DEFINITIONS ============================================================================
// ==============================================================================================
#define JOINT1_POS 3.123 // radians
#define JOINT1_NEG -3.123
#define JOINT2_NEGH -2.169
#define JOINT2_POSH 2.170
#define JOINT2_NEGS -2.148
#define JOINT2_POSS 2.153
#define JOINT3_NEG -2.112
#define JOINT3_POS 2.210
#define JOINT4_NEG -2.234
#define JOINT4_POS 2.229

#define EEl .057 // meters
#define B4l .04
#define C4l .022
#define B3l .101
#define C3l .027
#define B2l .101
#define C2l .027
#define B1l .038
#define C0l .005
#define B0l .07146

// ===============================================================================================
// ===== REXARM CLAMP ============================================================================
// ===============================================================================================
// This function takes in a configuration and sets flags if the configuration is beyond physical
// limits of the arm. Returns both attempted joint angles and flags (1-invalid, 0-valid).
// ===============================================================================================
double *rexarm_clamp(double q[8]);

// ===============================================================================================
// ===== CHECK LIMITS ============================================================================
// ===============================================================================================
// This function returns 1 if the forward kinematics configuration results in a self collision
// or floor collision, returns 0 otherwise.
// ===============================================================================================
int check_limits(forward_kinematics_t *k);

// ===============================================================================================
// ===== FORWARD KINEMATICS ======================================================================
// ===============================================================================================
// This function computes forward kinematics parameters for a given joint configuration (deg).
// ===============================================================================================
forward_kinematics_t * forward_kinematics(double * q);

// ===============================================================================================
// ===== INVERSE KINEMATICS ======================================================================
// ===============================================================================================
// This function computes inverse kinematics parameters for a desired xyz position (m), and 
// orientation of the end effector.  Returns -1 if invalid configuration.
// ===============================================================================================
inverse_kinematics_t * inverse_kinematics(double * position, double orientation);

// ===============================================================================================
// ===== REXARM VXO ARM ==========================================================================
// ===============================================================================================
// Creates rendering object for rexarm.  Arm is green if it is not in an error state, and joint
// turns red if it is in an error state.
// ===============================================================================================
vx_object_t * rexarm_vxo_arm(float errors[4], double angles[4], float arm_color[4], float alpha);

#endif
