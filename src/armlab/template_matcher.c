//-----------------------------------------------------------------------------------------
//----- TEMPLATE_MATCHER.C ------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
//-----------------------------------------------------------------------------------------
// This file contains the main code for template matching. It is called from armlab.c
//-----------------------------------------------------------------------------------------

// =========================================================================================
// ===== DEPENDENCIES ======================================================================
// =========================================================================================
#include "template_matcher.h"
#include <sched.h>

// =========================================================================================
// ===== TEMPLATE_MATCHER_FIND =============================================================
// =========================================================================================
// This function loops through the given image and calculates the lowest minimum from the
// given template. Once it finds all local minimum template matches, it creates an array
// of template locations and returns the array.
// =========================================================================================

varray_t * template_matcher_find(const image_u32_t *image, const image_u32_t *template, double thresh, int nearest_pixels, double decratio){
	double error = 0;
	image_u32_t * decImage = image_util_u32_decimate(image, 8.0);
        image_u32_t * decTemplate = image_util_u32_decimate(template, 8.0);
	int twidth = decTemplate->width;
	int theight = decTemplate->height;
	int iwidth = decImage->width;
	int iheight = decImage->height;
	int tstride = decTemplate->stride;
	zarray_t *errors = zarray_create(sizeof(double));
	varray_t *locations = varray_create ();
	
	//Loop through the image and match the template to the image
	for(int image_offset_x = 0; image_offset_x < (iwidth-twidth); image_offset_x ++){
		for(int image_offset_y = 0; image_offset_y < (iheight-theight); image_offset_y ++){
			for (int ty = 0; ty < theight; ty++) {
  				for (int tx = 0; tx < twidth; tx++) {
    					int t_idx = ty*template->stride + tx;
    					int im_idx = (image_offset_y+ty)*image->stride + (image_offset_x+tx);
					int template_abgr = template->buf[t_idx];
    					int image_abgr = image->buf[im_idx];

    					int template_red = (template_abgr >> 0)&0xff;
    					int image_red = (image_abgr >> 0)&0xff;
    					int template_green = (template_abgr >> 8)&0xff;
    					int image_green = (image_abgr >> 8)&0xff;
    					int template_blue = (template_abgr >> 16)&0xff;
    					int image_blue = (image_abgr >> 16)&0xff;

    					error +=  sqrt(pow(template_red - image_red,2) +
                   				pow(template_green - image_green,2) +
                   				pow(template_blue - image_blue,2));
  				}
			}	
			//Add the error of matching the template to the image location to the zarray of errors

			zarray_add(errors, &error);
			error = 0.0;
		}
	}
	
	//Loop through the errors and find the local minimum within each area
	for(int i=twidth; i < (iwidth-twidth); i=i+twidth){
		for(int j=theight; j < (iheight-theight); j=j+(theight)){
			int spot = j*tstride + i;
			double local_error;
			zarray_get(errors, spot, &local_error);
			int flag = 0;
			for(int a=0; a < twidth; a++){
				for(int b=0; b < theight; b++){
					double compare_left;
					double compare_right;
					double compare_up;
					double compare_down;
					zarray_get(errors, spot+a, &compare_right);
					zarray_get(errors, spot-a, &compare_left);
					zarray_get(errors, (j+b)*tstride + (i+a), &compare_up);
					zarray_get(errors, (j-b)*tstride + (i+a), &compare_down);
					if((local_error > compare_left)||(local_error > compare_right)||(local_error > compare_up)||(local_error > compare_down))
						flag = 1;
				}
			}
			
			//If a local minimum is detected and it is below the threshold, consider it a match
			if ((flag == 0) && (local_error < thresh)){
				double *pixels = calloc(1, sizeof(double)*2);
				//Transform it back to ground coordinates and store it
				double pixelx = (((i+twidth/2.)*2.0)/iwidth)-1.0;
				double pixely = (((j + theight/2.)*2.0)-iheight)/(-1.0*iwidth);
                                pixels[0] = pixelx;
                                pixels[1] = pixely;
                                varray_add(locations, pixels);
                        }
		}
	}
	
	return locations;
} 
