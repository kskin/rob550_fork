// ----------------------------------------------------------------------------------------------
// ----- PRM.H ---------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// This file implements a Probabilistic Roadmap Planner (PRM).
// ----------------------------------------------------------------------------------------------
#ifndef __PRM_H__
#define __PRM_H__

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <math.h>
#include "common/timeprofile.h"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math/gsl_util_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>
#include "imagesource/image_u32.h"
#include "imagesource/image_source.h"
#include "imagesource/image_convert.h"
#include "affine_calibration.h"
#include "common/varray.h"
#include "math/dijkstra.h"
#include <lcm/lcm.h>
#include "lcmtypes/dynamixel_command_list_t.h"
#include "lcmtypes/dynamixel_command_t.h"
#include "lcmtypes/dynamixel_status_list_t.h"
#include "lcmtypes/dynamixel_status_t.h"
#include "structures.h"
#include "rexarm.h"
#include "trajectory.h"
#include "affine_calibration.h"
#include "path_planning.h"

#define NEIGHBOURS 390 // Connectivity
#define NODES 400 // Number of nodes

// ===============================================================================================
// ===== ROADMAP CONSTRUCTION ====================================================================
// ===============================================================================================
// This function constructs a roadmap of the workspace with n nodes. Nodes are set as vertices
// in the graph and connected with edges if the path between nodes is collision free. Nodes refer
// to joint angle configurations. Returns graph.
// ===============================================================================================
dijkstra_graph_t * roadmap_construction(int n, int neighbours, varray_t *cylinders, varray_t *rings, state_t *state);

// ===============================================================================================
// ===== SOLVE QUERY =============================================================================
// ===============================================================================================
// This function finds the closest node in the roadmap to the initial arm configuration, qi,
// and the goal configuration, qf, given a graph g and arm state, state. Distance is measured
// by dist_norm. Returns shortest path between node closest to qi and node closest to qf, or
// returns -1 if no valid solution was found.
// ===============================================================================================
int* solve_query(double *qi, double *qf, int k, dijkstra_graph_t *g, state_t *state);

// ===============================================================================================
// ===== TWO NORM ================================================================================
// ===============================================================================================
// This function returns the q-space norm between qp and q.
// ===============================================================================================
double two_norm(double qp[4], double q[4]);

// ===============================================================================================
// ===== DIST NORM ===============================================================================
// ===============================================================================================
// This function returns the sum of squares distance (xyz coordinates) for end effector positions
// of configurations qp and q.
// ===============================================================================================
double dist_norm(double qp[4], double q[4]);

#endif
