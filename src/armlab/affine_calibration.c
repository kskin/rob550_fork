// ----------------------------------------------------------------------------------------------
// ----- AFFINE_CALIBRATION.C -------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------
// AUTHORS: Katelyn Fry, Meghan Richey, Katie Skinner
// SECTION: Section 1, Table 4
// ----------------------------------------------------------------------------------------------
// This file handles all affine calibration calculations. It takes in four coordinates and
// calculates the world origin. It also takes in locations of cylinders or rings and returns an
// appropriate place to render them on the screen.
// ----------------------------------------------------------------------------------------------

// ==============================================================================================
// ===== DEPENDENCIES ===========================================================================
// ==============================================================================================
#include "affine_calibration.h"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <math/gsl_util_matrix.h>

// ===============================================================================================
// ===== AFFINE CALIBRATION CREATE ===============================================================
// ===============================================================================================
// This function creates an affine calibration matrix based on the input pairs of coordinates.
// ===============================================================================================

affine_calibration_t * affine_calibration_create (const zarray_t * pairs)
{
  affine_calibration_t * x = (struct _affine_calibration_t*) malloc(sizeof(struct _affine_calibration_t));

  gsl_matrix * A, * cov;
  gsl_vector * b, * c;
  
  double chisq;
  double tempx, tempy;

  int size;
  int i, j = 0;

  //Calculates size of pairs
  size = zarray_size(pairs);
 
  //Allocates memory for matrices
  A = gsl_matrix_alloc(size, 6);
  b = gsl_vector_alloc(size);

  c = gsl_vector_alloc (6);
  cov = gsl_matrix_alloc (6, 6);

  gsl_multifit_linear_workspace * work  = gsl_multifit_linear_alloc (size, 6);

  //Populate A
  for(i = 0; i < size; i++)
  { 
      zarray_get(pairs, j, &tempx);
      zarray_get(pairs, j+1, &tempy);
      gsl_matrix_set(A, i, 0, tempx);
      gsl_matrix_set(A, i, 1, tempy);
      gsl_matrix_set(A, i, 2, 1);
      gsl_matrix_set(A, i, 3, 0);
      gsl_matrix_set(A, i, 4, 0);
      gsl_matrix_set(A, i, 5, 0);
      i++;
      gsl_matrix_set(A, i, 0, 0);
      gsl_matrix_set(A, i, 1, 0);
      gsl_matrix_set(A, i, 2, 0);
      gsl_matrix_set(A, i, 3, tempx);
      gsl_matrix_set(A, i, 4, tempy);
      gsl_matrix_set(A, i, 5, 1);

      j = j + 2;
  }

  //Populate b
  gsl_vector_set(b, 0, -.308);
  gsl_vector_set(b, 1, .304);
  gsl_vector_set(b, 2, .294);
  gsl_vector_set(b, 3, .304);
  gsl_vector_set(b, 4, -.308);
  gsl_vector_set(b, 5, -.315);
  gsl_vector_set(b, 6, .294);
  gsl_vector_set(b, 7, -.315);

  gsl_multifit_linear (A, b, c, cov, &chisq, work);

  x->length = 6;
  x->calibration[0] = gsl_vector_get(c, 0);
  x->calibration[1] = gsl_vector_get(c, 1);
  x->calibration[2] = gsl_vector_get(c, 2);
  x->calibration[3] = gsl_vector_get(c, 3);
  x->calibration[4] = gsl_vector_get(c, 4);
  x->calibration[5] = gsl_vector_get(c, 5);
  x->p2w[0] = 0.0;
  x->p2w[1] = 0.0;
  x->w2p[0] = 0.0;
  x->w2p[1] = 0.0;

  gsl_matrix_free(A);
  gsl_vector_free(b);
  gsl_vector_free(c);
  gsl_matrix_free(cov);

  gsl_multifit_linear_free (work);

  return x;
}

// ===============================================================================================
// ===== AFFINE CALIBRATION DESTROY ==============================================================
// ===============================================================================================
// This function destroys an instance of an affine calibration matrix.
// ===============================================================================================

void affine_calibration_destroy (affine_calibration_t * cal)
{
  free(cal);
  return;
}

// ================================================================================================
// ===== PIXELS TO WORLD ==========================================================================
// ================================================================================================
// This function converts input pixels to world coordinates using the affine calibration given.
// ================================================================================================

void affine_calibration_p2w (const affine_calibration_t *cal, const double pixels[2], double world[2])
{
  gsl_vector * w;  //world vector
  gsl_matrix * C; // Calibration matrix
  gsl_vector *p; // pixel vector
  C = gsl_matrix_alloc(3,3);
  gsl_matrix_set(C, 0, 0, cal->calibration[0]);
  gsl_matrix_set(C, 0, 1, cal->calibration[1]);
  gsl_matrix_set(C, 0, 2, cal->calibration[2]);
  gsl_matrix_set(C, 1, 0, cal->calibration[3]);
  gsl_matrix_set(C, 1, 1, cal->calibration[4]);
  gsl_matrix_set(C, 1, 2, cal->calibration[5]);
  gsl_matrix_set(C, 2, 0, 0);
  gsl_matrix_set(C, 2, 1, 0);
  gsl_matrix_set(C, 2, 2, 1);
  p=gsl_vector_alloc(3);
  gsl_vector_set(p, 0, pixels[0]);
  gsl_vector_set(p, 1, pixels[1]);
  gsl_vector_set(p, 2, 1.0);

  w = gsl_vector_alloc(3);

  //Calculate w
  gsl_blas_dgemv (CblasNoTrans, 1.0, C, p, 0.0, w);
  
  //Send w to cal structure
  world[0] = gsl_vector_get(w, 0);
  world[1] = gsl_vector_get(w, 1); 
  
  //Free matrices and vectors
  gsl_matrix_free(C);
  gsl_vector_free(p);
  gsl_vector_free(w);
  return;
}

// ================================================================================================
// ===== WORLD TO PIXELS ==========================================================================
// ================================================================================================
// This function translates input world coordinates to pixels using the affine calibration given.
// ================================================================================================

void affine_calibration_w2p (const affine_calibration_t *cal,const double world[2], double pixels[2])
{
  gsl_matrix * C;   //Calibration Matrix
  gsl_vector * w;   //world vector
  gsl_vector * p;   //pixel vector
  
  C = gsl_matrix_alloc(3,3);
  w = gsl_vector_alloc(3);
  p = gsl_vector_alloc(3);
 
  //Set w
  gsl_vector_set(w, 0, world[0]);
  gsl_vector_set(w, 1, world[1]);
  gsl_vector_set(w, 2, 1);

  gsl_matrix_set(C, 0, 0, cal->calibration[0]);
  gsl_matrix_set(C, 0, 1, cal->calibration[1]);
  gsl_matrix_set(C, 0, 2, cal->calibration[2]);
  gsl_matrix_set(C, 1, 0, cal->calibration[3]);
  gsl_matrix_set(C, 1, 1, cal->calibration[4]);
  gsl_matrix_set(C, 1, 2, cal->calibration[5]);
  gsl_matrix_set(C, 2, 0, 0);
  gsl_matrix_set(C, 2, 1, 0);
  gsl_matrix_set(C, 2, 2, 1);

  //calculates Cin
  gsl_matrix * Cin;
  gsl_permutation * perm;

  Cin = gsl_matrix_alloc(3,3);
  perm = gsl_permutation_alloc(3);

  //Performs LU decomp to determine Cin
  gslu_matrix_inv(Cin, C);

  //calculates Cin*w to get pixels
  gsl_blas_dgemv (CblasNoTrans, 1.0, Cin, w, 0.0, p);

  //sets p in struct
  pixels[0] = gsl_vector_get(p,0);
  pixels[1] = gsl_vector_get(p,1);

  //Free matrices and vectors
  gsl_matrix_free(C);  
  gsl_matrix_free(Cin);
  gsl_vector_free(w);
  gsl_vector_free(p);
  
  gsl_permutation_free(perm);

  return;
}
