#ifndef __TEMPLATE_MATCHER_H__
#define __TEMPLATE_MATCHER_H__

#include "common/zarray.h"
#include "imagesource/image_u32.h"
#include "imagesource/image_util.h"
#include "structures.h"

#ifdef __cplusplus
extern "C"{
#endif

//returns matches (zarray of double[2]'s)
varray_t * template_matcher_find(const image_u32_t *image, const image_u32_t *template, double thresh, int nearest_pixels, double decratio);

#ifdef __cplusplus
}
#endif

#endif //__TEMPLATE_MATCHER_H__
